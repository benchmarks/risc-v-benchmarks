# swaptions

## Usage
```
usage: ./bin/swaptions_scalar -ns 2 -sm 1024 -nt 1 
usage: ./bin/swaptions_vectorial_intrinsics -ns 2 -sm 1024 -nt 1 

example:
  ./bin/swaptions_vectorial_instrinsics -ns 2 -sm 1024 -nt 1 
```

## Parameters
```
  ns: Number of swaptions
  sm: Number of Simulations
  nt: Number of threads
```

## Validate result
Save output of the scalar version and compare with the result to validate with a tool like numdiff.
