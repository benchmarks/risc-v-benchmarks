# blackscholes

## Usage
```
usage: ./bin/blackscholes_scalar nthreads inputfile outputfile
usage: ./bin/blackscholes_vectorial_intrinsics nthreads inputfile outputfile

example:
  ./bin/blackscholes_vectorial_intrinsics 1 input/in_1024.input /dev/null
```
