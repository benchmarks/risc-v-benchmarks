# streamcluster

## Usage
```
usage: ./bin/streamcluster_scalar k1 k2 d n chunksize clustersize infile outfile nproc
usage: ./bin/streamcluster_vectorial_intrinsics k1 k2 d n chunksize clustersize infile outfile nproc
usage: ./bin/streamcluster_vectorial_autovectorization k1 k2 d n chunksize clustersize infile outfile nproc

example:
  ./bin/streamcluster_vectorial_autovectorization 3 10 16 16 16 10 none output.txt 1
```

## Parameters
```
  k1:          Min. number of centers allowed
  k2:          Max. number of centers allowed
  d:           Dimension of each data point
  n:           Number of data points
  chunksize:   Number of data points to handle per step
  clustersize: Maximum number of intermediate centers
  infile:      Input file (if n<=0)
  outfile:     Output file
  nproc:       Number of threads to use

if n > 0, points will be randomly generated instead of reading from infile.
```

## Validate result
Save output of the scalar version and compare with the result to validate with a tool like numdiff.
