#!/bin/bash

source ../../configure

VEHAVE_DEBUG_LEVEL=0
traces_dir="traces"
mkdir -p ${traces_dir}

for VECTOR_SIZE in 128 256 512 1024 2048 4096; do
	export VEHAVE_VECTOR_LENGTH=${VECTOR_SIZE}
	log_file="${traces_dir}/trace_${VECTOR_SIZE}.log"
	echo -n "Tracing vector size ${VECTOR_SIZE} at " > ${log_file}
	/bin/date >> ${log_file}
	LD_PRELOAD=${VEHAVE_LIB_SO} bin/particlefilter_vector.exe -x 128 -y 128 -z 2 -np 256 &>> ${log_file}
	raw_trace=$(ls -tr | tail -n 1)
	${VEHAVE_TO_PRV} --output-dir ${traces_dir} --output-name particlefilter_${VECTOR_SIZE}_x128y128z2np256 ${raw_trace} >> ${log_file}
	#rm ${raw_trace}
done

