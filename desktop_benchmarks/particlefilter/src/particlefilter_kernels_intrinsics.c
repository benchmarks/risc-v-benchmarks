#include "particlefilter_kernels_scalar.h"
#include "particlefilter_kernels_intrinsics.h"

#define PI 3.1415926535897932

inline _MMR_f64 randn_vector(long int * seed, int index ,unsigned long int gvl){
    /*Box-Muller algorithm*/
    _MMR_f64    xU = randu_vector(seed,index,gvl);
    _MMR_f64    xV = randu_vector(seed,index,gvl);
    _MMR_f64    xCosine;
    _MMR_f64    xRt;

    xV = _MM_MUL_f64(_MM_SET_f64(PI*2.0,gvl),xV,gvl);
    xCosine =_MM_COS_f64(xV,gvl);
    FENCE();
    xU = _MM_LOG_f64(xU,gvl);
    xRt =  _MM_MUL_f64(_MM_SET_f64(-2.0,gvl),xU,gvl);
    return _MM_MUL_f64(_MM_SQRT_f64(xRt,gvl),xCosine,gvl);
}

inline _MMR_f64 randu_vector(long int * seed, int index ,unsigned long int gvl)
{
    /*
    _MMR_i64    xseed = _MM_LOAD_i64(&seed[index],gvl);
    _MMR_i64    xA = _MM_SET_i64(A,gvl);
    _MMR_i64    xC = _MM_SET_i64(C,gvl);
    _MMR_i64    xM = _MM_SET_i64(M,gvl);

    xseed =  _MM_MUL_i64(xseed,xA,gvl);
    xseed =  _MM_ADD_i64(xseed,xC,gvl);

    _MM_STORE_i64(&seed[index],_MM_REM_i64(xseed,xM,gvl),gvl);
    FENCE();
    _MMR_f64    xResult;
    xResult = _MM_DIV_f64(_MM_VFCVT_F_X_f64(xseed,gvl),_MM_VFCVT_F_X_f64(xM,gvl),gvl);
    xResult = _MM_VFSGNJX_f64(xResult,xResult,gvl);
    return xResult;
    */

    /*
    Esta parte del codigo deberia ser en 32 bits, pero las instrucciones de conversion aún no están disponibles,
    moviendo todo a 64 bits el resultado cambia ya que no se desborda, y las variaciones son muchas.
    */
    double result[256];
    int num[256];
    //FENCE();
    //double* result = (double*)malloc(gvl*sizeof(double));
    //int* num = (int*)malloc(gvl*sizeof(int));

    FENCE();
    for(int x = index; x < index+gvl; x++){
        num[x-index] = A*seed[x] + C;
        seed[x] = num[x-index] % M;
        result[x-index] = fabs(seed[x]/((double) M));
    }
    _MMR_f64    xResult;
    xResult = _MM_LOAD_f64(&result[0],gvl);
    FENCE();
    return xResult;
}


void particleFilter_vector(int * I, int IszX, int IszY, int Nfr, int * seed, long int * seed_64, int Nparticles){


    int max_size = IszX*IszY*Nfr;
    long long start = get_time();

    //original particle centroid
    double xe = roundDouble(IszY/2.0);
    double ye = roundDouble(IszX/2.0);

    //expected object locations, compared to center
    int radius = 5;
    int diameter = radius*2 - 1;
    int * disk = (int *)malloc(diameter*diameter*sizeof(int));
    strelDisk(disk, radius);
    int countOnes = 0;
    int x, y;
    for(x = 0; x < diameter; x++){
        for(y = 0; y < diameter; y++){
            if(disk[x*diameter + y] == 1)
                countOnes++;
        }
    }

    double * objxy = (double *)malloc(countOnes*2*sizeof(double));
    getneighbors(disk, countOnes, objxy, radius);

    long long get_neighbors = get_time();
    printf("TIME TO GET NEIGHBORS TOOK: %f\n", elapsed_time(start, get_neighbors));
    double * weights = (double *)malloc(sizeof(double)*Nparticles);
    unsigned long int gvl = __builtin_epi_vsetvl(Nparticles, __epi_e64, __epi_m1);

    _MMR_f64    xweights = _MM_SET_f64(1.0/((double)(Nparticles)),gvl);
    for(x = 0; x < Nparticles; x=x+gvl){
        gvl     = __builtin_epi_vsetvl(Nparticles-x, __epi_e64, __epi_m1);
        _MM_STORE_f64(&weights[x],xweights,gvl);
    }
    FENCE();

    long long get_weights = get_time();
    printf("TIME TO GET WEIGHTSTOOK: %f\n", elapsed_time(get_neighbors, get_weights));
    //initial likelihood to 0.0
    double * likelihood = (double *)malloc(sizeof(double)*Nparticles);
    double * arrayX = (double *)malloc(sizeof(double)*Nparticles);
    double * arrayY = (double *)malloc(sizeof(double)*Nparticles);
    double * xj = (double *)malloc(sizeof(double)*Nparticles);
    double * yj = (double *)malloc(sizeof(double)*Nparticles);
    double * CDF = (double *)malloc(sizeof(double)*Nparticles);
    double * u = (double *)malloc(sizeof(double)*Nparticles);
    int * ind = (int*)malloc(sizeof(int)*countOnes*Nparticles);

    gvl     = __builtin_epi_vsetvl(Nparticles, __epi_e64, __epi_m1);
    _MMR_f64    xArrayX = _MM_SET_f64(xe,gvl);
    _MMR_f64    xArrayY = _MM_SET_f64(ye,gvl);
    for(int i = 0; i < Nparticles; i=i+gvl){
        gvl     = __builtin_epi_vsetvl(Nparticles-i, __epi_e64, __epi_m1);
        _MM_STORE_f64(&arrayX[i],xArrayX,gvl);
        _MM_STORE_f64(&arrayY[i],xArrayY,gvl);
    }
    FENCE();


    _MMR_f64    xAux;
    int k;
    printf("TIME TO SET ARRAYS TOOK: %f\n", elapsed_time(get_weights, get_time()));
    int indX, indY;
    for(k = 1; k < Nfr; k++){
        long long set_arrays = get_time();
        //apply motion model
        //draws sample from motion model (random walk). The only prior information
        //is that the object moves 2x as fast as in the y direction
        gvl     = __builtin_epi_vsetvl(Nparticles, __epi_e64, __epi_m1);
        for(x = 0; x < Nparticles; x=x+gvl){
        gvl     = __builtin_epi_vsetvl(Nparticles-x, __epi_e64, __epi_m1);
            xArrayX = _MM_LOAD_f64(&arrayX[x],gvl);
            FENCE();
            xAux = randn_vector(seed_64, x,gvl);
            FENCE();
            xAux =  _MM_MUL_f64(xAux, _MM_SET_f64(5.0,gvl),gvl);
            xAux =  _MM_ADD_f64(xAux, _MM_SET_f64(1.0,gvl),gvl);
            xArrayX = _MM_ADD_f64(xAux, xArrayX ,gvl);
            _MM_STORE_f64(&arrayX[x],xArrayX,gvl);

            xArrayY = _MM_LOAD_f64(&arrayY[x],gvl);
            FENCE();
            xAux = randn_vector(seed_64, x,gvl);
            FENCE();
            xAux =  _MM_MUL_f64(xAux, _MM_SET_f64(2.0,gvl),gvl);
            xAux =  _MM_ADD_f64(xAux, _MM_SET_f64(-2.0,gvl),gvl);
            xArrayY = _MM_ADD_f64(xAux, xArrayY ,gvl);
            _MM_STORE_f64(&arrayY[x],xArrayY,gvl);
        }
        FENCE();
        long long error = get_time();
        printf("TIME TO SET ERROR TOOK: %f\n", elapsed_time(set_arrays, error));
        for(x = 0; x < Nparticles; x++){
            //compute the likelihood: remember our assumption is that you know
            // foreground and the background image intensity distribution.
            // Notice that we consider here a likelihood ratio, instead of
            // p(z|x). It is possible in this case. why? a hometask for you.
            //calc ind
            for(y = 0; y < countOnes; y++){
                indX = roundDouble(arrayX[x]) + objxy[y*2 + 1];
                indY = roundDouble(arrayY[x]) + objxy[y*2];
                ind[x*countOnes + y] = fabs(indX*IszY*Nfr + indY*Nfr + k);
                if(ind[x*countOnes + y] >= max_size)
                    ind[x*countOnes + y] = 0;
            }
            likelihood[x] = 0;
            for(y = 0; y < countOnes; y++)
                likelihood[x] += (pow((I[ind[x*countOnes + y]] - 100),2) - pow((I[ind[x*countOnes + y]]-228),2))/50.0;
            likelihood[x] = likelihood[x]/((double) countOnes);
        }

        long long likelihood_time = get_time();
        printf("TIME TO GET LIKELIHOODS TOOK: %f\n", elapsed_time(error, likelihood_time));
        // update & normalize weights
        // using equation (63) of Arulampalam Tutorial
        //#pragma omp parallel for shared(Nparticles, weights, likelihood) private(x)
        for(x = 0; x < Nparticles; x++){
            weights[x] = weights[x] * exp(likelihood[x]);
        }
        long long exponential = get_time();
        printf("TIME TO GET EXP TOOK: %f\n", elapsed_time(likelihood_time, exponential));
        double sumWeights = 0;
        //#pragma omp parallel for private(x) reduction(+:sumWeights)
        for(x = 0; x < Nparticles; x++){
            sumWeights += weights[x];
        }
        long long sum_time = get_time();
        printf("TIME TO SUM WEIGHTS TOOK: %f\n", elapsed_time(exponential, sum_time));
        //#pragma omp parallel for shared(sumWeights, weights) private(x)
        for(x = 0; x < Nparticles; x++){
            weights[x] = weights[x]/sumWeights;
        }
        long long normalize = get_time();
        printf("TIME TO NORMALIZE WEIGHTS TOOK: %f\n", elapsed_time(sum_time, normalize));
        xe = 0;
        ye = 0;
        // estimate the object location by expected values
        //#pragma omp parallel for private(x) reduction(+:xe, ye)
        for(x = 0; x < Nparticles; x++){
            xe += arrayX[x] * weights[x];
            ye += arrayY[x] * weights[x];
        }
        long long move_time = get_time();
        printf("TIME TO MOVE OBJECT TOOK: %f\n", elapsed_time(normalize, move_time));
        printf("XE: %lf\n", xe);
        printf("YE: %lf\n", ye);
        double distance = sqrt( pow((double)(xe-(int)roundDouble(IszY/2.0)),2) + pow((double)(ye-(int)roundDouble(IszX/2.0)),2) );
        printf("%lf\n", distance);

        //resampling
        CDF[0] = weights[0];
        for(x = 1; x < Nparticles; x++){
            CDF[x] = weights[x] + CDF[x-1];
        }
        long long cum_sum = get_time();
        printf("TIME TO CALC CUM SUM TOOK: %f\n", elapsed_time(move_time, cum_sum));
        double u1 = (1/((double)(Nparticles)))*randu(seed, 0);
        //#pragma omp parallel for shared(u, u1, Nparticles) private(x)
        for(x = 0; x < Nparticles; x++){
            u[x] = u1 + x/((double)(Nparticles));
        }
        long long u_time = get_time();
        printf("TIME TO CALC U TOOK: %f\n", elapsed_time(cum_sum, u_time));

        int j, i;

        _MMR_MASK_i64   xComp;
        _MMR_i64        xMask;

        _MMR_f64        xCDF;
        _MMR_f64        xU;
        _MMR_i64        xArray;

        long int vector_complete;
        long int * locations = (long int *)malloc(sizeof(long int)*Nparticles);
        long int valid;
        gvl     = __builtin_epi_vsetvl(Nparticles, __epi_e64, __epi_m1);
        for(i = 0; i < Nparticles; i=i+gvl){
            gvl     = __builtin_epi_vsetvl(Nparticles-i, __epi_e64, __epi_m1);
            vector_complete = 0;
            xMask   = _MM_SET_i64(0,gvl);
            xArray  = _MM_SET_i64(Nparticles-1,gvl);
            xU      = _MM_LOAD_f64(&u[i],gvl);
            for(j = 0; j < Nparticles; j++){
                xCDF = _MM_SET_f64(CDF[j],gvl);
                xComp = _MM_VFGE_f64(xCDF,xU,gvl);
                xComp = _MM_CAST_i1_i64(_MM_XOR_i64(_MM_CAST_i64_i1(xComp),xMask,gvl));
                valid = _MM_VMFIRST_i64(xComp,gvl);
                if(valid != -1)
                {
                    xArray = _MM_MERGE_i64(xArray,_MM_SET_i64(j,gvl),xComp,gvl);
                    xMask = _MM_OR_i64(_MM_CAST_i64_i1(xComp),xMask,gvl);
                    vector_complete = _MM_VMPOPC_i64(_MM_CAST_i1_i64(xMask),gvl);
                }
                if(vector_complete == gvl){ break; }
                //FENCE();
            }
            _MM_STORE_i64(&locations[i],xArray,gvl);
        }
        FENCE();
        //#pragma omp parallel for shared(CDF, Nparticles, xj, yj, u, arrayX, arrayY) private(i, j)
        for(j = 0; j < Nparticles; j++){
            i = locations[j];
            xj[j] = arrayX[i];
            yj[j] = arrayY[i];
        }

        long long xyj_time = get_time();
        printf("TIME TO CALC NEW ARRAY X AND Y TOOK: %f\n", elapsed_time(u_time, xyj_time));

        //#pragma omp parallel for shared(weights, Nparticles) private(x)
        for(x = 0; x < Nparticles; x++){
            //reassign arrayX and arrayY
            arrayX[x] = xj[x];
            arrayY[x] = yj[x];
            weights[x] = 1/((double)(Nparticles));
        }
        long long reset = get_time();
        printf("TIME TO RESET WEIGHTS TOOK: %f\n", elapsed_time(xyj_time, reset));
    }
    free(disk);
    //free(objxy);
    free(weights);
    free(likelihood);
    free(xj);
    free(yj);
    free(arrayX);
    free(arrayY);
    free(CDF);
    free(u);
    free(ind);
}


