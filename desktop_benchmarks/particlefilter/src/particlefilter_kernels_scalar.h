#pragma once
#include <limits.h>

#define M  INT_MAX
#define A  1103515245
#define C  12345

double randn(int * seed, int index);
double randu(int * seed, int index);
void particleFilter(int * I, int IszX, int IszY, int Nfr, int * seed, int Nparticles);
