#pragma once
#include "vector_defines.h"

inline _MMR_f64 randn_vector(long int * seed, int index ,unsigned long int gvl);
inline _MMR_f64 randu_vector(long int * seed, int index ,unsigned long int gvl);
void particleFilter_vector(int * I, int IszX, int IszY, int Nfr, int * seed, long int * seed_64, int Nparticles);
