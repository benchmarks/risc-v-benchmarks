#Axpy

```
Usage: ./particlefilter_scalar -x size_x_dim -y size_y_dim -z num_frames -np num_particles
Usage: ./particlefilter_vectorial -x size_x_dim -y size_y_dim -z num_frames -np num_particles

Example:
./particlefilter_scalar -x 1024 -y 1024 -z 4 -np 8192
```
