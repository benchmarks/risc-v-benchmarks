# canneal

## Usage
```
usage: ./bin/canneal_scalar NTHREADS NSWAPS TEMP NETLIST [NSTEPS]
usage: ./bin/canneal_vectorial_intrinsics NTHREADS NSWAPS TEMP NETLIST [NSTEPS]

example:
  ./bin/canneal_vectorial_intrinsics 1 100 300 input/100.nets 32 
```
