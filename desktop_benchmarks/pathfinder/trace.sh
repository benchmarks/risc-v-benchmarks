#!/bin/bash

VEHAVE_DEBUG_LEVEL=0
traces_dir="traces"
mkdir -p ${traces_dir}

for VECTOR_SIZE in 128 256 512 1024 2048 4096; do
	export VEHAVE_VECTOR_LENGTH=${VECTOR_SIZE}
	log_file="${traces_dir}/trace_${VECTOR_SIZE}.log"
	echo -n "Tracing vector size ${VECTOR_SIZE} at " > ${log_file}
	/bin/date >> ${log_file}
	LD_PRELOAD=/apps/vehave/EPI-0.7/development/lib64/libvehave.so bin/pathfinder_vector.exe 64 64 output_64_64.txt &>> ${log_file}
	raw_trace=$(ls -tr | tail -n 1)
	/apps/vehave/EPI-0.7/development/share/vehave2prv/vehave2prv --output-dir ${traces_dir} --output-name pathfinder_${VECTOR_SIZE}_64_64 ${raw_trace} >> ${log_file}
	rm ${raw_trace}
done

