#Compile all benchmarks with individual settings defined in their Makefiles

include Makefile.in
HPC_BENCHMARKS_DIRS=axpy fft fftp jacobi-2d lulesh gemm somier spmv

DESKTOP_BENCHMARKS_DIRS=blackscholes canneal particlefilter pathfinder streamcluster swaptions

MICRO_BENCHMARKS_DIRS=BuffCopyUnit BuffCopyStrided BuffCopyIndexed FpuMicroKernel InstrNopBalance MemArithBalance LatencyVrgather

.PHONY: default all clean $(HPC_BENCHMARKS_DIRS) $(MICRO_BENCHMARKS_DIRS)

default: 
	@cd common; make; cd ..
	@$(foreach dir,$(HPC_BENCHMARKS_DIRS),${MAKE} -C hpc_benchmarks/$(dir);)
	@$(foreach dir,$(DESKTOP_BENCHMARKS_DIRS),${MAKE} -C desktop_benchmarks/$(dir);)
	@$(foreach dir,$(MICRO_BENCHMARKS_DIRS),${MAKE} -C micro_benchmarks/$(dir);)

all: 
	@cd common; make all; cd ..
	@$(foreach dir,$(HPC_BENCHMARKS_DIRS),${MAKE} -C hpc_benchmarks/$(dir) all ;)
	@$(foreach dir,$(DESKTOP_BENCHMARKS_DIRS),${MAKE} -C desktop_benchmarks/$(dir) all ;)
	@$(foreach dir,$(MICRO_BENCHMARKS_DIRS),${MAKE} -C micro_benchmarks/$(dir) all ;)

fftp:
	${MAKE} -C third_party fftw
	${MAKE} -C hpc_benchmarks/fftp all
	${MAKE} -C hpc_benchmarks/fftp/test all

spmv-ellpack:
	rm -rf hpc_benchmarks/spmv-ellpack/spmv/build
	mkdir -p hpc_benchmarks/spmv-ellpack/spmv/build
	cd hpc_benchmarks/spmv-ellpack/spmv/build;\
	../configure riscv;\
	INDEX64=1 EPI_EXT=07 PATH=${EPI_LLVM_HOME}/bin:${PATH} make

clean:
	@cd common; make clean; cd ..
	@$(foreach dir,$(HPC_BENCHMARKS_DIRS),${MAKE} -C hpc_benchmarks/$(dir) clean ;)
	@$(foreach dir,$(DESKTOP_BENCHMARKS_DIRS),${MAKE} -C desktop_benchmarks/$(dir) clean ;)
	@$(foreach dir,$(MICRO_BENCHMARKS_DIRS),${MAKE} -C micro_benchmarks/$(dir) clean ;)
	@rm -rf hpc_benchmarks/spmv-ellpack/spmv/build

