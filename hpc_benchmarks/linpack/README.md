This directory contains a RVV-optimized Linpack.

#Compilation

First, load the clang, blis, and openmpi modules and export the required enviroment variables. In arriesgado nodes:

```
module load llvm/EPI-0.7-development blis openmpi/ubuntu/4.1.5_gcc11.3.0 
export OMPI_CC=clang
```

Then, run `make`


#Running

The Linpack binary will be found in `build/xhpl`, alongside the input file `build/HPL.dat`.

To run it you will need to load the clang, openmpi, and blis modules:

```
module load llvm/EPI-0.7-development openmpi/ubuntu/4.1.5_gcc11.3.0 blis
```

Then go into the `build` directory and execute the binary with no arguments:

```
cd build
./xhpl
```

You can also execute the script `./run.sh` with the `native` or `emulated` argument to automatically load the modules, run Linpack, and parse the results. 

If you delete the `build/HPL.dat` input file by mistake, it is available at hpl-2.3/HPL.dat.
