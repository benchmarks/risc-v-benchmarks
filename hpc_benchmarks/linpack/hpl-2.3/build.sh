#!/bin/bash
source /etc/profile.d/modules.sh

module load llvm/EPI-0.7-development blis openBLAS/ubuntu/0.3.20_gcc10.3.0 openmpi/ubuntu/4.1.5_gcc11.3.0 extrae/4.0.6_papi-like
export OMPI_CC=clang
export HPL_BUILD_DIR=$PWD

make clean_arch_all arch=Linux_EPAC_blis_vector
make clean_arch_all arch=Novec

make arch=Linux_EPAC_blis_vector
make arch=Novec

