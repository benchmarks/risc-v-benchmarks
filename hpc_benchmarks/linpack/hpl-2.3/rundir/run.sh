#!/bin/bash

if [ $# -ne 1 ]; then
	echo "need runmode:"
	echo "./run.sh [vec, scalar]"
	exit -1
fi

source /etc/profile.d/modules.sh
module load llvm/EPI-0.7-development openmpi/ubuntu/4.1.5_gcc11.3.0 extrae/4.0.6_papi-like

if [ $1 == "vec" ]; then
	binary=xhpl
	module load blis autoHP
	prefix=autohp
else
	binary=xhpl-novec
	module load openBLAS/ubuntu/0.3.20_gcc10.3.0
fi

IFS=;
out=`$prefix ./$binary`
N=`echo $out | grep WC00L2L16 | awk '{print $2}'`
t=`echo $out | grep WC00L2L16 | awk '{print $6}'`
g=`echo $out | grep WC00L2L16 | awk '{print $7}'`
echo -e "$binary\t$N\t$t\t$g"
