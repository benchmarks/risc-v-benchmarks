#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"


#Load Extrae
VERSION=4.0.4
#source /etc/profile.d/modules.sh
module purge 
module load extrae/${VERSION} 
module load llvm/EPI-0.7-development blis openBLAS/ubuntu/0.3.20_gcc10.3.0 openmpi/ubuntu/4.1.5_gcc11.3.0
export OMPI_CC=clang
export SLURM_MPI_TYPE="pmix"
export OMPI_MCA_btl="^openib"
export LD_LIBRARY_PATH=${PAPI_LIBS}:$LD_LIBRARY_PATH
export EXTRAE_CONFIG_FILE=${SCRIPT_DIR}/extrae-arriesgado.xml

#Execute instrumented binary
#LD_PRELOAD=${EXTRAE_HOME}/lib/libseqtrace.so:${LD_PRELOAD} "$@"
"$@"

#Move traces to extrae_prv_traces folder
mkdir -p extrae_prv_traces 
tracename=`basename $1`
finalname=$tracename
for ext in prv pcf row; do mv ${tracename}.${ext} extrae_prv_traces/arr-${finalname}.${ext}; done
rm -rf set-0
