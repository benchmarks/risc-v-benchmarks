#!/bin/bash

usage() {
	echo "Must specifiy running mode, either \"native\" or \"emulate\"."
	echo "Usage: ./run.sh <native/emulate>"
	exit -1
}

if [ $# -ne 1 ]; then usage; fi

if [[ "$1" == "emulate" ]]; then
	export VEHAVE_TRACE_SINGLE_THREAD=1;
	export VEHAVE_DEBUG_LEVEL=0 
	export VEHAVE_TRACE=0 
	export VEHAVE_VECTOR_LENGTH=16384 
	prefix=/apps/riscv/vehave/EPI-0.7/development/bin/vehave
elif [[ "$1" != "native" ]]; then
  usage
fi

source /etc/profile.d/modules.sh
module load llvm/EPI-0.7-development openmpi/ubuntu/4.1.5_gcc11.3.0 blis

cd build

echo -e "n,time,gflops"

output=$(${prefix} ./xhpl)
exit_code=$?
if [ ${exit_code} -ne 0 ]; then
  echo "Failed execution."
  exit 1
fi

N=$(echo "${output}" | grep WC00L2L16 | awk '{print $2}')
t=$(echo "${output}" | grep WC00L2L16 | awk '{print $6}')
g=$(echo "${output}" | grep WC00L2L16 | awk '{print $7}')
echo -e "${N},${t},${g}"
