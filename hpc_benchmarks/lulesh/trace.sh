#!/bin/bash

source ../../configure

VEHAVE_DEBUG_LEVEL=0
traces_dir="traces"
mkdir -p ${traces_dir}

app="lulesh2.0"
args="-i 2 -s 10 -p"

for VECTOR_SIZE in 128 256 512 1024 2048 4096; do
	export VEHAVE_VECTOR_LENGTH=${VECTOR_SIZE}
	log_file="${traces_dir}/trace_${VECTOR_SIZE}.log"
	echo -n "Tracing vector size ${VECTOR_SIZE} at " > ${log_file}
	/bin/date >> ${log_file}
	LD_PRELOAD=${VEHAVE_LIB_SO} ./${app} ${args} &>> ${log_file}
	raw_trace=$(ls -tr | tail -n 1)
	${VEHAVE_TO_PRV} --output-dir ${traces_dir} --output-name lulesh2_0_${VECTOR_SIZE}_i2s10p ${raw_trace} >> ${log_file}
	echo rm ${raw_trace} # FIXME: cannot assume last created file is the raw trace
done

