#!/bin/bash
source ../../environment.sh
source ${RVB_RUN_DIR}/run_main.sh $*

mode=$1;

export PARSING_FILE="`pwd`/parse.sh"

# Options:
versions="scalar autovect rvv0.7" 
sizes=`seq 12 22`
times=1


for i_size in ${sizes}; do
  size=$(( 2 ** i_size))
  ARGS="${size} ${times}"
  for version in ${versions}; do
    BINARY=`pwd`/bin/axpy-${version}
    export PREFIX_VEHAVE_TRACE_FILE=traces/axpy_${version}-${size}_elements
    echo "Executing ${BINARY} ${ARGS}"
    ${RVB_RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
  done
done
