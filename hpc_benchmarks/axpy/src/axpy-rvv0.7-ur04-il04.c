#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

void axpy(double a, double *dx, double *dy, int n) {

  const long max_gvl = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
  __epi_1xf64 v_a = __builtin_epi_vbroadcast_1xf64(a, max_gvl);

    int i0 = 0;
    long gvl0 = __builtin_epi_vsetvl(n, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx0  = __builtin_epi_vload_1xf64(&dx[0], gvl0);
    __epi_1xf64 v_dy0  = __builtin_epi_vload_1xf64(&dy[0], gvl0);

    int i1 = gvl0;
    long gvl1 = __builtin_epi_vsetvl(n-i1, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx1  = __builtin_epi_vload_1xf64(&dx[i1], gvl1);
    __epi_1xf64 v_dy1  = __builtin_epi_vload_1xf64(&dy[i1], gvl1);

    int i2 = i1+gvl1;
    long gvl2 = __builtin_epi_vsetvl(n-i2, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx2  = __builtin_epi_vload_1xf64(&dx[i2], gvl2);
    __epi_1xf64 v_dy2  = __builtin_epi_vload_1xf64(&dy[i2], gvl2);

    int i3 = i2+gvl2;
    long gvl3 = __builtin_epi_vsetvl(n-i3, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx3  = __builtin_epi_vload_1xf64(&dx[i3], gvl3);
    __epi_1xf64 v_dy3  = __builtin_epi_vload_1xf64(&dy[i3], gvl3);

    __epi_1xf64 v_res0, v_res1, v_res2, v_res3; 

  for (int i4 = i3+gvl3; i4 < n;) {

    long gvl4 = __builtin_epi_vsetvl(n-i4, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx4  = __builtin_epi_vload_1xf64(&dx[i4], gvl4);
    __epi_1xf64 v_dy4  = __builtin_epi_vload_1xf64(&dy[i4], gvl4);

    int i5 = i4+gvl4;
    long gvl5 = __builtin_epi_vsetvl(n-i5, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx5  = __builtin_epi_vload_1xf64(&dx[i5], gvl5);
    __epi_1xf64 v_dy5  = __builtin_epi_vload_1xf64(&dy[i5], gvl5);

    int i6 = i5+gvl5;
    long gvl6 = __builtin_epi_vsetvl(n-i6, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx6  = __builtin_epi_vload_1xf64(&dx[i6], gvl6);
    __epi_1xf64 v_dy6  = __builtin_epi_vload_1xf64(&dy[i6], gvl6);

    int i7 = i6+gvl6;
    long gvl7 = __builtin_epi_vsetvl(n-i7, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx7  = __builtin_epi_vload_1xf64(&dx[i7], gvl7);
    __epi_1xf64 v_dy7  = __builtin_epi_vload_1xf64(&dy[i7], gvl7);

    v_res0 = __builtin_epi_vfmacc_1xf64(v_dy0, v_a, v_dx0, gvl0);
    __builtin_epi_vstore_1xf64(&dy[i0], v_res0, gvl0);

    v_res1 = __builtin_epi_vfmacc_1xf64(v_dy1, v_a, v_dx1, gvl1);
    __builtin_epi_vstore_1xf64(&dy[i1], v_res1, gvl1);

    v_res2 = __builtin_epi_vfmacc_1xf64(v_dy2, v_a, v_dx2, gvl2);
    __builtin_epi_vstore_1xf64(&dy[i2], v_res2, gvl2);

    v_res3 = __builtin_epi_vfmacc_1xf64(v_dy3, v_a, v_dx3, gvl3);
    __builtin_epi_vstore_1xf64(&dy[i3], v_res3, gvl3);

    i0 = i7 + gvl7;
    gvl0 = __builtin_epi_vsetvl(n-i0, __epi_e64, __epi_m1);
    v_dx0  = __builtin_epi_vload_1xf64(&dx[i0], gvl0);
    v_dy0  = __builtin_epi_vload_1xf64(&dy[i0], gvl0);

    i1 = i0 + gvl0;
    gvl1 = __builtin_epi_vsetvl(n-i1, __epi_e64, __epi_m1);
    v_dx1  = __builtin_epi_vload_1xf64(&dx[i1], gvl1);
    v_dy1  = __builtin_epi_vload_1xf64(&dy[i1], gvl1);

    i2 = i1 + gvl1;
    gvl2 = __builtin_epi_vsetvl(n-i2, __epi_e64, __epi_m1);
    v_dx2  = __builtin_epi_vload_1xf64(&dx[i2], gvl2);
    v_dy2  = __builtin_epi_vload_1xf64(&dy[i2], gvl2);

    i3 = i2 + gvl2;
    gvl3 = __builtin_epi_vsetvl(n-i3, __epi_e64, __epi_m1);
    v_dx3  = __builtin_epi_vload_1xf64(&dx[i3], gvl3);
    v_dy3  = __builtin_epi_vload_1xf64(&dy[i3], gvl3);

    __epi_1xf64 v_res4 = __builtin_epi_vfmacc_1xf64(v_dy4, v_a, v_dx4, gvl4);
    __builtin_epi_vstore_1xf64(&dy[i4], v_res4, gvl4);

    __epi_1xf64 v_res5 = __builtin_epi_vfmacc_1xf64(v_dy5, v_a, v_dx5, gvl5);
    __builtin_epi_vstore_1xf64(&dy[i5], v_res5, gvl5);

    __epi_1xf64 v_res6 = __builtin_epi_vfmacc_1xf64(v_dy6, v_a, v_dx6, gvl6);
    __builtin_epi_vstore_1xf64(&dy[i6], v_res6, gvl6);

    __epi_1xf64 v_res7 = __builtin_epi_vfmacc_1xf64(v_dy7, v_a, v_dx7, gvl7);
    __builtin_epi_vstore_1xf64(&dy[i7], v_res7, gvl7);

    i4 = i3 + gvl3;
  }

  v_res0 = __builtin_epi_vfmacc_1xf64(v_dy0, v_a, v_dx0, gvl0);
  __builtin_epi_vstore_1xf64(&dy[i0], v_res0, gvl0);

  v_res1 = __builtin_epi_vfmacc_1xf64(v_dy1, v_a, v_dx1, gvl1);
  __builtin_epi_vstore_1xf64(&dy[i1], v_res1, gvl1);

  v_res2 = __builtin_epi_vfmacc_1xf64(v_dy2, v_a, v_dx2, gvl2);
  __builtin_epi_vstore_1xf64(&dy[i2], v_res2, gvl2);

  v_res3 = __builtin_epi_vfmacc_1xf64(v_dy3, v_a, v_dx3, gvl3);
  __builtin_epi_vstore_1xf64(&dy[i3], v_res3, gvl3);

}
