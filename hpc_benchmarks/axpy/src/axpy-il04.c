//Scalar code also reference code
void axpy(double a, double *dx, double *dy, int n) {
   #pragma clang loop vectorize(enable) interleave_count(4)
   for (int i=0; i<n; i++) {
      dy[i] += a*dx[i];
   }
}
