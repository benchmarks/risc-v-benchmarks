// See LICENSE for license details.

//**************************************************************************
// Multi-threaded axpy benchmark
//--------------------------------------------------------------------------
// This benchmark This benchmark runs several AXPY operations in parallle. AXPY
// is a Level 1 operation in the Basic Linear Algebra Subprograms (BLAS)
// package, and is a common operation in computations with vector processors.
// AXPY is a combination of scalar multiplication and vector addition. The
// input data (and reference data) should be generated using the
// gendata.pl perl script 
//--------------------------------------------------------------------------
// Includes 

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include "custom_def.h"

//--------------------------------------------------------------------------
// Basic Utilities and Multi-thread Support
#include "util.h"
#include "dataset.h"


#define  VERYFY_RESULT  
//--------------------------------------------------------------------------
// axpy function
 
extern void __attribute__((noinline)) axpy(const size_t coreid, const size_t ncores,const size_t lda , data_t A[], data_t B[], data_t alpha );

#ifdef VERYFY_RESULT
    #include "double_cmp.h"
    extern int   mt_verify(const size_t coreid, const size_t ncores,const size_t lda , double* test,   double* verify );
#endif


int MAIN()
{
   INIT_CID(); // Provides core id (cid) and number of cores (nc)

   if(cid==0) {printf("We are %d cores.\n",nc);}  

   BARRIER();
   stats(axpy(cid,nc,ARRAY_SIZE,input1_data,input2_data,Alfa); BARRIER(),ARRAY_SIZE);
   BARRIER();

#ifdef VERYFY_RESULT
   int res = mt_verify(cid,nc,ARRAY_SIZE , input1_data, verify_data);
   if(res){
      printf("Verrification failed!\n");
      exit(res);  
   }
   BARRIER();
#endif

   exit(0);  
}

