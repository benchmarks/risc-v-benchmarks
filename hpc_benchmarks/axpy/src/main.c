#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "elapsed_time.h"
#include "compare_results.h"
#include "alloc_hp.h"
#include "common.h"

#ifdef USE_FLOAT
	#define compare_array(dy_opt, dy_ref, n) compare_array_float(dy_opt, dy_ref, n)
#else
	#define compare_array(dy_opt, dy_ref, n) compare_array_double(dy_opt, dy_ref, n)
#endif

//Kernel functions signature
extern void axpy(ELEM_T a, ELEM_T *dx, ELEM_T *dy, int n);

void axpy_ref(ELEM_T a, ELEM_T *dx, ELEM_T *dy, int n) {
   int i;
   for (i=0; i<n; i++) {
      dy[i] += a*dx[i];
   }
}

void init_vector(ELEM_T *pv, long n, ELEM_T value)
{
   for (int i=0; i<n; i++){ 
     pv[i]= value;
   }
}

int main(int argc, char *argv[])
{
  long long start,end;

  ELEM_T a=1.0;
  long n;
  long ntimes;

  if (argc == 3){
    n = atol(argv[1]);
    ntimes = atol(argv[2]);
  }else{
    printf("Usage ./axpy num_elements ntimes\n");
    exit(1);
  }
	printf("Size of ELEM_T: %zu (bytes)\n", sizeof(ELEM_T));
  /* Allocate the source and result vectors */
  ELEM_T *dx     = (ELEM_T*)malloc_hp(n*sizeof(ELEM_T));
  ELEM_T *dy_opt = (ELEM_T*)malloc_hp(n*sizeof(ELEM_T));

  init_vector(dx, n, 1.0);
  init_vector(dy_opt, n, 2.0);
  
  start = get_time();
  for(int i = 0; i < ntimes; i++){
    axpy(a, dx, dy_opt, n);
  }
  end = get_time();
  double elapsed = elapsed_time(start, end);
  printf("Time: %f s\n", elapsed);
  printf("Performance: %f GFLOPS/s\n", (ntimes*n*2/1E9)/elapsed);

#if CHECK_RESULT
  printf("Checking result against refeference version.\n");
  ELEM_T *dy_ref = (ELEM_T*)malloc(n*sizeof(ELEM_T));
  init_vector(dy_ref, n, 2.0);
  for(int i = 0; i < ntimes; i++){
    axpy_ref(a, dx, dy_ref, n);
  }
  compare_array(dy_opt, dy_ref, n);
  free(dy_ref);
#endif

  printf ("done\n");
  free_hp(dy_opt, n*sizeof(ELEM_T ));
  free_hp(dx, n*sizeof(ELEM_T ));
  return 0;
}
