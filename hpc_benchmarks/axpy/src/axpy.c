#include "common.h"
void axpy(ELEM_T a, ELEM_T *dx, ELEM_T *dy, int n) {
   int i;
   for (i=0; i<n; i++) {
      dy[i] += a*dx[i];
   }
}
