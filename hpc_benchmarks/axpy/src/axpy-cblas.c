#include "blis.h"
#include "common.h"

void axpy(ELEM_T a, ELEM_T *dx, ELEM_T *dy, int n) {
#ifdef USE_FLOAT
	cblas_saxpy(n, a, dx, 1, dy, 1);
#else
	cblas_daxpy(n, a, dx, 1, dy, 1);
#endif
}

