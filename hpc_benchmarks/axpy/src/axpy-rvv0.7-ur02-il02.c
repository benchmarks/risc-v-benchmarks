#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

void axpy(double a, double *dx, double *dy, int n) {

  const long max_gvl = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
  __epi_1xf64 v_a = __builtin_epi_vbroadcast_1xf64(a, max_gvl);

    int i0 = 0;
    long gvl0 = __builtin_epi_vsetvl(n, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx0  = __builtin_epi_vload_1xf64(&dx[0], gvl0);
    __epi_1xf64 v_dy0  = __builtin_epi_vload_1xf64(&dy[0], gvl0);

    int i1 = gvl0;
    long gvl1 = __builtin_epi_vsetvl(n-i1, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx1  = __builtin_epi_vload_1xf64(&dx[i1], gvl1);
    __epi_1xf64 v_dy1  = __builtin_epi_vload_1xf64(&dy[i1], gvl1);

    __epi_1xf64 v_res0, v_res1; 

  for (int i2 = i1+gvl1; i2 < n;) {

    long gvl2 = __builtin_epi_vsetvl(n-i2, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx2  = __builtin_epi_vload_1xf64(&dx[i2], gvl2);
    __epi_1xf64 v_dy2  = __builtin_epi_vload_1xf64(&dy[i2], gvl2);

    long i3 = i2+gvl2;
    long gvl3 = __builtin_epi_vsetvl(n-i3, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx3  = __builtin_epi_vload_1xf64(&dx[i3], gvl3);
    __epi_1xf64 v_dy3  = __builtin_epi_vload_1xf64(&dy[i3], gvl3);

    v_res0 = __builtin_epi_vfmacc_1xf64(v_dy0, v_a, v_dx0, gvl0);
    __builtin_epi_vstore_1xf64(&dy[i0], v_res0, gvl0);

    __epi_1xf64 v_res1 = __builtin_epi_vfmacc_1xf64(v_dy1, v_a, v_dx1, gvl1);
    __builtin_epi_vstore_1xf64(&dy[i1], v_res1, gvl1);

    i0 = i3 + gvl3;

    gvl0 = __builtin_epi_vsetvl(n-i0, __epi_e64, __epi_m1);
    v_dx0  = __builtin_epi_vload_1xf64(&dx[i0], gvl0);
    v_dy0  = __builtin_epi_vload_1xf64(&dy[i0], gvl0);

    i1 = i0 + gvl0;
    gvl1 = __builtin_epi_vsetvl(n-i1, __epi_e64, __epi_m1);
    v_dx1  = __builtin_epi_vload_1xf64(&dx[i1], gvl1);
    v_dy1  = __builtin_epi_vload_1xf64(&dy[i1], gvl1);

    __epi_1xf64 v_res2 = __builtin_epi_vfmacc_1xf64(v_dy2, v_a, v_dx2, gvl2);
    __builtin_epi_vstore_1xf64(&dy[i2], v_res2, gvl2);

    __epi_1xf64 v_res3 = __builtin_epi_vfmacc_1xf64(v_dy3, v_a, v_dx3, gvl3);
    __builtin_epi_vstore_1xf64(&dy[i3], v_res3, gvl3);

    i2 = i1 + gvl1;
  }

  v_res0 = __builtin_epi_vfmacc_1xf64(v_dy0, v_a, v_dx0, gvl0);
  __builtin_epi_vstore_1xf64(&dy[i0], v_res0, gvl0);

  v_res1 = __builtin_epi_vfmacc_1xf64(v_dy1, v_a, v_dx1, gvl1);
  __builtin_epi_vstore_1xf64(&dy[i1], v_res1, gvl1);

}
