#include "common.h"
//Scalar code also reference code
void axpy(ELEM_T a, ELEM_T *dx, ELEM_T *dy, int n) {
   #pragma omp simd
   for (int i=0; i<n; i++) {
      dy[i] += a*dx[i];
   }
}
