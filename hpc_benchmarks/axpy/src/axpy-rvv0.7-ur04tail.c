#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

void axpy(double a, double *dx, double *dy, int n) {

  const int unroll = 4;
  const long max_gvl = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
  const int loops = n/max_gvl;

  __epi_1xf64 v_a = __builtin_epi_vbroadcast_1xf64(a, max_gvl);

  for (int i = 0; i < loops; i += unroll) {
    __epi_1xf64 v_dx0  = __builtin_epi_vload_1xf64(&dx[(i+0)*max_gvl], max_gvl);
    __epi_1xf64 v_dy0  = __builtin_epi_vload_1xf64(&dy[(i+0)*max_gvl], max_gvl);
    __epi_1xf64 v_dx1  = __builtin_epi_vload_1xf64(&dx[(i+1)*max_gvl], max_gvl);
    __epi_1xf64 v_dy1  = __builtin_epi_vload_1xf64(&dy[(i+1)*max_gvl], max_gvl);
    __epi_1xf64 v_dx2  = __builtin_epi_vload_1xf64(&dx[(i+2)*max_gvl], max_gvl);
    __epi_1xf64 v_dy2  = __builtin_epi_vload_1xf64(&dy[(i+2)*max_gvl], max_gvl);
    __epi_1xf64 v_dx3  = __builtin_epi_vload_1xf64(&dx[(i+3)*max_gvl], max_gvl);
    __epi_1xf64 v_dy3  = __builtin_epi_vload_1xf64(&dy[(i+3)*max_gvl], max_gvl);
    __epi_1xf64 v_res0 = __builtin_epi_vfmacc_1xf64(v_dy0, v_a, v_dx0, max_gvl);
    __epi_1xf64 v_res1 = __builtin_epi_vfmacc_1xf64(v_dy1, v_a, v_dx1, max_gvl);
    __epi_1xf64 v_res2 = __builtin_epi_vfmacc_1xf64(v_dy2, v_a, v_dx2, max_gvl);
    __epi_1xf64 v_res3 = __builtin_epi_vfmacc_1xf64(v_dy3, v_a, v_dx3, max_gvl);
    __builtin_epi_vstore_1xf64(&dy[(i+0)*max_gvl], v_res0, max_gvl);
    __builtin_epi_vstore_1xf64(&dy[(i+1)*max_gvl], v_res1, max_gvl);
    __builtin_epi_vstore_1xf64(&dy[(i+2)*max_gvl], v_res2, max_gvl);
    __builtin_epi_vstore_1xf64(&dy[(i+3)*max_gvl], v_res3, max_gvl);
  }

  long gvl = 0; 
  int start_remain = n - ((loops%unroll)*max_gvl);
  for (int i = start_remain; i < n; ) {
    gvl = __builtin_epi_vsetvl(n - i, __epi_e64, __epi_m1);
    __epi_1xf64 v_dx  = __builtin_epi_vload_1xf64(&dx[i], gvl);
    __epi_1xf64 v_dy  = __builtin_epi_vload_1xf64(&dy[i], gvl);
    __epi_1xf64 v_res = __builtin_epi_vfmacc_1xf64(v_dy, v_a, v_dx, gvl);
    __builtin_epi_vstore_1xf64(&dy[i], v_res, gvl);
    i += gvl;
  }

}
