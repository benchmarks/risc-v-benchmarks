#!/bin/bash
source ../../environment.sh

export VEHAVE_DEBUG_LEVEL=0
export VEHAVE_TRACE_SINGLE_THREAD=1
export VEHAVE_TRACE=1
export VEHAVE_USE_PERF_COUNTERS=0

traces_dir="traces"
mkdir -p ${traces_dir}

num_elements=4096
#num_elements=32768

for VECTOR_SIZE in 128; do # 256 512 1024 2048 4096 8192 16384; do
  #Set VEHAVE variables for specific run
  export VEHAVE_VECTOR_LENGTH=${VECTOR_SIZE} #max vector length in bits
  export TRACE_NAME=axpy_${VECTOR_SIZE}bitsVL_${num_elements}elems
  export VEHAVE_TRACE_FILE=${TRACE_NAME}.trace #Name of the output vehave trace

  #Prepare log file
  log_file="${traces_dir}/trace_${VECTOR_SIZE}.log"
  echo -n "Tracing vector size ${VECTOR_SIZE} at " | tee ${log_file}
  /bin/date | tee -a ${log_file}

  #Run binary
  echo The preload lib is: ${VEHAVE_LIB_SO}
  LD_PRELOAD=${VEHAVE_LIB_SO} $* ${num_elements} 5 | tee -a ${log_file}

  ${VEHAVE_TO_PRV} --output-dir ${traces_dir} --output-name ${TRACE_NAME} $VEHAVE_TRACE_FILE | tee -a ${log_file}
  rm ${VEHAVE_TRACE_FILE}
  gzip ${traces_dir}/${TRACE_NAME}.prv
done

