# Axpy

This benchmark runs several AXPY operations. AXPY is a Level 1 operation in the
Basic Linear Algebra Subprograms (BLAS) package, and is a common operation in
computations with vector processors.  The algorithm is a combination of scalar
multiplication and vector addition.


## How to Build

You can build binnaries by executing the `make` command. By default this
command will build a set of pre-configured program's versions. You may also
generate specific target version by specifying the parameter `target` as
described in the *Available Versions* section. For instance:

```bash
$ make rvv
```

will build the RISC-V vector extension versions as configured in your setup
files.


## How to Execute

You can run the binaries providing the following parameters:

```bash
$ ./bin/<axpy-version> size
```

Where:
- __size__: is the array size, in number of elements.

The folder also includes a `run.sh` script that executes the benchmark
according with certain configurations.

## Available Versions

| Target      | Versions                                            |
| ----        | ----                                                |
| base        | scalar, autovec, autovec-il04                       |
| omp         | simd                                                |
| cblas       | _\<default\>_                                       |
| rvv (0.7)   | _\<default\>_, ur02-il02, ur04, ur04-il04, ur04tail |
| rvv (1.0)   |                                                     |
| bmetal      | mc                                                  |


Some versions could be non-compatible with the compiler and selected options as
they are configured in the setup files (see the root README.md file). In these
cases, either the build process will not generate the binary, or the compilation
process will fail. Check your compiler manual and/or the setup files accordingly.

### Bare-metal version (additional info)

The bare-metal version is generated according with the setup and Makefile
configurations. You can override that configuration by using the
`RVB_BMETAL_VER`, `BM_CORES` and `BM_SIZE` environment variables. For instance,
if you want to generate the `ariane` version, using `2` cores, with an array
size of `512` you should run:

```bash
$ make bmetal BM_SIZE=512 BM_CORES=2 RVB_BMETAL_VER=ariane
```

Bare-metal version does not accept any kind of execution parameters, so the
information in provided by the input data header.  The input data header (ie,
the `dataset.h` file) is generated using the `gendata.pl` perl script as
configured in the Makefile. 

The generated binary will include the number of _cores_ (if required), the
bare-metal _version_, and the compile-time configured _size_ as part of its
name. For instance:

```bash
$ axpy-bmetal-mc-2-coyote-1024
```

is a bare-metal and multi-core version, using 2 cores, for the coyote
simulator, and computing array sizes of 1024 elements.

