#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

for i_size in `seq 5 11`; do
  size=$(( 2 ** i_size))
  ARGS="${size} 8"
  for version in scalar_vanilla intrinsic_vanilla autovectorization_ompsimd_vanilla; do
    BINARY=`pwd`/bin/jacobi2d_${version}
    export PREFIX_VEHAVE_TRACE_FILE=traces/jacobi2d_${version}-${size}_elements
    echo "Executing ${BINARY} ${ARGS}"
    ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
  done
done
