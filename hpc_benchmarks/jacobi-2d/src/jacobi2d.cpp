/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* jacobi-2d.c: this file is part of PolyBench/C */

#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "compare_results.h"
#include "elapsed_time.h"

using namespace std;
#define DATA_TYPE double

extern void kernel_jacobi_2d_vector(int tsteps,int n, DATA_TYPE **A,DATA_TYPE **B);

/* Array initialization. */
static void init_array (int n, DATA_TYPE **A, DATA_TYPE **B)
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      {
      A[i][j] = ((DATA_TYPE) i*(j+2) + 2) / n;
      B[i][j] = ((DATA_TYPE) i*(j+3) + 3) / n;
      }
}

static void kernel_jacobi_2d(int tsteps,int n, DATA_TYPE **A,DATA_TYPE **B)
{
  int t, i, j;
  for (t = 0; t < tsteps; t++)
    {
      for (i = 1; i < n - 1; i++)
       for (j = 1; j < n - 1; j++)
         B[i][j] = (0.2) * (A[i][j] + A[i][j-1] + A[i][1+j] + A[1+i][j] + A[i-1][j]);
      for (i = 1; i < n - 1; i++)
       for (j = 1; j < n - 1; j++)
         A[i][j] = (0.2) * (B[i][j] + B[i][j-1] + B[i][1+j] + B[1+i][j] + B[i-1][j]);
    }
}


int main(int argc, char** argv)
{
  if(argc!=3){
        printf("Usage: pathfiner width N TSTEPS\n");
        exit(0);
    }

    int N = atoi(argv[1]);
    int TSTEPS = atoi(argv[2]);
  /* Retrieve problem size. */
  int n = N;
  int tsteps = TSTEPS;

  /* Variable declaration/allocation. */
  DATA_TYPE** A = (DATA_TYPE **)malloc(n * sizeof(DATA_TYPE *));
  DATA_TYPE** B = (DATA_TYPE **)malloc(n * sizeof(DATA_TYPE *));
  for (int i = 0; i < n; i++) {
    A[i] = (DATA_TYPE *)malloc(n * sizeof(DATA_TYPE));
    B[i] = (DATA_TYPE *)malloc(n * sizeof(DATA_TYPE));
  }
  /* Initialize array(s). */
  init_array(n, A, B);

#ifdef USE_VECTOR_RISCV
  DATA_TYPE** A_vec = (DATA_TYPE **)malloc(n * sizeof(DATA_TYPE *));
  DATA_TYPE** B_vec = (DATA_TYPE **)malloc(n * sizeof(DATA_TYPE *));
  for (int i = 0; i < n; i++) {
    A_vec[i] = (DATA_TYPE *)malloc(n * sizeof(DATA_TYPE));
    B_vec[i] = (DATA_TYPE *)malloc(n * sizeof(DATA_TYPE));
  }
  /* Initialize array(s). */
  init_array(n, A_vec, B_vec);
#endif

  long long start = get_time();
  kernel_jacobi_2d(tsteps, n, A, B);
  long long end = get_time();

#ifndef USE_VECTOR_RISCV
  printf("time(scalar/ref): %lf\n", elapsed_time(start, end));
#endif

#ifdef RESULT_PRINT
  output_printfile(n,A, outfilename );
#endif

#ifdef USE_VECTOR_RISCV
  long long start_vector = get_time();
  kernel_jacobi_2d_vector(tsteps, n, A_vec, B_vec);
  long long end_vector = get_time();
  printf("time(opt/vector): %lf\n", elapsed_time(start_vector, end_vector));
  for (int i = 0; i < n; i++) {
    compare_array_double(A[i], A_vec[i], n);
    compare_array_double(B[i], B_vec[i], n);
    free(A_vec[i]);
    free(B_vec[i]);
  }
  free(A_vec);
  free(B_vec);
#endif

  for (int i = 0; i < n; i++) {
    free(A[i]);
    free(B[i]);
  }
  free(A);
  free(B);

  return 0;
}
