/************************************************************************/
// RISC-V VECTOR Version by Cristóbal Ramírez Lazo, "Barcelona 2019"
#ifdef USE_VECTOR_RISCV
#include "vector_defines.h"
#endif
/************************************************************************/


#define DATA_TYPE double

void kernel_jacobi_2d_intrinsics(int tsteps,int n, DATA_TYPE **A,DATA_TYPE **B){
    _MMR_f64    xU;
    _MMR_f64    xUtmp;
    _MMR_f64    xUleft;
    _MMR_f64    xUright;
    _MMR_f64    xUtop;
    _MMR_f64    xUbottom;
    _MMR_f64    xConstant;

    double diff, sum=0.0;
    unsigned long int izq,der;
    int size_y = n-2;
    int size_x = n-2;

    unsigned long int gvl = __builtin_epi_vsetvl(size_y, __epi_e64, __epi_m1);

    xConstant = _MM_SET_f64(0.20,gvl);

    for (int j=1; j<=size_x; j=j+gvl) 
    {
        gvl = __builtin_epi_vsetvl(size_y-j+1, __epi_e64, __epi_m1);

        xU = _MM_LOAD_f64(&A[1][j],gvl);
        xUtop = _MM_LOAD_f64(&A[0][j],gvl);
        xUbottom = _MM_LOAD_f64(&A[2][j],gvl);

        for (int i=1; i<=size_y; i++) 
        {
            if(i!=1)
            {
                xUtop = xU;
                xU =  xUbottom;
                xUbottom =  _MM_LOAD_f64(&A[i+1][j],gvl);
            }
            izq = *(unsigned long int*)&A[i][j-1]; 
            der = *(unsigned long int*)&A[i][j+gvl];
            xUleft = _MM_VSLIDE1UP_f64(xU,izq,gvl);
            xUright = _MM_VSLIDE1DOWN_f64(xU,der,gvl);
            xUtmp = _MM_ADD_f64(xUleft,xUright,gvl);
            xUtmp = _MM_ADD_f64(xUtmp,xUtop,gvl);
            xUtmp = _MM_ADD_f64(xUtmp,xUbottom,gvl);
            xUtmp = _MM_ADD_f64(xUtmp,xU,gvl);
            xUtmp = _MM_MUL_f64(xUtmp,xConstant,gvl);
            _MM_STORE_f64(&B[i][j], xUtmp,gvl);
        }
    }
    FENCE();
}

void kernel_jacobi_2d_vector(int tsteps,int n, DATA_TYPE **A,DATA_TYPE **B){
  int t, i, j;
    for (t = 0; t < tsteps; t++)
    {
      kernel_jacobi_2d_intrinsics(tsteps,n, A,B);
      kernel_jacobi_2d_intrinsics(tsteps,n, B,A);
    }
}
