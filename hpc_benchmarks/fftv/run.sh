#!/bin/bash

usage() {
	echo "Must specifiy running mode, either \"native\" or \"emulate\"."
	echo "Usage: ./run.sh <native/emulate>"
  exit -1
}

if [ $# -ne 1 ]; then usage; fi

if [[ "$1" == "emulate" ]]; then
	export VEHAVE_TRACE_SINGLE_THREAD=1;
	export VEHAVE_DEBUG_LEVEL=0 
	export VEHAVE_TRACE=0 
	export VEHAVE_VECTOR_LENGTH=16384 
	prefix=/apps/riscv/vehave/EPI-0.7/development/bin/vehave
elif [[ "$1" != "native" ]]; then
  usage
fi

echo -e "size,fftv_cyc,fftv_ins,fftw_cyc,fftw_ins,fftv-s_cyc,fftv-s_ins"
for isize in `seq 3 20`; do
	size=$(( 2 ** ${isize}))
	output=$(${prefix} ./bin/fftv_vec.x ${size} 1)
  exit_code=$?
  if [ ${exit_code} -ne 0 ]; then
    echo "Failed execution."
    exit 1
  fi

  v_t=$(echo "${output}" | grep "fftv cycles" | awk '{print $3}')
	v_i=$(echo "${output}" | grep "fftv inst" | awk '{print $3}')
	w_t=$(echo "${output}" | grep "fftw cycles" | awk '{print $3}')
	w_i=$(echo "${output}" | grep "fftw inst" | awk '{print $3}')

	output=$(./bin/fftv_scalar.x ${size} 1)
  exit_code=$?
  if [ ${exit_code} -ne 0 ]; then
    echo "Failed execution."
    exit 1
  fi

  s_t=$(echo "${output}" | grep "fftv cycles" | awk '{print $3}')
	s_i=$(echo "${output}" | grep "fftv inst" | awk '{print $3}')
	echo -e "${size},${v_t},${v_i},${w_t},${w_i},${s_t},${s_i}"
done
