# FFTV

This folder contains the submodule FFTV, a FFT library optimized for long vector architectures with automatic vectorization capabilities.

# Compilation

You can build two targets, `make fftv` and `make fftv-fftw`, with the second one also compiling FFTW for validation and performance comparison purposes.

# Running

The `./run.sh` takes one parameter with two possible values, `./run.sh native` or `./run.sh emulated`. On the FPGA, run with "native", and on the Arriesgado cluster, run with "emulated". The `run.sh` script will execute multiple FFTs of different sizes and parse the results for you, but feel free to execute it yourself.

After compiling, the binaries are found in `./bin/fftv_scalar.x` and `./bin/fftv_vec.x`, and they can be run like this:

```
./fftv_[scalar/vec].x N dir
```
Where:
- `N` is the size of the transform in double-precision elements.
- `dir` is either 1 or 0 (forward or backward/inverse transform).

When `dir` is 1 (forward transform), `N` can be any arbitrary size greater than 0, but the performance of the transform is degraded when `N` cannot be factorized into 2,3,4,5,6,8 (powers of 2, 4 or 8 are recommended for best performance and vectorization).

When `dir` is 0 (inverse transform), `N` **must** be able to be factorized into 2,3,4,5,6,8. Arbitrary size inverse transform are still WIP.


