#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <assert.h>
#include "utils.h"
#include "somier.h"
#include "elapsed_time.h"
#include "compare_results.h"


double Xcenter[3];

double dt=0.001;   // 0.1;
double spring_K=10.0;
double M=1.0;
int err;


int main(int argc, char *argv[])
{
   int nt;
   int ntsteps = 20;
   int N;

   if (argc !=3) {
      printf("Usage: somier steps N(elements of each dimension)\n");
      exit(1);
   }

   ntsteps  = atoi(argv[1]);  //TODO: better parsing
   N       = atoi(argv[2]);  //TODO: better parsing
   printf ("Problem size = %d, steps = %d\n", N, ntsteps);

   double (*X)[N][N][N];
   double (*V)[N][N][N];
   double (*A)[N][N][N];
   double (*F)[N][N][N];
   double (*F_ref)[N][N][N];

   X      = malloc(3*sizeof (*X));
   V      = malloc(3*sizeof (*V));
   A      = malloc(3*sizeof (*A));
   F      = malloc(3*sizeof (*F));
   F_ref  = malloc (3*sizeof (*F_ref));

   clear_4D(N, F);
   clear_4D(N, A);
   clear_4D(N, V);
   init_X(N, X);

   printf("Execute scalar/ref  version\n");
   printf("Set initial speed\n");
   V[0][N/2][N/2][N/2] = 0.1;  V[1][N/2][N/2][N/2] = 0.1;  V[2][N/2][N/2][N/2] = 0.1;

   long long start,end;
   start = get_time();
   for (nt=0; nt <ntsteps-1; nt++) {
      if(nt%10 == 0) {
        print_state (N, X, Xcenter, nt);
      }
      Xcenter[0]=0, Xcenter[1]=0; Xcenter[2]=0;   //reset aggregate stats
      clear_4D(N, F_ref);
      compute_forces(N, X, F_ref);
      acceleration(N, A, F_ref, M);
      velocities(N, V, A, dt);
      positions(N, X, V, dt);
      compute_stats(N, X, Xcenter);
   }
   end = get_time();
   printf("somier_scalar time: %f\n", elapsed_time(start, end));

#if defined(RVB_USE_RVV) || defined(RVB_USE_OMP)
   clear_4D(N, F);
   clear_4D(N, A);
   clear_4D(N, V);
   init_X(N, X);

   #if RVB_USE_RVV
   printf("Execute intrinsic vectorial version\n");
   #elif RVB_USE_OMP
   printf("Execute autovectorization vectorial version\n");
   //printf("No yet implemented\n");
   //exit(1);
   #endif

   printf("Set initial speed\n");
   V[0][N/2][N/2][N/2] = 0.1;  V[1][N/2][N/2][N/2] = 0.1;  V[2][N/2][N/2][N/2] = 0.1;
   start = get_time();
   for (nt=0; nt <ntsteps-1; nt++) {
      if(nt%10 == 0) {
        print_state (N, X, Xcenter, nt);
      }
      Xcenter[0]=0, Xcenter[1]=0; Xcenter[2]=0;   //reset aggregate stats
      clear_4D(N, F);
      #if RVB_USE_RVV
      compute_forces_prevec(N, X, F);
      accel_intr  (N, A, F, M);
      vel_intr  (N, V, A, dt);
      pos_intr  (N, X, V, dt);
      #elif RVB_USE_OMP
      compute_forces_prevec(N, X, F);
      accel_autovec  (N, A, F, M);
      velocities_autovec  (N, V, A, dt);
      positions_autovec  (N, X, V, dt);
      #endif
      compute_stats(N, X, Xcenter);
   }
   end = get_time();

   #if RVB_USE_RVV
   printf("somier intrinsics time: %f\n", elapsed_time(start, end));
   #elif RVB_USE_OMP
   printf("somier autovectorization time: %f\n", elapsed_time(start, end));
   #endif
   printf("Comparing result with reference version.\n");
   compare_array_double(F, F_ref, N*N*N);
#endif
}

