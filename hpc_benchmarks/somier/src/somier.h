extern double M;
extern double dt;
extern double spring_K;
extern double Xcenter[3];

extern void init_X (int n, double (*X)[n][n][n]);

#ifndef RVB_USE_BMETAL
extern void compute_forces(int n, double (*X)[n][n][n], double (*F)[n][n][n]);
extern void compute_forces_prevec(int n, double (*X)[n][n][n], double (*F)[n][n][n]);

extern void acceleration(int n, double (*A)[n][n][n], double (*F)[n][n][n], double M);
extern void velocities(int n, double (*V)[n][n][n], double (*A)[n][n][n], double dt);
extern void positions(int n, double (*X)[n][n][n], double (*V)[n][n][n], double dt);

extern void compute_stats(int n, double (*X)[n][n][n], double Xcenter[3]);

#else
extern void compute_stats_pre(int cid, int nc, int n, double (*X)[n][n][n]);
extern void init_forloop_boundaries (int cid,int nc, int dim );

extern void compute_forces(int cid, int nc, int n, double (*X)[n][n][n], double (*F)[n][n][n]);

extern void acceleration(int cid, int nc, int n, double (*A)[n][n][n], double (*F)[n][n][n], double M);
extern void velocities(int cid, int nc, int n, double (*V)[n][n][n], double (*A)[n][n][n], double dt);
extern void positions(int cid, int nc, int n, double (*X)[n][n][n], double (*V)[n][n][n], double dt);

extern void compute_stats(int cid, int nc, int n, double (*X)[n][n][n], double Xcenter[3]);

extern void force_contribution(int cid, int nc, int n, double (*X)[n][n][n], double (*F)[n][n][n],
                   int i, int j, int k, int neig_i, int neig_j, int neig_k);
#endif

#if defined(RVB_USE_RVV) || defined(RVB_USE_OMP)
#if RVB_USE_RVV
extern inline void force_contr_vec(int n, double (*X)[n][n][n], double (*F)[n][n][n], int i, int j, int neig_i, int neig_j);
extern void accel_intr(int n, double (*A)[n][n][n], double (*F)[n][n][n], double M);
extern void vel_intr(int n, double (*V)[n][n][n], double (*A)[n][n][n], double dt);
extern void pos_intr(int n, double (*X)[n][n][n], double (*V)[n][n][n], double dt);
#elif RVB_USE_OMP
extern void accel_autovec(int n, double (*A)[n][n][n], double (*F)[n][n][n], double M);
extern void velocities_autovec(int n, double (*V)[n][n][n], double (*A)[n][n][n], double dt);
extern void positions_autovec(int n, double (*X)[n][n][n], double (*V)[n][n][n], double dt);
#endif
#endif

