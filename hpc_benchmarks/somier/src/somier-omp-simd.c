#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <assert.h>
#include "somier.h"

inline void accel_autovec(int n, double (*A)[n][n][n], double (*F)[n][n][n], double M)
{
   int i, j, k;
   for (i = 0; i<n; i++)
      for (j = 0; j<n; j++)
         #pragma omp simd
         for (k = 0; k<n; k++) {
            A[0][i][j][k]= F[0][i][j][k]/M;
            A[1][i][j][k]= F[1][i][j][k]/M;
            A[2][i][j][k]= F[2][i][j][k]/M;
	 }

}

inline void velocities_autovec(int n, double (*V)[n][n][n], double (*A)[n][n][n], double dt)
{
   int i, j, k;
//#dear compiler: please fuse next two loops if you can 
   for (i = 0; i<n; i++)
      for (j = 0; j<n; j++) {
         #pragma omp simd
         for (k = 0; k<n; k++) {
               V[0][i][j][k] += A[0][i][j][k]*dt;
               V[1][i][j][k] += A[1][i][j][k]*dt;
               V[2][i][j][k] += A[2][i][j][k]*dt;
            }
     }
}


void positions_autovec(int n, double (*X)[n][n][n], double (*V)[n][n][n], double dt)
{
   int i, j, k;
//#dear compiler: please fuse next two loops if you can 
   for (i = 0; i<n; i++)
      for (j = 0; j<n; j++)
         #pragma omp simd
         for (k = 0; k<n; k++) {
               X[0][i][j][k] += V[0][i][j][k]*dt;
               X[1][i][j][k] += V[1][i][j][k]*dt;
               X[2][i][j][k] += V[2][i][j][k]*dt;
            }
}


//reference scalar code 
void force_contr_autovec(int n, double (*X)[n][n][n], double (*F)[n][n][n], int i, int j, int neig_i, int neig_j)
{
   double dx, dy, dz, dl, spring_F, FX, FY,FZ;

   double local_spring_K = spring_K;
   #pragma omp simd
   for (int k=1; k<n-1; k++) {
      dx=X[0][neig_i][neig_j][k]-X[0][i][j][k];
      dy=X[1][neig_i][neig_j][k]-X[1][i][j][k];
      dz=X[2][neig_i][neig_j][k]-X[2][i][j][k];
      dl = sqrt(dx*dx + dy*dy + dz*dz);
      spring_F = 0.25 * local_spring_K*(dl-1);
      FX = spring_F * dx/dl; 
      FY = spring_F * dy/dl;
      FZ = spring_F * dz/dl; 
      F[0][i][j][k] += FX;
      F[1][i][j][k] += FY;
      F[2][i][j][k] += FZ;
   }
}


void k_force_contr_autovec(int n, double (*X)[n][n][n], double (*F)[n][n][n], int i, int j)
{
   double dx, dy, dz, dl, spring_F, FX, FY,FZ;

   double local_spring_K = spring_K;
   #pragma omp simd
   for (int k=1; k<n-1; k++) {
      dx=X[0][i][j][k-1]-X[0][i][j][k];
      dy=X[1][i][j][k-1]-X[1][i][j][k];
      dz=X[2][i][j][k-1]-X[2][i][j][k];
      dl = sqrt(dx*dx + dy*dy + dz*dz);
      spring_F = 0.25 * local_spring_K*(dl-1);
      FX = spring_F * dx/dl; 
      FY = spring_F * dy/dl;
      FZ = spring_F * dz/dl; 
      F[0][i][j][k] += FX;
      F[1][i][j][k] += FY;
      F[2][i][j][k] += FZ;
      dx=X[0][i][j][k+1]-X[0][i][j][k];
      dy=X[1][i][j][k+1]-X[1][i][j][k];
      dz=X[2][i][j][k+1]-X[2][i][j][k];
      dl = sqrt(dx*dx + dy*dy + dz*dz);
      spring_F = 0.25 * local_spring_K*(dl-1);
      FX = spring_F * dx/dl; 
      FY = spring_F * dy/dl;
      FZ = spring_F * dz/dl; 
      F[0][i][j][k] += FX;
      F[1][i][j][k] += FY;
      F[2][i][j][k] += FZ;
   }
}

void compute_forces_prevec(int n, double (*X)[n][n][n], double (*F)[n][n][n])
{
   for (int i=1; i<n-1; i++) {
      for (int j=1; j<n-1; j++) {
            force_contr_autovec (n, X, F, i, j, i,   j+1);  
            force_contr_autovec (n, X, F, i, j, i-1, j  );   
            force_contr_autovec (n, X, F, i, j, i+1, j  );   
            force_contr_autovec (n, X, F, i, j, i,   j-1);  
            k_force_contr_autovec (n, X, F, i, j);
      }
   }
}

