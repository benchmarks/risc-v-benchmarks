// See LICENSE for license details.
//--------------------------------------------------------------------------
// Includes 
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include "custom_def.h"

//--------------------------------------------------------------------------
// Input/Reference Data
double dt=0.001;
double spring_K=10.0;
double M=1.0;
int err;

#include "dataset.h"
#include "somier.h"

//--------------------------------------------------------------------------
// Basic Utilities and Multi-thread Support
#include "util.h"

//#define  VERYFY_RESULT  
//--------------------------------------------------------------------------
#define VERYFY_RESULT

#ifdef VERYFY_RESULT
    #include "double_cmp.h"
    extern int  __attribute__((noinline)) mt_verify(const size_t coreid, const size_t ncores,const size_t lda , double* test,   double* verify );
#endif

void somier_scalar (int cid, int nc)
{
   compute_forces(cid, nc, N, X, F);
   // if(cid==0) printf("S1\n");
   BARRIER();

   acceleration(cid, nc, N, A, F, M);
   // if(cid==0) printf("S2\n");
   BARRIER();

   velocities(cid, nc, N, V, A, dt);
   //  if(cid==0) printf("S3\n");
   BARRIER();

   positions(cid, nc, N, X, V, dt);
   //  if(cid==0) printf("S4\n");
   BARRIER();

   compute_stats_pre(cid, nc, N, X);
   //  if(cid==0) printf("S5\n");
   BARRIER();

   compute_stats(cid, nc, N, X, Xcenter);
   //   if(cid==0) printf("S6\n");
   BARRIER();
}



void print_duble (double f,char * u){
   int t =  f;
   float t2 = (f*1000)-(t*1000);    
   int t1 = t2;
   printf("%u.%u%s",t,t1,u);    
}


//--------------------------------------------------------------------------
// Main
//
// all threads start executing thread_entry(). Use their "coreid" to
// differentiate between threads (each thread is running on a separate core).
int MAIN()
{  
   INIT_CID();

   if(nc> BM_CORES)    nc =  BM_CORES;  

   if(cid<nc) init_forloop_boundaries (cid,nc, N);   

   if(cid==0) {
      printf("We are %d cores.\n",nc);
      Xcenter[0]=0, Xcenter[1]=0; Xcenter[2]=0;      
   }   

   BARRIER();
   stats(somier_scalar(cid,nc);BARRIER(),N);
   BARRIER();

#ifdef VERYFY_RESULT   
   if(cid == 0) { 
      printf ("\tV=");
      print_duble(V[0][N/2][N/2][N/2],","),
	 print_duble(V[1][N/2][N/2][N/2],","),
	 print_duble(V[2][N/2][N/2][N/2],"\t\t X= "),
	 print_duble(X[0][N/2][N/2][N/2],","),
	 print_duble(X[1][N/2][N/2][N/2],","),
	 print_duble(X[2][N/2][N/2][N/2],"\n");           
   }
#endif            

   exit(0);    
}
