# Somier

## Usage
```
usage: ./bin/somier-scalar nsteps N
usage: ./bin/somier-rvv0.7 nsteps N

example:
  ./bin/somier-rvv0.7 4 64
```


## Available Versions

| Target      | Versions                                            |
| ----        | ----                                                |
| base        | scalar, autovec                                     |
| omp         | simd                                                |
| cblas       |                                                     |
| rvv (0.7)   | _\<default\>_                                       |
| rvv (1.0)   |                                                     |
| bmetal      | mc                                                  |


Some versions could be non-compatible with the compiler and selected options as
they are configured in the setup files (see the root README.md file). In these
cases, either the build process will not generate the binary, or the compilation
process will fail. Check your compiler manual and/or the setup files accordingly.


### Bare-metal version (additional info)

The bare-metal version is generated according with the setup and Makefile
configurations. You can override that configuration by using the
`RVB_BMETAL_VER`, `BM_CORES` and `BM_SIZE` environment variables. For instance,
if you want to generate the `ariane` version, using `2` cores, with a matrix
size of `512` (i.e., 512x512x512) you should run:

```bash
$ make bmetal BM_SIZE=512 BM_CORES=2 RVB_BMETAL_VER=ariane
```

Bare-metal version does not accept any kind of execution parameters, so the
information in provided by the input data header.  The input data header (ie,
the `dataset.h` file) is generated using the `gendata.pl` perl script as
configured in the Makefile. 

The generated binary will include the number of _cores_ (if required), the
bare-metal _version_, and the compile-time configured _size_ as part of its
name. For instance:

```bash
$ somier-bmetal-mc-2-coyote-512
```

is a bare-metal and multi-core version, using 2 cores, for the coyote
simulator, and computing 3D matrices of 512x512x512 elements.

