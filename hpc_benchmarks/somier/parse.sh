#!/bin/bash

echo -n "`basename $1`," #version
echo -n "`echo  $2 | awk '{print $1}'`," #nreps
echo -n "`echo  $2 | awk '{print $2}'`," #size
echo "$3" | grep time | tail -n 1 | awk '{ print $3 }' #time
