#!/bin/bash
source ../../environment.sh
source ${RVB_RUN_DIR}/run_main.sh $*

mode=$1;

export PARSING_FILE="`pwd`/parse.sh"

# Options:
versions="scalar autovect rvv0.7" 
sizes=`seq 16 2 32`
niters=4

for size in ${sizes}; do
  ARGS="${niters} ${size}"
  for version in ${versions}; do
      BINARY=`pwd`/bin/somier-${version}
      export PREFIX_VEHAVE_TRACE_FILE=traces/somier_${version}-${size}_elements
      echo "Executing ${BINARY} ${ARGS}"
      ${RVB_RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
  done
done
