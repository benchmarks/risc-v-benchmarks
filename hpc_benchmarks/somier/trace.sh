#!/bin/bash
source ../../environment.sh

VEHAVE_DEBUG_LEVEL=0
traces_dir="traces"
mkdir -p ${traces_dir}

app=./bin/somier-rvv0.7
N=4
niters=4

for VECTOR_SIZE in 128 256 512 1024 2048 4096; do
	export VEHAVE_VECTOR_LENGTH=${VECTOR_SIZE}
	log_file="${traces_dir}/trace_${VECTOR_SIZE}.log"
	echo -n "Tracing vector size ${VECTOR_SIZE} at " > ${log_file}
	/bin/date >> ${log_file}
	LD_PRELOAD=${VEHAVE_LIB_SO} ./${app} ${niters} ${N} &>> ${log_file}
	raw_trace=$(ls -tr | tail -n 1)
	${VEHAVE_TO_PRV} --output-dir ${traces_dir} --output-name somier_vec_${VECTOR_SIZE}_4_20 ${raw_trace} >> ${log_file}
done

