#!/bin/bash

INPUT_PATH=/home/pmtest/epi/ftp/sdvs/integration-functional-tests/resources/tests/spmv/spmv_inputs

usage() {
	echo "Must specifiy running mode, either \"native\" or \"emulate\"."
	echo "Usage: ./run.sh <native/emulate>"
	exit -1
}

if [ $# -ne 1 ]; then usage; fi

if [[ "$1" == "emulate" ]]; then
	export VEHAVE_TRACE_SINGLE_THREAD=1;
	export VEHAVE_DEBUG_LEVEL=0 
	export VEHAVE_TRACE=0 
	export VEHAVE_VECTOR_LENGTH=16384 
	prefix=/apps/riscv/vehave/EPI-0.7/development/bin/vehave
elif [[ "$1" != "native" ]]; then
  usage
fi

echo -e "dataset,ref_time,ref_gflops,vec_time,vec_gflops"
for cage in 6 7 8 9 10
do
	output=$(${prefix} ./build/bin/spmv_sellcslib -v 256 -m ${INPUT_PATH}/cage${cage}/cage${cage}.mtx)
  exit_code=$?
  if [ ${exit_code} -ne 0 ]; then
    echo "Failed execution."
    exit 1
  fi

	ref_t=$(echo "${output}" | grep "Reference version execution time" | awk '{print $NF}' | cut -d ',' -f1)
	ref_g=$(echo "${output}" | grep "Reference version *FLOPS" | awk '{print $NF}' | cut -d ',' -f1)
	vec_t=$(echo "${output}" | grep "sellcslib version execution time" | awk '{print $NF}' | cut -d ',' -f1)
	vec_g=$(echo "${output}" | grep "sellcslib version *FLOPS" | awk '{print $NF}' | cut -d ',' -f1)
	echo -e "cage${cage},$ref_t,$ref_g,$vec_t,$vec_g"
done
