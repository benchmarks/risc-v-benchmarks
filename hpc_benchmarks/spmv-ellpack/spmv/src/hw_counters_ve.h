/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/


#ifndef _HW_COUNTERS_H_
#define _HW_COUNTERS_H_

#include <stdint.h>
#include <stdio.h>


typedef struct ve_pmc_t
{
    int num_of_events;
    char **labels;
    int *codes;
    long long *counter_values;
    double start_time_usec;
    double total_time_usec; 
} ve_pmc_t;

uint64_t * ve_init_hw_counters();
void ve_hw_counters_start(uint64_t *restrict events);
void ve_hw_counters_stop(uint64_t *restrict events);
void ve_print_hardware_counters(const char *section, const uint64_t *ev);


#endif /* _HW_COUNTERS_H_ */
