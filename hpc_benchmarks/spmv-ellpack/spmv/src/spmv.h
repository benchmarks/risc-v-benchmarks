/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _SPMV_H_
#define _SPMV_H_
#include "sparse_matrix.h"
#include "macros.h"

extern size_t align_sz;

void run_spmv_ref_test(const SparseMatrixCSR * matrix, const elem_t *__restrict x,
                          elem_t *__restrict y_ref, double *__restrict elapsed_times, int num_iterations);

int run_custom_spmv_test(const SparseMatrixCSR * matrix, const elem_t *__restrict x, elem_t *__restrict y,
                          const elem_t *__restrict y_ref, double *__restrict elapsed_times, int num_iterations);

void print_additional_custom_report(char * text_padding, double *elapsed_times);


#endif
