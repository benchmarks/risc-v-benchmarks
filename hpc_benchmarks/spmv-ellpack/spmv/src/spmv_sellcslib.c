/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mytimer.h"
#include "sellcs-spmv.h"
#include "sparse_matrix.h"
#include "utils.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(USE_OMP)
    #include "omp.h"
    #if defined(ENABLE_DFC)
        #define ALG_VERSION "sellcslib_DFC_omp"
    #else
        #define ALG_VERSION "sellcslib_omp"
    #endif
#else
    #if defined(ENABLE_DFC)
        #define ALG_VERSION "sellcslib_DFC"
    #else
        #define ALG_VERSION "sellcslib"
    #endif
#endif

char algorithm_version[30] = ALG_VERSION;

uint64_t sigma_window = 16384;
size_t chunk_size = 16;
size_t num_blocks = 1;
int32_t max_vlen = 64;

int run_custom_spmv_test(const SparseMatrixCSR* restrict csr_matrix, const elem_t* restrict x, elem_t* restrict y,
    const elem_t* restrict y_ref, double* restrict elapsed_times, int num_iterations)
{
    double tmp_time;
    int is_correct = -1;

    // Init data structures
    sellcs_matrix_t sellcs_matrix;
    int32_t res = sellcs_init_params(max_vlen, sigma_window, &sellcs_matrix);

    // Convert CSR to sell-c-sigma
    tmp_time = mytimer();
    res = sellcs_create_matrix_from_CSR_rd(csr_matrix->nrows, csr_matrix->ncolumns, (index_t*)csr_matrix->row_pointers,
        (index_t*)csr_matrix->column_indices, csr_matrix->values, 0, 0, &sellcs_matrix);
    elapsed_times[3] = mytimer() - tmp_time;

    elapsed_times[2] = 0.0;

    // Auto-tune execution parameters
    sellcs_analyze_matrix(&sellcs_matrix, 0);

    for (uint64_t i = 0; i < num_iterations; i++) {
        tmp_time = mytimer();
        // Execute SpMV
        sellcs_execute_mv_d(&sellcs_matrix, x, y);
        elapsed_times[2] += mytimer() - tmp_time;
    }
    elapsed_times[2] = elapsed_times[2] / num_iterations;

#ifdef SPMV_VERIFY
    // Verify that the result vector 'y' is correct
    is_correct = validate_vector(y, y_ref, sellcs_matrix.nrows);
#endif

    return is_correct;
}

void print_additional_custom_report(char* text_padding, double* elapsed_times)
{
    printf("%s\"Row order sigma window\": %lu,\n", text_padding, sigma_window);
    printf("%s\"Task Size\": %lu\n", text_padding, chunk_size);
}
