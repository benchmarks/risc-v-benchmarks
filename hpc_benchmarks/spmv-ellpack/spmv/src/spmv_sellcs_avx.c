/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include <immintrin.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../libsellcs-spmv/sellcs-spmv.h"
#include "assert.h"
#include "macros.h"
#include "mytimer.h"
#include "sparse_matrix.h"
#include "utils.h"

#ifdef PAPI
    #include <papi.h>
#endif

#if defined(USE_OMP)
    #include "omp.h"
    #define ALG_VERSION "sellcs_avx_omp"
#else
    #define ALG_VERSION "sellcs_avx"
#endif

char algorithm_version[30] = ALG_VERSION;

size_t sigma_window = 16384;
size_t chunk_size = 16;
size_t num_blocks = 1;
int32_t max_vlen = 8;

void kernel_sellcs_avx(const sellcs_matrix_t* restrict matrix,
    const elem_t* restrict x,
    elem_t* restrict y,
    const size_t start_slice,
    const size_t end_slice)
{
    const uint32_t vlen = matrix->C;

    // WARNING:
    // This version does not handle the extra rows at the end of the last slice.
    // OVERFLOW ROWS WILL WRITE TRASH AT 0 index. Todo in the future.
    for (size_t slice_idx = start_slice; slice_idx < end_slice; slice_idx++) {
        index_t row_idx = slice_idx << 3;

        elem_t* values_pointer = &matrix->values[matrix->slice_pointers[slice_idx]];
        index_t* colidx_pointer = &matrix->column_indices[matrix->slice_pointers[slice_idx]];

        __m512d tmp_results = { 0, 0, 0, 0, 0, 0, 0, 0 };
        index_t swidth = matrix->slice_widths[slice_idx];

        __m512i y_sc_idx = _mm512_load_epi64(&matrix->row_order[row_idx]);

        for (size_t i = 0; i < swidth; i++) {
            // Load values and column indices
            __m512d values_vblock = _mm512_load_pd(values_pointer);
            __m512i col_index_vblock = _mm512_load_epi64(colidx_pointer);
            // Gather x
            __m512d x_vblock = _mm512_i64gather_pd(col_index_vblock, x, 8);
            // Multiply and add
            tmp_results = _mm512_fmadd_pd(x_vblock, values_vblock, tmp_results);

            values_pointer += matrix->C;
            colidx_pointer += matrix->C;
        }

        _mm512_i64scatter_pd(y, y_sc_idx, tmp_results, 8);
    }
}

int run_custom_spmv_test(const SparseMatrixCSR* restrict csr_matrix, const elem_t* restrict x,
    elem_t* restrict y, const elem_t* restrict y_ref, double* restrict elapsed_times, int num_iterations)
{
    double tmp_time;
    int is_correct = -1;

#ifdef PAPI

    // Initialize library at start of execution
    PAPI_library_init(PAPI_VER_CURRENT);

    // Start measurements
    int eventset = PAPI_NULL, code;
    PAPI_create_eventset(&eventset);

    PAPI_event_name_to_code("PAPI_DP_OPS", &code);
    PAPI_add_event(eventset, code);

#endif

    // Init data structures
    sellcs_matrix_t sellcs_matrix;
    int32_t res = sellcs_init_params(max_vlen, sigma_window, &sellcs_matrix);

    // Convert CSR to sell-c-sigma
    tmp_time = mytimer();
    res = sellcs_create_matrix_from_CSR_rd(csr_matrix->nrows, csr_matrix->ncolumns, (index_t*)csr_matrix->row_pointers,
        (index_t*)csr_matrix->column_indices, csr_matrix->values, 0, 0, &sellcs_matrix);
    elapsed_times[3] = mytimer() - tmp_time;

    elapsed_times[2] = 0.0;

    // Auto-tune execution parameters
    // sellcs_analyze_matrix(&sellcs_matrix, 0);

#ifdef PAPI
    PAPI_start(eventset);
#endif

    for (uint64_t i = 0; i < num_iterations; i++) {
        tmp_time = mytimer();
        // Execute SpMV
        kernel_sellcs_avx(&sellcs_matrix, x, y, 0, sellcs_matrix.nslices);
        elapsed_times[2] += mytimer() - tmp_time;
    }
    elapsed_times[2] = elapsed_times[2] / num_iterations;

#ifdef PAPI

    long long values[0];
    PAPI_stop(eventset, values);
    fprintf(stderr, "%-24s = %8llu\n", "PAPI_DP_OPS per iteration", values[0] / num_iterations);

#endif 

#ifdef SPMV_VERIFY
    // Verify that the result vector 'y' is correct
    is_correct = validate_vector(y, y_ref, sellcs_matrix.nrows);
    printf("Warning: In this implementation a number or rows not multiple of the VLEN will write trash in y[0]. Do not worry its irrelevant to study performance. \nThis can be fixed by adding code to handle differently the last slice.");
#endif

    return is_correct;
}

void print_additional_custom_report(char* text_padding, double* elapsed_times)
{
    printf("%s\"Row order sigma window\": %lu,\n", text_padding, sigma_window);
    printf("%s\"Task Size\": %lu\n", text_padding, chunk_size);
}
