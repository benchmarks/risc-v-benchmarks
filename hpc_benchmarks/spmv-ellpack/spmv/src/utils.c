/* Author: Constantino Gómez, 2020
 *
 * Licensed to the Barcelona Supercomputing Center (BSC) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The BSC licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "utils.h"
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <inttypes.h>
#include <string.h>
#include "macros.h"
#include <assert.h>

int validate_vector(const elem_t *y, const elem_t *y_ref, const size_t size)
{
    /*
        Input two vectors that are supposed to have the same content.
        Useful to compare floating point accuracy deviation.
        If the different 
    */
    int debug = 1;
    int is_100_correct = 1, is_good_enough = 1, correct = 0;
    elem_t abs_diff, rel_diff;
    elem_t threshold = 1e-7;
    int max_errors = 50;
    int err_count = 0;

    int v = 0;
    while (v < size)
    {
        correct = y_ref[v] == y[v];

        if (!correct || (y[v] != y[v])) // if y is NaN y != y will be true
        {
            is_100_correct = 0;

            abs_diff = y_ref[v] - y[v];
            rel_diff = abs_diff / y_ref[v];
            if (fabs(rel_diff) > threshold || (y[v] != y[v]))
            {
                is_good_enough = 0;
                fprintf(stderr, "Warning: element in y[%d] has a relative diff = <%f>!\n", v, rel_diff);
                if (debug)
                    fprintf(stderr, "   yref: <%g> != <%g>\n", y_ref[v], y[v]);

                err_count++;
                if (err_count > max_errors)
                    return 0;
            }
        }

        v++;
    }

    // Return 2 if 100% correct, 1 if good enough, 0 if not correct
    return is_100_correct + is_good_enough;
}

void init_x(elem_t *x, size_t n, int test_case)
{
    size_t i;
    switch (test_case)
    {
    case 1:
        for (i = 0; i < n; i++)
            x[i] = 3.0;
        break;
    case 2:
        for (i = 0; i < n; i++)
            x[i] = (elem_t) 1.0;
            // x[i] = (elem_t)i + 1;
           
        break;
    default:
        printf("Unexpected X Initialization\n");
        exit(-1);
    }
}

void memset_float(elem_t *vec, float value, size_t n)
{
    for (size_t i = 0; i < n; i++)
        vec[i] = value;
}

void print_arr_uint(size_t N, char *name, size_t *vector)
{
    size_t i;
    fprintf(stderr, "\nPrinting vector: %s\n", name);
    for (i = 0; i < N; i++)
    {
        if (!i % 30)
            fprintf(stderr, "\n");
        fprintf(stderr, "%lu, ", vector[i]);
    }
    fprintf(stderr, "\n");
}

void print_arr_float(size_t N, char *name, elem_t *vector)
{
    size_t i;
    printf("\nPrinting vector: %s", name);
    for (i = 0; i < N; i++)
    {
        if (!i % 30)
            printf("\n");
        printf("%g, ", vector[i]);
    }
    printf("\n");
}

void check_mem_alloc(void *ptr, const char *err_msg)
{
    if (ptr == NULL)
    {
        fprintf(stderr, "Memory Allocation Error: could not allocate %s. Application will exit.", err_msg);
        exit(1);
    }
}


size_t get_multiple_of_align_size(size_t size)
{
    // Returns the closest multiple of the <align_size> to <size>
    size_t padded_size = ((align_sz - (size % align_sz)) % align_sz) + size;

#ifdef SPMV_DEBUG
    fprintf(stderr, "Allocating [%" PRIu64 "] bytes, should be aligned to: [%" PRIu64 "]\n", padded_size, align_sz);
#endif

    return padded_size;
}
