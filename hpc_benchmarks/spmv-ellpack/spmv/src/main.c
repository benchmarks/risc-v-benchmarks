/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mytimer.h"
#include "report_results.h"
#include "sparse_matrix.h"
#include "spmv.h"
#include "utils.h"
#include <getopt.h> // Required if using -std=cXX
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// extern enum MatrixFormat required_matrix_format;
extern size_t sigma_window;
extern size_t num_blocks;
extern size_t chunk_size;
extern int32_t max_vlen;


#ifdef ALIGN_TO
size_t align_sz = ALIGN_TO;
#else
size_t align_sz = 64;
#endif

void print_usage()
{
    printf("Usage: ./spmv [-p <size> | -m <mtx filepath>]\n");
}

int main(int argc, char** argv)
{
    char* mtx_filepath = "";
    int opt, verification;
    int num_iterations = 10;
    double elapsed_times[15];
    elem_t *y_ref, *y, *x;
    size_t pentadiagonal_size = 0, x_size = 0, y_size = 0;
    int synth_col_pattern = 1;
    double sparsity = 0;
    SparseMatrixCOO tmp_matrix;
    SparseMatrixCSR csr_matrix;

    while ((opt = getopt(argc, argv, ":i:m:p:s:x:y:r:w:b:c:v:")) != -1) {
        switch (opt) {
        case 'i':
            num_iterations = atoi(optarg);
            break;
        case 'm':
            mtx_filepath = optarg;
            break;
        case 'p':
            pentadiagonal_size = atoi(optarg);
            break;
        case 's':
            sparsity = atof(optarg);
            break;
        case 'x':
            x_size = atoi(optarg);
            break;
        case 'y':
            y_size = atoi(optarg);
            break;
        case 'r':
            synth_col_pattern = atoi(optarg);
            break;
        case 'w':
            sigma_window = atoi(optarg);
            break;
        case 'b':
            num_blocks = atoi(optarg);
            break;
        case 'c':
            chunk_size = atoi(optarg);
            break;
        case 'v':
            max_vlen = atoi(optarg);
            break;
        case ':':
            printf("Missing value in option \'%c\'\n", optopt);
            print_usage();
            exit(1);
        case '?':
            printf("Unknown option \'%c\'\n", optopt);
            break;
        }
    }

    /*  In case we deal with extra arguments in the future:

        for(; optind < argc; optind++){
            printf(“extra arguments: %s\n”, argv[optind]);
        } 
    */

    /*  Loading input matrix */
    elapsed_times[0] = mytimer();
    if (strcmp(mtx_filepath, "")) {
        /* If a Sparse matrix in MTX format is provided, transform it to CSR. */
        fast_load_from_mtx_file(mtx_filepath, &tmp_matrix);
        convert_coo_to_csr(&tmp_matrix, &csr_matrix, 0);

    } else if (pentadiagonal_size) {
        /* load_pentadiagonal(pentadiagonal_size, &the_matrix); */
        load_octadiagonal(pentadiagonal_size, &csr_matrix);
    } else if (sparsity) {
        /* Create synthetic matrix */
        generate_sparse_matrix(x_size, y_size, sparsity, synth_col_pattern, &csr_matrix);
    } else {
        printf("You must specify a valid input method\n");
        print_usage();
        exit(1);
    }

    /* Allocate and initialize the x vector */
    x = (elem_t*)aligned_alloc(align_sz, csr_matrix.ncolumns * sizeof(elem_t));
    check_mem_alloc(x, "X vector");
    init_x(x, csr_matrix.ncolumns, 2);

    /* Allocate and initialize the y_ref vector */
    y_ref = (elem_t*)aligned_alloc(align_sz, csr_matrix.nrows * sizeof(elem_t));
    memset_float(y_ref, 0.0, csr_matrix.nrows);
    check_mem_alloc(y_ref, "y_ref vector");

    elapsed_times[0] = mytimer() - elapsed_times[0];

    /*  Execute the reference SPMV for timing and validation purposes.
     *  Post: y_ref contains the reference solution of vector y.
     */
    run_spmv_ref_test(&csr_matrix, x, y_ref, elapsed_times, 1);

    /* Run your custom SPMV implementation */
    y = (elem_t*)aligned_alloc(align_sz, csr_matrix.nrows * sizeof(elem_t));
    check_mem_alloc(y, "y vector");
    memset_float(y, 0.0, csr_matrix.nrows);

    verification = run_custom_spmv_test(&csr_matrix, x, y, y_ref, elapsed_times, num_iterations);

    // HW_COUNTERS(print_hardware_counters("spmv_reference", &hw_counters));
    ReportResults(&csr_matrix, verification, elapsed_times, num_iterations);

    return 0;
}
