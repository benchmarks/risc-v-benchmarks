/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _HW_COUNTERS_H_
#define _HW_COUNTERS_H_

#include <stdint.h>
#include <stdio.h>

extern char algorithm_version[30];

typedef struct papi_events_t
{
    int num_of_events;
    char **labels;
    int *codes;
    long long *counter_values;
    double start_time_usec;
    double total_time_usec; 
} papi_events_t;

void init_hw_counters(papi_events_t *ev);
void add_hw_counter(papi_events_t *ev, const char *label, const int papi_code);
void hw_counters_start(papi_events_t *events);
void hw_counters_stop(papi_events_t *events);
void handle_error(int ret, const char *location);
void print_hardware_counters(const char *section, const papi_events_t *ev);

#endif /* _HW_COUNTERS_H_ */
