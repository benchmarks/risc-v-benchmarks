/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _REPORT_RESULTS_H_
#define _REPORT_RESULTS_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sparse_matrix.h"

extern char algorithm_version[30];
extern size_t align_sz;


void ReportResults(const SparseMatrixCSR *matrix, int verification, double *elapsed_times,
                   int num_iterations);
#endif /* _REPORT_RESULTS_H_ */
