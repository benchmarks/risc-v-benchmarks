/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef SPARSE_MATRIX_H
#define SPARSE_MATRIX_H

#include <stdlib.h>
#include <stdint.h>
#include "typedefs.h"
 
extern size_t align_sz;
// typedef double elem_t;

enum MatrixFormat
{
    COO,
    CSR,
    CSC,
    CSRSIMD,
    ELLPACK,
    VBSF
};

struct SparseMatrixCSR_STRUCT
{
    char *name;
    elem_t *values; // values of matrix entries
    size_t *column_indices;
    size_t *row_pointers;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
};
typedef struct SparseMatrixCSR_STRUCT SparseMatrixCSR;

struct SparseMatrixELLPACK_STRUCT
{
    char *name;
    elem_t *values;
    size_t *column_indices;
    size_t max_row_size;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
};
typedef struct SparseMatrixELLPACK_STRUCT SparseMatrixELLPACK;

struct SparseMatrixSELLCS_STRUCT
{
    char *name;
    elem_t *values;
    size_t *column_indices;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
    size_t C;
    size_t sigma;
    size_t nslices;
    size_t *slice_widths;
    size_t *slice_pointers;
    size_t *row_order;
    uint8_t *vop_lengths;
    size_t *vop_pointers; // slice widths and vop_pointers can be merged, to express both; just extend vop_pointers size by 1 as row_pointers in csr is nrows + 1;
};
typedef struct SparseMatrixSELLCS_STRUCT SparseMatrixSELLCS;

struct SparseMatrixSELLCSBLOCK_STRUCT
{
    char *name;
    elem_t *values;
    size_t *column_indices;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
    size_t C;
    size_t sigma;
    size_t nblocks;
    size_t nslices;
    size_t *slice_widths;
    size_t *slice_pointers;
    size_t *block_pointers;
    size_t *row_order;
    elem_t *y_tmp;
    uint8_t *vop_lengths;
    size_t *vop_pointers;
    size_t *slice_block;
};
typedef struct SparseMatrixSELLCSBLOCK_STRUCT SparseMatrixSELLCSBLOCK;

struct SparseMatrixCSC_STRUCT
{
    char *name;
    elem_t *values; // values of matrix entries
    size_t *row_indices;
    size_t *column_pointers;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
};
typedef struct SparseMatrixCSC_STRUCT SparseMatrixCSC;

struct SparseMatrixCSRSIMD_STRUCT
{
    char *name;
    elem_t *values; // values of matrix entries
    size_t *row_pointers;
    size_t *column_indices;
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
};
typedef struct SparseMatrixCSRSIMD_STRUCT SparseMatrixCSRSIMD;

struct SparseMatrixCOO_STRUCT
{
    char *name;
    elem_t *values;  // values of matrix entries
    size_t *rows;    // row_index
    size_t *columns; // col_index
    size_t nrows;
    size_t ncolumns;
    size_t nnz;
};
typedef struct SparseMatrixCOO_STRUCT SparseMatrixCOO;

// void load_from_mtx_file(const char *mtx_filepath, SparseMatrixCOO *coo_matrix);
void fast_load_from_mtx_file(const char *mtx_filepath, SparseMatrixCOO *coo_matrix);
void order_coo_by_column(const SparseMatrixCOO *coo_matrix, SparseMatrixCOO *ordered_coo_matrix);
void convert_coo_to_csr(const SparseMatrixCOO *coo_matrix, SparseMatrixCSR *csr_matrix, int free_coo);
void convert_coo_to_csc(const SparseMatrixCOO *coo_matrix, SparseMatrixCSC *csc_matrix);
void convert_csr_to_csrsimd(const SparseMatrixCSR *csr_matrix, SparseMatrixCSRSIMD *csrsimd_matrix, int segment_len);
void convert_csr_to_ellpack(const SparseMatrixCSR *csr_matrix, SparseMatrixELLPACK *ellpack_matrix, const uint32_t order_by_row_size, const size_t *__restrict row_order);

void convert_csr_to_sellcsb_dfc(const SparseMatrixCSR *csr_matrix,
                                SparseMatrixSELLCSBLOCK *sellcsb_matrix,
                                size_t **__restrict row_size_per_block,
                                size_t **__restrict row_order_per_block);

void load_pentadiagonal(size_t n, SparseMatrixCSR *csr_matrix);
void load_octadiagonal(size_t n, SparseMatrixCSR *csr_matrix);
void generate_sparse_matrix(size_t nrows, size_t ncols, double sparsity, int col_dist, SparseMatrixCSR *csr_matrix);
void print_full_csr_matrix(const SparseMatrixCSR *csr_matrix);

#endif // SPARSE_MATRIX_H
