/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef SORTING_UTILS_H
#define SORTING_UTILS_H

#include "sparse_matrix.h"

extern size_t align_sz;

size_t *get_order_by_row_size_radix(size_t *rows_size, const size_t nrows, size_t sigma_ordering_window);


#endif // SORTING_UTILS_H
