/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "spmv.h"
#include "mytimer.h"

void spmv_ref(const elem_t *__restrict a, const size_t *__restrict ia, const size_t *__restrict ja,
              const elem_t *__restrict x, elem_t *__restrict y, const size_t nrows)
{
  size_t row, idx;
  for (row = 0; row < nrows; row++)
  {
    elem_t sum = 0.0;
    for (idx = ia[row]; idx < ia[row + 1]; idx++)
    {
      sum += a[idx] * x[ja[idx]];
    }
    y[row] = sum;
  }
}

void run_spmv_ref_test(const SparseMatrixCSR * matrix, const elem_t *__restrict x,
                       elem_t *__restrict y_ref, double *__restrict elapsed_times, int num_iterations)
{
  double tmp_time, accumulated_time = 0.0;
  

  /* Warm-up */
  spmv_ref(matrix->values, matrix->row_pointers, matrix->column_indices, x, y_ref, matrix->nrows);

  for (int i = 0; i < num_iterations; i++)
  {
    // HW_COUNTERS(hw_counters_start(&hw_counters));
    tmp_time = mytimer();
    spmv_ref(matrix->values, matrix->row_pointers, matrix->column_indices, x, y_ref, matrix->nrows);
    accumulated_time += mytimer() - tmp_time;
    // HW_COUNTERS(hw_counters_stop(&hw_counters));
  }

  elapsed_times[1] = accumulated_time / num_iterations;
}
