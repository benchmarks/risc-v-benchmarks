#include "mytimer.h"
#include "spmv.h"
#include "utils.h"
#include <mkl.h>
#include <mkl_spblas.h>
#include <stdint.h>
#include <stdio.h>

#ifdef PAPI
    #include <papi.h>
#endif

enum MatrixFormat required_matrix_format = CSR;
char algorithm_version[30] = "mkl";

const size_t unroll = 256;
size_t sigma_window = 8192;
size_t chunk_size = 64 * unroll;
size_t num_blocks = 1;
int32_t max_vlen = 8;

int run_custom_spmv_test(const SparseMatrixCSR* restrict matrix, const elem_t* restrict x,
    elem_t* restrict y, const elem_t* restrict y_ref, double* elapsed_times, int num_iterations)
{
    int stat, is_correct = -1, i;
    sparse_matrix_t A, A_BSR;
    struct matrix_descr descr_A;
    double tmp_time, accumulated_time = 0;

#ifdef PAPI

    // Initialize library at start of execution
    PAPI_library_init(PAPI_VER_CURRENT);

    // Start measurements
    int eventset = PAPI_NULL, code;
    PAPI_create_eventset(&eventset);

    PAPI_event_name_to_code("PAPI_DP_OPS", &code);
    PAPI_add_event(eventset, code);

#endif

    /* MKL_INT will be 32- or 64- depending if MKL_ILP64 is defined
       32-bit mode might cause overflow issues if matrix dimensions are massive*/

    MKL_INT *int_row_pointers, *int_row_column_indices;

    descr_A.type = SPARSE_MATRIX_TYPE_GENERAL;

    if (matrix->nrows > 2 << (32 - 2)) {
        printf("Error: Matrix num rows (%lu) exceeds the limit (%d).", matrix->nrows, 2 << (32 - 2));
        exit(1);
    }

    /*  CSR structures conversion to MKL internal format.
        row_pointers and col_indices from uint64_t to int32_t or int64_t. */

    int_row_pointers = malloc((matrix->nrows + 1) * sizeof(MKL_INT));
    int_row_column_indices = malloc(matrix->nnz * sizeof(MKL_INT));

    for (i = 0; i < matrix->nrows + 1; i++) {
        int_row_pointers[i] = matrix->row_pointers[i];
    }

    for (i = 0; i < matrix->nnz; i++) {
        int_row_column_indices[i] = matrix->column_indices[i];
    }

    // Set MKL_INT = 64-bit signed int
    mkl_set_interface_layer(MKL_INTERFACE_ILP64);

    // Set type of threading
    mkl_set_threading_layer(MKL_THREADING_INTEL);

    // Create MKL matrix data structure (in their, presumably custom, CSR format)
    stat = mkl_sparse_d_create_csr(&A, SPARSE_INDEX_BASE_ZERO, matrix->nrows, matrix->ncolumns,
        int_row_pointers, int_row_pointers + 1,
        int_row_column_indices, matrix->values);
    
#ifdef PAPI
    PAPI_start(eventset);
#endif

    // Run MKL
    for (i = 0; i < num_iterations; i++) {
        memset_float(y, 0.0, matrix->nrows);
        tmp_time = mytimer();
        mkl_sparse_d_mv(SPARSE_OPERATION_NON_TRANSPOSE, 1.0, A, descr_A, x, 1.0, y);
        accumulated_time += mytimer() - tmp_time;
    }

#ifdef PAPI

    long long values[0];
    PAPI_stop(eventset, values);
    fprintf(stderr, "%-24s = %8llu\n", "PAPI_DP_OPS per iteration", values[0] / num_iterations);

#endif 

    elapsed_times[2] = accumulated_time / num_iterations;


#ifdef SPMV_VERIFY
    is_correct = validate_vector(y, y_ref, matrix->nrows);
#endif

    // elapsed_times[3] = accumulated_time / num_iterations;

    return is_correct;
}

void print_additional_custom_report(char* text_padding, double* elapsed_times)
{
    // printf("%s\"mkl (BSR) version execution time [s]\": %f,\n", text_padding, elapsed_times[3]);
}
