/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "sorting_utils.h"
#include "utils.h"
#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

/* Vector RADIX SORT */
uint64_t get_max_value(size_t* order_vector, size_t start, size_t end)
{
    uint64_t max_val = order_vector[start];
    for (size_t i = start + 1; i < end; i++)
        if (order_vector[i] > max_val)
            max_val = order_vector[i];

    return max_val;
}

const uint64_t nbins = 16; // depends on base_p2
const uint64_t base_p2 = 4; // we will use 2^4 bins
const uint64_t base_mod_mask = 0xF; // depends on base_p2

void reverse_count_sort_p2(size_t* order_vector, size_t* paired_vector, size_t start, size_t end, uint64_t exp)
{
    const size_t len = end - start;
    size_t tmp_array[len];
    size_t tmp_array_paired[len];

    size_t* bins = (size_t*)aligned_alloc(align_sz, get_multiple_of_align_size(nbins * sizeof(size_t)));
    memset(bins, 0, get_multiple_of_align_size(nbins * sizeof(size_t)));
    // Histogram of the values in NNZ
    for (size_t i = start; i < end; i++) {
        size_t bin_id = (order_vector[i] >> exp) & base_mod_mask;
        bins[bin_id]++;
    }
    // Indexes to insert values in reverse
    size_t cum_sum = 0;
    for (size_t i = 0; i < nbins; i++) {
        cum_sum += bins[i];
        bins[i] = cum_sum;
    }
    for (size_t i = start; i < end; i++) {
        size_t bin_id = (order_vector[i] >> exp) & base_mod_mask;
        // Reverse the write index (position)
        size_t write_at_index = len - bins[bin_id];
        tmp_array[write_at_index] = order_vector[i];
        tmp_array_paired[write_at_index] = paired_vector[i];
        bins[bin_id]--;
    }
    memcpy(&order_vector[start], tmp_array, len * sizeof(size_t));
    memcpy(&paired_vector[start], tmp_array_paired, len * sizeof(size_t));

    free(bins);
}

void radix_sort_paired_descending(size_t* order_vector, size_t* paired_vector, size_t start, size_t end)
{
    // assert(end-start % 256);
    uint64_t maxvalue = get_max_value(order_vector, start, end);
    uint64_t max_num_digits = 0;
    while (maxvalue) {
        max_num_digits++;
        maxvalue = maxvalue >> base_p2;
    }

    size_t exp = 0;
    for (uint64_t i = 0; i < max_num_digits; i++) {
        reverse_count_sort_p2(order_vector, paired_vector, start, end, exp);
        exp += base_p2;
    }
}

//////





void reverse_count_sort_p2_opt(size_t* order_vector, size_t* paired_vector, size_t start, size_t end, uint64_t exp,
    size_t* bins, size_t* tmp_array, size_t* tmp_array_paired)
{
    const size_t len = end - start;
    // size_t tmp_array[len];
    // size_t tmp_array_paired[len];
    memset(bins, 0, nbins * sizeof(size_t));

    // Histogram of the values in NNZ
    for (size_t i = start; i < end; i++) {
        size_t bin_id = (order_vector[i] >> exp) & base_mod_mask;
        bins[bin_id]++;
    }
    // Indexes to insert values in reverse
    size_t cum_sum = 0;
    for (size_t i = 0; i < nbins; i++) {
        cum_sum += bins[i];
        bins[i] = cum_sum;
    }
    for (size_t i = start; i < end; i++) {
        const size_t bin_id = (order_vector[i] >> exp) & base_mod_mask;
        // Reverse the write index (position)
        const size_t write_at_index = len - bins[bin_id];
        tmp_array[write_at_index] = order_vector[i];
        tmp_array_paired[write_at_index] = paired_vector[i];
        bins[bin_id]--;
    }
    memcpy(&order_vector[start], tmp_array, len * sizeof(size_t));
    memcpy(&paired_vector[start], tmp_array_paired, len * sizeof(size_t));
}




void radix_sort_paired_descending_opt(size_t* order_vector, size_t* paired_vector, size_t start, size_t end)
{

    size_t* bins = (size_t*)aligned_alloc(align_sz, get_multiple_of_align_size(nbins * sizeof(size_t)));

    // assert(end-start % 256);
    uint64_t maxvalue = get_max_value(order_vector, start, end);
    uint64_t max_num_digits = 0;
    while (maxvalue) {
        max_num_digits++;
        maxvalue = maxvalue >> base_p2;
    }
    size_t tmp_array[end - start];
    size_t tmp_array_paired[end - start];

    size_t exp = 0;
    for (uint64_t i = 0; i < max_num_digits; i++) {
        reverse_count_sort_p2_opt(order_vector, paired_vector, start, end, exp, bins, tmp_array, tmp_array_paired);
        exp += base_p2;
    }

    free(bins);

}

size_t* get_order_by_row_size_radix(size_t* rows_size, const size_t nrows, const size_t sigma_ordering_window)
{
    // Post: rows_size is ordered paired with row_order which contains the offset index (between 0 and sigma_ordering_window-1)
    //       corresponding to the order inside each 'ordering window'.

    size_t* row_order = (size_t*)aligned_alloc(align_sz, get_multiple_of_align_size(nrows * sizeof(size_t)));
    check_mem_alloc(row_order, "get_order_by_row_size.row_order\n");


    // Set a block of row_order between 0 and vlen-1
    for (size_t i = 0; i < nrows; i++)
        row_order[i] = i;

    #pragma omp parallel for schedule(dynamic,1)
    for (size_t k = 0; k < nrows; k += sigma_ordering_window) {
        size_t row_end = (k + sigma_ordering_window > nrows) ? nrows : k + sigma_ordering_window;
        // size_t sort_size = row_end - k;
        radix_sort_paired_descending_opt(&rows_size[0], &row_order[0], k, row_end);
    }

    return row_order;
}

