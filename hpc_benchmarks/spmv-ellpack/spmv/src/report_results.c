/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "report_results.h"
#include <stdio.h>
#include <time.h>
#include "spmv.h"

#if defined(USE_OMP)
#include <omp.h> // enables use of get_num_cpus
#endif


void ReportResults(const SparseMatrixCSR *matrix, int verification, double *elapsed_times,
                   int num_iterations)
{
    char date_and_time[30];
    char verify_str[10];
    unsigned int num_threads = 1;

    /* Get current date and time */
    time_t t = time(NULL);
    struct tm *tm_info;
    tm_info = localtime(&t);
    strftime(date_and_time, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    /* Verification test */
    if (verification == -1)
        strcpy(verify_str, "Disabled");
    else if (verification > 0)
        strcpy(verify_str, "Pass");
    else
        strcpy(verify_str, "FAIL");

    /* Fetch the number of threads used by the OMP/OmpSs runtime */
#if defined(USE_OMPSS2)
    num_threads = nanos6_get_num_cpus();
#elif defined(USE_OMP)
    num_threads = omp_get_max_threads();
#endif

    /*
        Compute Performance Statistics
    */

    double num_fp_ops_sparsemv = 2.0 * matrix->nnz; // For each NNZ we perform 1 multiply and 1 add

    /*  
        Note: This is REFERENCE Memory RD/WR count (not necessarily from/to main memory) 
        Not representative of the actual number of load/store instructions executed 
        (use Hardware counters for that).
    */
    /*
        Note about reads:
        @HPCG
        fp_num_reads_sparsemv = (nnz reads of values + nnz reads of indices + nrow reads of x ) 

        There is something off here, I think it should be:
            fp_num_reads_sparsemv = nnz * ( 1x read in <values>, 1x read <indices>, 1x read in <x>) + numrows reads of <row_ptr>

    */
    double num_fp_reads_sparsemv = matrix->nnz * (sizeof(elem_t) + sizeof(size_t)) +
                                   matrix->nrows * sizeof(elem_t); // 1 SpMV with nnz reads of values, nnz reads indices, plus nrow reads of x

    double num_fp_writes_sparsemv = matrix->nrows * sizeof(elem_t); // nrows * 1x WR result in y[]

		#ifdef USE_RV_CYCLE_COUNTER
			#define GIGA 1 //To report Flop/C instead of Gflop/C
			#define BW "B/c"
			#define PERF "FLOPS/c"
			#define TIME "c"
		#else
			#define GIGA 1.0E9
			#define BW "GB/s"
			#define PERF "GFLOPS/s"
			#define TIME "s"
		#endif
    
    /*      Dump report in JSON format to stdout    */
    printf("{\n");
    printf("    \"Benchmark\": \"SPMV EPI\",\n");
    printf("    \"Algorithm version\": \"%s\",\n", algorithm_version);
    printf("    \"Execution date\": \"%s\",\n", date_and_time);
    printf("    \"Verification test\": \"%s\",\n", verify_str);
    printf("    \"Number of threads\": %u,\n", num_threads);

    printf("    \"Problem summary\": {\n");
    printf("        \"Input name\": \"%s\",\n", matrix->name);
    printf("        \"Matrix Num. Rows\": %lu,\n", matrix->nrows);
    printf("        \"Matrix Num. Columns\": %lu,\n", matrix->ncolumns);
    printf("        \"Total non-zero elements\": %lu,\n", matrix->nnz);
    printf("        \"Non-zero elements per row\": %lu,\n", matrix->nnz / matrix->nrows);
    printf("        \"Num. averaged iterations\": %d\n", num_iterations);
    printf("    },\n");

    printf("    \"Memory statistics\": { \n");
    printf("        \"Total memory allocated [MB]\": %f,\n", 0.0);
    printf("        \"Reference version read BW [" BW "]\": %f,\n", num_fp_reads_sparsemv / elapsed_times[1] / GIGA);
    printf("        \"Reference version read WR [" BW "]\": %f,\n", num_fp_writes_sparsemv / elapsed_times[1] / GIGA);
    printf("        \"%s version read BW [" BW "]\": %f,\n", algorithm_version, 0.0);
    printf("        \"%s version write BW [" BW "]\": %f\n", algorithm_version, 0.0);
    printf("    },\n");

    printf("    \"Performance statistics\": { \n");
    printf("        \"Time allocating and loading data [" TIME "]\": %f,\n", elapsed_times[0]);
    printf("        \"Time converting to %s format [" TIME "]\": %f,\n", algorithm_version, elapsed_times[3]);
    printf("        \"Reference version execution time [" TIME "]\": %f,\n", elapsed_times[1]);
    printf("        \"Reference version " PERF "\": %f,\n", num_fp_ops_sparsemv / elapsed_times[1] / GIGA);
    printf("        \"%s version execution time [" TIME "]\": %f,\n", algorithm_version, elapsed_times[2]);
    printf("        \"%s version " PERF "\": %f\n", algorithm_version, num_fp_ops_sparsemv / elapsed_times[2] / GIGA);
    printf("    },\n");
    // Print additional information report for this particular version
    //
    printf("    \"Version specific stats\": { \n");
    print_additional_custom_report("        ", elapsed_times);
    printf("    }\n");
    printf("}\n");

}
