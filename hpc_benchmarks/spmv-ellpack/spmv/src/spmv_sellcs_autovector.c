/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "assert.h"
#include "macros.h"
#include "mytimer.h"
#include "sellcs-spmv.h"
#include "sorting_utils.h"
#include "spmv.h"
#include "utils.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef PAPI
    #include <papi.h>
#endif


#if defined(USE_OMP)
    #include "omp.h"
    #define ALG_VERSION "sellcs_autovector_omp"
#else
    #define ALG_VERSION "sellcs_autovector"
#endif

enum MatrixFormat required_matrix_format = CSR;

char algorithm_version[30] = ALG_VERSION;

size_t sigma_window = 16384;
size_t chunk_size = 16;
size_t num_blocks = 1;
int32_t max_vlen = 8;

void sellcs_autovector_notinlibrary(const sellcs_matrix_t* matrix,
    const elem_t* restrict x,
    elem_t* restrict y,
    const index_t start_row,
    const index_t end_row)
{
    const uint32_t vlen = matrix->C;
    for (size_t row_idx = start_row; row_idx < end_row; row_idx += vlen) {
        index_t slice_idx = row_idx >> matrix->shift;

        elem_t* values_pointer = &matrix->values[matrix->slice_pointers[slice_idx]];
        index_t* colidx_pointer = &matrix->column_indices[matrix->slice_pointers[slice_idx]];

        index_t swidth = matrix->slice_widths[slice_idx];
        index_t* y_sc_idx = &matrix->row_order[row_idx];

        // Y is initialized to 0
        // Non-intrinsics version
        for (index_t j = 0; j < swidth; j++) {
            for (index_t k = 0; k < vlen; k++) {
                // Load Values and Column indices
                const elem_t aval = values_pointer[k];
                const index_t col_idx = colidx_pointer[k];
                const elem_t xval = x[col_idx];
                // if (j == 0)
                //     fprintf(stderr, "%lu,", y_sc_idx[k]);
                y[y_sc_idx[k]] += xval * aval;
            }
            values_pointer += vlen;
            colidx_pointer += vlen;
        }
        // fprintf(stderr, "\n");
    }
}

int run_custom_spmv_test(const SparseMatrixCSR* restrict csr_matrix, const elem_t* restrict x, elem_t* restrict y,
    const elem_t* restrict y_ref, double* restrict elapsed_times, int num_iterations)
{
    int is_correct = -1;
    double tmp_time;

#ifdef PAPI

    // Initialize library at start of execution
    PAPI_library_init(PAPI_VER_CURRENT);

    // Start measurements
    int eventset = PAPI_NULL, code;
    PAPI_create_eventset(&eventset);

    PAPI_event_name_to_code("PAPI_DP_OPS", &code);
    PAPI_add_event(eventset, code);

#endif

    sellcs_matrix_t sellcs_matrix;
    int32_t res = sellcs_init_params(max_vlen, sigma_window, &sellcs_matrix);

    tmp_time = mytimer();
    res = sellcs_create_matrix_from_CSR_rd(csr_matrix->nrows, csr_matrix->ncolumns, (index_t*)csr_matrix->row_pointers,
        (index_t*)csr_matrix->column_indices, csr_matrix->values, 0, 0, &sellcs_matrix);
    elapsed_times[3] = mytimer() - tmp_time;

    elapsed_times[2] = 0.0;

#if defined(USE_OMP)

    fprintf(stderr, "This version does not exists yet.\n");
    exit(0);
#else

#ifdef PAPI
    PAPI_start(eventset);
#endif

    for (size_t i = 0; i < num_iterations; i++) {
        memset_float(y, 0.0, sellcs_matrix.nrows);
        tmp_time = mytimer();
    #if defined(ENABLE_DFC)
        fprintf(stderr, "This version does not exists yet.\n");
    #else
        sellcs_autovector_notinlibrary(&sellcs_matrix, x, y, 0, sellcs_matrix.nrows);
    #endif

        elapsed_times[2] += mytimer() - tmp_time;
    }

#endif

#ifdef PAPI

    long long values[0];
    PAPI_stop(eventset, values);
    fprintf(stderr, "%-24s = %8llu\n", "PAPI_DP_OPS per iteration", values[0] / num_iterations);

#endif 

    elapsed_times[2] = elapsed_times[2] / num_iterations;



#ifdef SPMV_VERIFY
    is_correct = validate_vector(y, y_ref, sellcs_matrix.nrows);
#endif

    return is_correct;
}

void print_additional_custom_report(char* text_padding, double* elapsed_times)
{
    printf("%s\"Row order sigma window\": %lu,\n", text_padding, sigma_window);
    printf("%s\"Task Size\": %lu\n", text_padding, chunk_size);
}
