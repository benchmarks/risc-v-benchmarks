/* Copyright 2020 Barcelona Supercomputing Center
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "sparse_matrix.h"
#include "macros.h"
#include "mmio.h"
#include "mytimer.h"
#include "utils.h"
#include <inttypes.h>
#include <libgen.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* 
    Loads a Sparse Matrix from a .MTX file format
    into a SparseMatrixCOO data structure.

    More info about the MTX format @ https://math.nist.gov/MatrixMarket/
    Find the biggest MTX repository @ https://sparse.tamu.edu/
*/
void fast_load_from_mtx_file(const char *mtx_filepath, SparseMatrixCOO *coo_matrix)
{

#ifdef SPMV_DEBUG
    double start, elapsed;
    start = mytimer();
#endif

    int ret_code;
    unsigned int mtx_rows, mtx_cols, mtx_entries;
    FILE *f;
    MM_typecode matcode;

    if ((f = fopen(mtx_filepath, "r")) == NULL)
    {
        fprintf(stderr, "Could not open file: %s \n", mtx_filepath);
        exit(1);
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        fprintf(stderr, "Could not process Matrix Market banner.\n");
        exit(1);
    }

    /* 
        This code, will only work with MTX containing: REAL number, Sparse, Matrices.
        Throws an error otherwise. See mmio.h for more information.
    */
    if ((!mm_is_real(matcode) && !mm_is_pattern(matcode)) || !mm_is_matrix(matcode) || !mm_is_sparse(matcode))
    {
        fprintf(stderr, "Market Market type: [%s] not supported\n", mm_typecode_to_str(matcode));
        exit(1);
    }

    /* Get the number of matrix rows and columns */
    if ((ret_code = mm_read_mtx_crd_size(f, &mtx_rows, &mtx_cols, &mtx_entries)) != 0)
    {
        fprintf(stderr, "Error while reading matrix dimension sizes.\n");
        exit(1);
    }

    long current_stream_position = ftell(f);
    fseek(f, 0, SEEK_END);
    long nnz_string_size = ftell(f) - current_stream_position;
    fseek(f, current_stream_position, SEEK_SET); // Leave the pointer where it was before

    char *nnz_string = (char *)malloc(nnz_string_size + 1);
    fread(nnz_string, 1, nnz_string_size, f);
    fclose(f);

    /* Fill COO struct */
    coo_matrix->nrows = mtx_rows;
    coo_matrix->ncolumns = mtx_cols;
    coo_matrix->name = basename((char *)mtx_filepath);

    size_t nnz_count = 0;
    if (mm_is_symmetric(matcode))
    {
        size_t max_entries = 2 * mtx_entries; // 2 * mtx_entries is an upper bound
        coo_matrix->rows = (size_t *)aligned_alloc(align_sz, max_entries * sizeof(size_t));
        check_mem_alloc(coo_matrix->rows, "coo rows");

        coo_matrix->columns = (size_t *)aligned_alloc(align_sz, max_entries * sizeof(size_t));
        check_mem_alloc(coo_matrix->columns, "coo cols");

        coo_matrix->values = (elem_t *)aligned_alloc(align_sz, max_entries * sizeof(elem_t));
        check_mem_alloc(coo_matrix->values, "coo values");

        // Load Symmetric MTX, note that COO might be unordered.
        if (!mm_is_pattern(matcode))
        {
            char *line_ptr = nnz_string;
            char *next_token;

            for (size_t i = 0; i < mtx_entries; i++)
            {
                coo_matrix->rows[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->columns[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->values[nnz_count] = strtod(line_ptr, &next_token);
                line_ptr = next_token;

                if (coo_matrix->values[nnz_count] == 0.0)
                    continue;

                if (coo_matrix->rows[nnz_count] == coo_matrix->columns[nnz_count])
                {
                    nnz_count++;
                }
                else
                {
                    coo_matrix->rows[nnz_count + 1] = coo_matrix->columns[nnz_count];
                    coo_matrix->columns[nnz_count + 1] = coo_matrix->rows[nnz_count];
                    coo_matrix->values[nnz_count + 1] = coo_matrix->values[nnz_count];
                    nnz_count = nnz_count + 2;
                }
            }
        }
        else
        {
            char *line_ptr = nnz_string;
            char *next_token;

            for (size_t i = 0; i < mtx_entries; i++)
            {
                coo_matrix->rows[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->columns[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->values[nnz_count] = 1.0f;
                // fprintf(stderr, " %lu %lu %lf\n", coo_matrix->rows[nnz_count], coo_matrix->columns[nnz_count], coo_matrix->values[nnz_count]);

                if (coo_matrix->values[nnz_count] == 0.0)
                    continue;

                if (coo_matrix->rows[nnz_count] == coo_matrix->columns[nnz_count])
                {
                    nnz_count++;
                }
                else
                {
                    coo_matrix->rows[nnz_count + 1] = coo_matrix->columns[nnz_count];
                    coo_matrix->columns[nnz_count + 1] = coo_matrix->rows[nnz_count];
                    coo_matrix->values[nnz_count + 1] = 1.0f;
                    nnz_count = nnz_count + 2;
                }
            }
        }
    }
    else
    {
        coo_matrix->rows = (size_t *)aligned_alloc(align_sz, mtx_entries * sizeof(size_t));
        check_mem_alloc(coo_matrix->rows, "coo rows");

        coo_matrix->columns = (size_t *)aligned_alloc(align_sz, mtx_entries * sizeof(size_t));
        check_mem_alloc(coo_matrix->columns, "coo cols");

        coo_matrix->values = (elem_t *)aligned_alloc(align_sz, mtx_entries * sizeof(elem_t));
        check_mem_alloc(coo_matrix->values, "coo values");

        if (!mm_is_pattern(matcode))
        {
            char *line_ptr = nnz_string;
            char *next_token;

            for (size_t i = 0; i < mtx_entries; i++)
            {
                coo_matrix->rows[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->columns[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->values[nnz_count] = strtod(line_ptr, &next_token);
                line_ptr = next_token;

                if (coo_matrix->values[nnz_count] != 0.0)
                    nnz_count++;
            }
        }
        else
        {
            char *line_ptr = nnz_string;
            char *next_token;

            for (size_t i = 0; i < mtx_entries; i++)
            {
                coo_matrix->rows[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->columns[nnz_count] = strtoul(line_ptr, &next_token, 10) - 1;
                line_ptr = next_token;
                coo_matrix->values[nnz_count] = 1.0f;

                if (coo_matrix->values[nnz_count] != 0.0)
                    nnz_count++;
            }
        }
    }
    coo_matrix->nnz = nnz_count;
    free(nnz_string);

#ifdef SPMV_DEBUG
    elapsed = mytimer() - start;
    fprintf(stderr, "Elapsed loading to COO =\t %g seconds\n", elapsed);
#endif
}

void order_coo_by_column(const SparseMatrixCOO *coo_matrix, SparseMatrixCOO *ordered_coo_matrix)
{
    size_t *histogram_columns = (size_t *)aligned_alloc(64, coo_matrix->ncolumns * sizeof(size_t));

    ordered_coo_matrix->nnz = coo_matrix->nnz;
    ordered_coo_matrix->nrows = coo_matrix->nrows;
    ordered_coo_matrix->ncolumns = coo_matrix->ncolumns;

    for (size_t i = 0; i < coo_matrix->nnz; i++)
    {
        histogram_columns[coo_matrix->columns[i]]++;
    }

    size_t accum = 0;
    size_t tmp;
    for (size_t i = 0; i < coo_matrix->ncolumns; i++)
    {
        tmp = histogram_columns[i];
        histogram_columns[i] = accum;
        accum += tmp;
    }

    for (size_t i = 0; i < coo_matrix->nnz; i++)
    {
        size_t insert_index = histogram_columns[coo_matrix->columns[i]];

        ordered_coo_matrix->values[insert_index] = coo_matrix->values[i];
        ordered_coo_matrix->columns[insert_index] = coo_matrix->columns[i];
        ordered_coo_matrix->rows[insert_index] = coo_matrix->rows[i];

        histogram_columns[coo_matrix->columns[i]]++;
    }

    // fprintf(stderr, "\n\ncolumns vector:\n");
    // for (size_t i = 0; i < 20; i++)
    // {
    //     fprintf(stderr, "%lu,", ordered_coo_matrix->columns[i]);
    // }

    free(coo_matrix->rows);
    free(coo_matrix->columns);
    free(coo_matrix->values);
}

int check_column_order(const SparseMatrixCOO *matrix)
{
    int is_ordered = 1;
    for (size_t i = 1; i < matrix->nnz; i++)
    {
        if (matrix->columns[i] < matrix->columns[i - 1])
        {
            // fprintf(stderr, "WARNING: Matrix is NOT ordered @ element: %lu, row: %lu, col: %lu\n", i, matrix->rows[i], matrix->columns[i]);
            is_ordered = 0;
            break;
        }
    }

    return is_ordered;
}

void convert_coo_to_csc(const SparseMatrixCOO *coo_matrix, SparseMatrixCSC *csc_matrix)
{
    size_t col_nnz, cum_sum;

#ifdef DEBUG
    double start, elapsed;
    start = mytimer();
#endif

    /* Allocate CSR Matrix data structure */
    csc_matrix->column_pointers = (size_t *)aligned_alloc(align_sz, (coo_matrix->ncolumns + 1) * sizeof(size_t));
    check_mem_alloc(csc_matrix->column_pointers, "SparseMatrixCSC.column_pointers");
    memset(csc_matrix->column_pointers, 0, (coo_matrix->ncolumns + 1) * sizeof(size_t));

    csc_matrix->row_indices = (size_t *)aligned_alloc(align_sz, coo_matrix->nnz * sizeof(size_t));
    check_mem_alloc(csc_matrix->row_indices, "SparseMatrixCSC.row_indices");

    csc_matrix->values = (elem_t *)aligned_alloc(align_sz, coo_matrix->nnz * sizeof(elem_t));
    check_mem_alloc(csc_matrix->values, "SparseMatrixCSC.values");

    csc_matrix->name = coo_matrix->name;
    csc_matrix->nrows = coo_matrix->nrows;
    csc_matrix->ncolumns = coo_matrix->ncolumns;
    csc_matrix->nnz = coo_matrix->nnz;

    // Store the number of Non-Zero elements in each column (histogram)
    for (size_t i = 0; i < coo_matrix->nnz; i++)
        csc_matrix->column_pointers[coo_matrix->columns[i]]++;

    /*  
        Pre: Array containing the number of nnz in each column.
        Post: csc_matrix->column_pointers constains the (nnz) index to the first element of each column.
    */
    cum_sum = 0;
    for (size_t i = 0; i < coo_matrix->ncolumns; i++)
    {
        col_nnz = csc_matrix->column_pointers[i];
        csc_matrix->column_pointers[i] = cum_sum;
        cum_sum += col_nnz;
    }
    csc_matrix->column_pointers[csc_matrix->ncolumns] = csc_matrix->nnz;

    size_t *tmp_col_index = (size_t *)calloc(coo_matrix->ncolumns, sizeof(size_t));
    check_mem_alloc(tmp_col_index, "tmp_col_index");

    for (size_t c = 0; c < csc_matrix->ncolumns; c++)
        tmp_col_index[c] = csc_matrix->column_pointers[c];

    for (size_t i = 0; i < coo_matrix->nnz; i++)
    {
        size_t nnz_idx = tmp_col_index[coo_matrix->columns[i]]++;

        csc_matrix->row_indices[nnz_idx] = coo_matrix->rows[i];
        csc_matrix->values[nnz_idx] = coo_matrix->values[i];
    }

    free(tmp_col_index);

#ifdef DEBUG
    elapsed = mytimer() - start;
    fprintf(stderr, "Elapsed time converting COO to CSR =\t %g milliseconds\n", elapsed);
#endif
}

void convert_coo_to_csr(const SparseMatrixCOO *coo_matrix, SparseMatrixCSR *csr_matrix, int free_coo)
{

#ifdef SPMV_DEBUG
    double start, elapsed;
    start = mytimer();
#endif

    /* Allocate CSR Matrix data structure in memory */
    csr_matrix->row_pointers = (size_t *)aligned_alloc(align_sz, (coo_matrix->nrows + 1) * sizeof(size_t));
    check_mem_alloc(csr_matrix->row_pointers, "SparseMatrixCSR.row_pointers");
    memset(csr_matrix->row_pointers, 0, (coo_matrix->nrows + 1) * sizeof(size_t));

    csr_matrix->column_indices = (size_t *)aligned_alloc(align_sz, coo_matrix->nnz * sizeof(size_t));
    check_mem_alloc(csr_matrix->column_indices, "SparseMatrixCSR.column_indices");

    csr_matrix->values = (elem_t *)aligned_alloc(align_sz, coo_matrix->nnz * sizeof(elem_t));
    check_mem_alloc(csr_matrix->values, "SparseMatrixCSR.values");

    // Store the number of Non-Zero elements in each Row
    for (size_t i = 0; i < coo_matrix->nnz; i++)
        csr_matrix->row_pointers[coo_matrix->rows[i]]++;

    // Update Row Pointers so they consider the previous pointer offset
    // (using accumulative sum).
    size_t cum_sum = 0;
    for (size_t i = 0; i < coo_matrix->nrows; i++)
    {
        size_t row_nnz = csr_matrix->row_pointers[i];
        csr_matrix->row_pointers[i] = cum_sum;
        cum_sum += row_nnz;
    }

    /*  Adds COO values to CSR

        Note: Next block of code reuses csr->row_pointers[] to keep track of the values added from
        the COO matrix.
        This way is able to create the CSR */
    for (size_t i = 0; i < coo_matrix->nnz; i++)
    {
        size_t row_index = coo_matrix->rows[i];
        size_t column_index = coo_matrix->columns[i];
        elem_t value = coo_matrix->values[i];

        size_t j = csr_matrix->row_pointers[row_index];
        csr_matrix->column_indices[j] = column_index;
        csr_matrix->values[j] = value;
        csr_matrix->row_pointers[row_index]++;
    }

    // Restore the correct row_pointers
    for (size_t i = coo_matrix->nrows - 1; i > 0; i--)
    {
        csr_matrix->row_pointers[i] = csr_matrix->row_pointers[i - 1];
    }
    csr_matrix->row_pointers[0] = 0;
    csr_matrix->row_pointers[coo_matrix->nrows] = coo_matrix->nnz;

    csr_matrix->nnz = coo_matrix->nnz;
    csr_matrix->nrows = coo_matrix->nrows;
    csr_matrix->ncolumns = coo_matrix->ncolumns;
    csr_matrix->name = coo_matrix->name;

    /*  For each row, sort the corresponding arrasy csr.column_indices and csr.values // REMOVE??

        TODO: We should check if this step makes sense or can be optimized
        1) If the .mtx format by definition is ordered 
        2) If we force the COO Matrix to be ordered first, we can avoid this
        3) Test speed of standard library sorting vs current sorting approach. */

    // for (size_t i = 0; i < csr_matrix->nrows; i++)
    // {
    //     // print_arr_uint(csr_matrix->row_pointers[i+1]- csr_matrix->row_pointers[i], "before", &csr_matrix->column_indices[csr_matrix->row_pointers[i]]);

    //     // This could be optimized
    //     // sort_paired_vectors(csr_matrix->row_pointers[i], csr_matrix->row_pointers[i + 1],
    //     //                     csr_matrix->column_indices, csr_matrix->values);

    //     // fprintf(stderr, "Sorting from: [%" PRIu64 "] to [%" PRIu64 "]\n", csr_matrix->row_pointers[i],  csr_matrix->row_pointers[i + 1]);

    //     // radix_sort_paired_vectors(csr_matrix->column_indices, csr_matrix->values, csr_matrix->row_pointers[i], csr_matrix->row_pointers[i + 1]);
    //     // print_arr_uint(csr_matrix->row_pointers[i+1]- csr_matrix->row_pointers[i], "after", &csr_matrix->column_indices[csr_matrix->row_pointers[i]]);
    //     // exit(0);
    // }

#ifdef SPMV_DEBUG
    elapsed = mytimer() - start;
    fprintf(stderr, "Elapsed time converting COO to CSR =\t %g seconds\n", elapsed);
#endif

    if (free_coo)
    {
        free(coo_matrix->values);
        free(coo_matrix->rows);
        free(coo_matrix->columns);
    }
}

void convert_csr_to_ellpack(const SparseMatrixCSR *csr_matrix, SparseMatrixELLPACK *ellpack_matrix,
                            uint32_t order_by_row_size, const size_t *__restrict row_order)
{

#ifdef DEBUG
    double start, elapsed;
    start = mytimer();
#endif
    ellpack_matrix->max_row_size = 0;
    for (size_t i = 0; i < csr_matrix->nrows; i++)
    {
        size_t row_size = csr_matrix->row_pointers[i + 1] - csr_matrix->row_pointers[i];

        if (row_size > ellpack_matrix->max_row_size)
            ellpack_matrix->max_row_size = row_size;
    }

    /* Allocate ELLPACK Matrix data structure in memory */
    ellpack_matrix->column_indices = (size_t *)aligned_alloc(align_sz, ellpack_matrix->max_row_size * csr_matrix->nrows * sizeof(size_t));
    check_mem_alloc(ellpack_matrix->column_indices, "SparseMatrixELLPACK.column_indices");
    memset(ellpack_matrix->column_indices, 0, ellpack_matrix->max_row_size * csr_matrix->nrows * sizeof(size_t));

    ellpack_matrix->values = (elem_t *)aligned_alloc(align_sz, ellpack_matrix->max_row_size * csr_matrix->nrows * sizeof(elem_t));
    check_mem_alloc(ellpack_matrix->values, "SparseMatrixELLPACK.values");
    memset(ellpack_matrix->values, 0, ellpack_matrix->max_row_size * csr_matrix->nrows * sizeof(size_t));

    if (order_by_row_size)
    {
        /*  Adds CSR values and column indices to ELLPACK */
        // for (size_t r = 0; r < csr_matrix->nrows; r++)
        // {
        //     size_t ell_elem_idx = r * ellpack_matrix->max_row_size;
        //     size_t nnz_start = csr_matrix->row_pointers[row_order[r]];
        //     size_t nnz_end = csr_matrix->row_pointers[row_order[r] + 1];

        //     for (size_t i = nnz_start; i < nnz_end; i++)
        //     {
        //         ellpack_matrix->values[ell_elem_idx] = csr_matrix->values[i];
        //         ellpack_matrix->column_indices[ell_elem_idx] = csr_matrix->column_indices[i];

        //         ell_elem_idx++;
        //     }
        // }
    }
    else
    {
        /*  Adds CSR values and column indices to ELLPACK */
        for (size_t r = 0; r < csr_matrix->nrows; r++)
        {
            size_t write_elem_idx = r;

            for (size_t i = csr_matrix->row_pointers[r]; i < csr_matrix->row_pointers[r + 1]; i++)
            {
                ellpack_matrix->values[write_elem_idx] = csr_matrix->values[i];
                ellpack_matrix->column_indices[write_elem_idx] = csr_matrix->column_indices[i];

                write_elem_idx += csr_matrix->nrows;
            }
        }
    }

    ellpack_matrix->nnz = csr_matrix->nnz;
    ellpack_matrix->nrows = csr_matrix->nrows;
    ellpack_matrix->ncolumns = csr_matrix->ncolumns;
    ellpack_matrix->name = csr_matrix->name;

    /*  FREE CSR STRUCTURES  */
    // free(csr_matrix->values);
    // free(csr_matrix->column_indices);
    // free(csr_matrix->row_pointers);
}

/*  Create a pentadiagonal matrix, representing very roughly a finite
    difference approximation to the Laplacian on a square n x n mesh */

void load_pentadiagonal(size_t n, SparseMatrixCSR *csr_matrix)
{
    /* Warning: if n > sqrt(2^31), you will get integer overflow */
    if (n > 2 << 30)
    {
        fprintf(stderr, "Error: Matrix num rows (%lu) exceeds the limit (%d).", n, 2 << 30);
        exit(1);
    }

    if (n < 3)
    {
        fprintf(stderr, "Error: Matrix num rows(=%lu) cannot be smaller than 3!\n", n);
        exit(1);
    }

    csr_matrix->nrows = n * n; // n * n is correct
    csr_matrix->ncolumns = csr_matrix->nrows;
    csr_matrix->nnz = csr_matrix->nrows * 5;

    // ALIGNMENT?
    csr_matrix->row_pointers = (size_t *)aligned_alloc(align_sz, (csr_matrix->nrows + 1) * sizeof(size_t));
    csr_matrix->column_indices = (size_t *)aligned_alloc(align_sz, csr_matrix->nnz * sizeof(size_t));
    csr_matrix->values = (elem_t *)aligned_alloc(align_sz, csr_matrix->nnz * sizeof(elem_t));

    size_t row_index = 0;
    size_t nnz_index = 0;
    size_t i = 0, j = 0;

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            csr_matrix->row_pointers[row_index] = nnz_index;

            // Diagonal row_index-n (starts at row_index = n)
            if (i > 0)
            {
                csr_matrix->column_indices[nnz_index] = row_index - n;
                csr_matrix->values[nnz_index] = -1.0;
                nnz_index++;
            }
            // Diagonal row_index-1
            if (j > 0)
            {
                csr_matrix->column_indices[nnz_index] = row_index - 1;
                csr_matrix->values[nnz_index] = -1.0;
                nnz_index++;
            }

            // Diagonal row_index
            csr_matrix->column_indices[nnz_index] = row_index;
            csr_matrix->values[nnz_index] = 4.0;
            nnz_index++;

            // Diagonal row_index+1
            if (j < n - 1)
            {
                csr_matrix->column_indices[nnz_index] = row_index + 1;
                csr_matrix->values[nnz_index] = -1.0;
                nnz_index++;
            }

            // Diagonal row_index+n (ends at row_index = nrows-n)
            if (i < n - 1)
            {
                csr_matrix->column_indices[nnz_index] = row_index + n;
                csr_matrix->values[nnz_index] = -1.0;
                nnz_index++;
            }
            row_index++;
        }
    }
    csr_matrix->row_pointers[row_index] = nnz_index;
    csr_matrix->name = "pentadiagonal";
}

void load_octadiagonal(size_t n, SparseMatrixCSR *csr_matrix)
{
    /* Warning: if n > sqrt(2^31), you will get integer overflow */
    if (n > 2 << 30)
    {
        fprintf(stderr, "Error: Matrix num rows (%lu) exceeds the limit (%d).", n, 2 << 30);
        exit(1);
    }

    if (n < 3)
    {
        fprintf(stderr, "Error: Matrix num rows(=%lu) cannot be smaller than 3!\n", n);
        exit(1);
    }

    csr_matrix->nrows = n * n; // n * n is correct
    csr_matrix->ncolumns = csr_matrix->nrows;

    csr_matrix->row_pointers = (size_t *)aligned_alloc(align_sz, (csr_matrix->nrows + 1) * sizeof(size_t));
    csr_matrix->column_indices = (size_t *)aligned_alloc(align_sz, ((csr_matrix->nrows * 8) + 256) * sizeof(size_t));
    csr_matrix->values = (elem_t *)aligned_alloc(align_sz, csr_matrix->nrows * 8 * sizeof(elem_t));

    size_t row_index = 0;
    size_t nnz_index = 0;
    size_t i = 0, j = 0;

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            csr_matrix->row_pointers[row_index] = nnz_index;

            // Diagonal row_index-n (starts at row_index = n)
            if (i > 1)
            {
                csr_matrix->column_indices[nnz_index] = row_index - n - n;
                csr_matrix->values[nnz_index] = -4.0000;
                nnz_index++;
            }
            if (i > 0)
            {
                csr_matrix->column_indices[nnz_index] = row_index - n;
                csr_matrix->values[nnz_index] = -3.00000;
                nnz_index++;
            }

            // Diagonal row_index-2
            if (j > 1 || i > 0)
            {
                csr_matrix->column_indices[nnz_index] = row_index - 2;
                csr_matrix->values[nnz_index] = -2.00000;
                nnz_index++;
            }
            // Diagonal row_index-1
            if (j > 0 || i > 0)
            {
                csr_matrix->column_indices[nnz_index] = row_index - 1;
                csr_matrix->values[nnz_index] = -1.00000;
                nnz_index++;
            }

            // Diagonal row_index
            csr_matrix->column_indices[nnz_index] = row_index;
            csr_matrix->values[nnz_index] = 4.0;
            nnz_index++;

            // Diagonal row_index+1
            if ((j < n - 1) && (i < n - 1))
            {
                csr_matrix->column_indices[nnz_index] = row_index + 1;
                csr_matrix->values[nnz_index] = -1.5;
                nnz_index++;
            }

            // Diagonal row_index+n (ends at row_index = nrows-n)
            if (i < n - 1)
            {
                csr_matrix->column_indices[nnz_index] = row_index + n;
                csr_matrix->values[nnz_index] = -2.5;
                nnz_index++;
            }

            // Diagonal row_index+n+n (ends at row_index = nrows-n)
            if (i < n - 2)
            {
                csr_matrix->column_indices[nnz_index] = row_index + n + n;
                csr_matrix->values[nnz_index] = -3.5;
                nnz_index++;
            }

            row_index++;
        }
    }
    csr_matrix->row_pointers[row_index] = nnz_index;
    csr_matrix->name = "octadiagonal";
    csr_matrix->nnz = nnz_index; // This is not ok ???

    for (int k = 0; k < 256; k++)
    {
        csr_matrix->column_indices[nnz_index + k] = 0;
    }
}

void generate_sparse_matrix(size_t nrows, size_t ncols, double sparsity, int col_dist, SparseMatrixCSR *__restrict mymatrix)
{
    /*
        col_dist: Determines how columns are distributed
            0: Random
            1: Strided
            2: Strided + Shifted by 1 NOT IMPLEMENTED
            3: Strided + Shifted by Cache line size NOT IMPLEMENTED
    */
    size_t nnz = 0, stride = 0;
    size_t nnz_per_row = 0;
    elem_t nnz_per_rowf = 0;

    switch (col_dist)
    {
    case 0:
        /* TODO */

        break;

    case 1:
        nnz_per_rowf = ncols * sparsity;
        stride = ncols / nnz_per_rowf;
        nnz_per_row = ncols / stride;
        nnz = nnz_per_row * nrows;

        if (nnz_per_row < 1)
            exit(1);
        break;

    default:
        break;
    }

    mymatrix->ncolumns = ncols;
    mymatrix->nrows = nrows;
    mymatrix->nnz = nnz;
    mymatrix->name = "strided";

    fprintf(stderr, "Generating matrix with sparsity: %f\n", sparsity);
    fprintf(stderr, "NNZ per row: %lu, w/ stride: %lu, along %lu columns\n", nnz_per_row, stride, ncols);
    fprintf(stderr, "For a total NNZ of: %lu\n", nnz);

    mymatrix->row_pointers = (size_t *)aligned_alloc(align_sz, (mymatrix->nrows + 1) * sizeof(size_t));
    check_mem_alloc(mymatrix->row_pointers, "SparseMatrixCSR.row_pointers");
    // memset(csr_matrix->row_pointers, 0, (nrows + 1) * sizeof(size_t));

    mymatrix->column_indices = (size_t *)aligned_alloc(align_sz, nnz * sizeof(size_t));
    check_mem_alloc(mymatrix->column_indices, "SparseMatrixCSR.column_indices");

    mymatrix->values = (elem_t *)aligned_alloc(align_sz, nnz * sizeof(elem_t));
    check_mem_alloc(mymatrix->values, "SparseMatrixCSR.values");

    size_t *col_idx_sequence = (size_t *)malloc(nnz_per_row * sizeof(size_t));
    for (size_t c = 0; c < nnz_per_row; c++)
    {
        col_idx_sequence[c] = c * stride;
    }
    //  print_arr_uint(nnz_per_row, "index sequence", col_idx_sequence);

    for (size_t i = 0; i < nnz; i++)
    {
        // fill with random values between 0 and 10
        mymatrix->values[i] = ((elem_t)rand() / (elem_t)(RAND_MAX)*5);
        mymatrix->column_indices[i] = col_idx_sequence[i % nnz_per_row];
    }

    for (size_t r = 0; r < nrows + 1; r++)
    {
        mymatrix->row_pointers[r] = r * nnz_per_row;
    }

    //print_arr_float(nnz, "values", mymatrix->values);
    // print_arr_uint(nnz, "col indices", mymatrix->column_indices);

    // print_arr_uint(nrows + 1, "rows indices", mymatrix->row_pointers);

    // print_full_csr_matrix(csr_matrix);
}

void print_full_csr_matrix(const SparseMatrixCSR *csr_matrix)
{
    for (size_t row_index = 0; row_index < csr_matrix->nrows; row_index++)
    {
        size_t start = csr_matrix->row_pointers[row_index];
        size_t end = csr_matrix->row_pointers[row_index + 1] - 1;

        size_t elem_index = start;
        for (size_t col_index = 0; col_index < csr_matrix->ncolumns; col_index++)
        {

            if (start - end == 0)
            {
                // emtpy row
                printf("0");
                if (col_index < csr_matrix->ncolumns - 1)
                    printf(",");
            }
            else if (col_index < csr_matrix->column_indices[start])
            {
                //leading 0's
                printf("0");
                if (col_index < csr_matrix->ncolumns - 1)
                    printf(",");
            }
            else if (col_index > csr_matrix->column_indices[end])
            {
                //trailing 0's
                printf("0");
                if (col_index < csr_matrix->ncolumns - 1)
                    printf(",");
            }
            else if (csr_matrix->column_indices[elem_index] == col_index)
            {
                //nnz
                printf("%.2e", csr_matrix->values[elem_index]);
                if (col_index < csr_matrix->ncolumns - 1)
                    printf(",");
                elem_index++;
            }
            else
            {
                // 0's between nnz
                printf("0");
                if (col_index < csr_matrix->ncolumns - 1)
                    printf(",");
                //elem_index++;
            }
        }
        printf("\n");
    }
    fflush(stdout);
}

void convert_csr_to_csrsimd(const SparseMatrixCSR *csr_matrix, SparseMatrixCSRSIMD *csrsimd_matrix, int segment_len)
{

    // row_ptr = Vector with Pointers to the start of each row in the coldind vector.
    // col_ind = Vector with the column indexes to the values in vector val.
    // val = vector with values
    // <total_fragments> we keep track

#ifdef SPMV_VERIFY
    fprintf(stderr, "Pre-processing...\n");
#endif

    size_t i, j, k;
    size_t total_independent_fragments = 0;
    size_t values_array_size = 0, column_indices_array_size = 0;

    for (i = 0; i < csr_matrix->nrows; i++)
    {
        int current_fragment_size = 1;
        int fragment_elements = 0;
        // size_t fragment_start_index = csr_matrix->column_indices[csr_matrix->row_pointers[i]];

        // Note: if the row is empty (has no nnz) the following loop wont execute any iteration.
        for (j = csr_matrix->row_pointers[i]; j < csr_matrix->row_pointers[i + 1]; j++)
        {
            int is_last = (j == csr_matrix->row_pointers[i + 1] - 1);

            values_array_size++; //add value
            fragment_elements++; //new fragment in this row

            // Check if the size of the fragment increased: fragment size = ceil(frag_elements/segment_length)
            current_fragment_size = (fragment_elements + segment_len - 1) / segment_len;

            if (!is_last)
            {
                // Number of 0's to next NNZ
                int distance = csr_matrix->column_indices[j + 1] - csr_matrix->column_indices[j] - 1;

                // Cost of adding a new fragment
                int cost_new_fragment = ((current_fragment_size + segment_len - 1) / segment_len) + 1;

                // Cost of extending the fragment.
                int tmp = current_fragment_size + distance + 1;
                int cost_extend = ((tmp + segment_len - 1) / segment_len);

                /* 
                    If A > B means it is not worth to add a fragment we extend to the next element
                    If A = B we also extend  (so we dont create a new entry in the column index )
                */

                if (cost_new_fragment >= cost_extend)
                {
                    // Extending current fragment
                    // Fill the distance with zeros.
                    for (k = 0; k < distance; k++)
                    {
                        values_array_size++; //add 0 (+distance)
                    }
                    fragment_elements += distance;
                }
                else
                {
                    /*
                        Finalize current independent fragment & Fill with 0's
                    */
                    int tmp = fragment_elements % segment_len;
                    int filling = (segment_len - tmp) % segment_len;

                    for (int z = 0; z < filling; z++)
                    {
                        values_array_size++; // add 0's (+filling)
                    }

                    // Save current fragment and start new independent one.
                    column_indices_array_size += 2; // space to save for start_index and fragment_size

                    // fragment_start_index = csr_matrix->column_indices[j + 1];
                    current_fragment_size = 0; // unnecesary?
                    fragment_elements = 0;
                    total_independent_fragments++;
                }
            }
            else
            {
                // Last NNZ in row
                // Fill the current fragment with zeros until it fits the segment length.
                int tmp = fragment_elements % segment_len;
                int filling = (segment_len - tmp) % segment_len;

                for (int z = 0; z < filling; z++)
                {
                    values_array_size++;
                }
                column_indices_array_size += 2; // space to save for start_index and fragment_size
                total_independent_fragments++;
            }
        }
    }

    /*
        Malloc CSR SIMD structures
     */
    csrsimd_matrix->nrows = csr_matrix->nrows;
    csrsimd_matrix->ncolumns = csr_matrix->ncolumns;
    csrsimd_matrix->nnz = csr_matrix->nnz;
    csrsimd_matrix->name = csr_matrix->name;

    csrsimd_matrix->row_pointers = (size_t *)aligned_alloc(align_sz, (csr_matrix->nrows + 1) * sizeof(size_t));
    csrsimd_matrix->column_indices = (size_t *)aligned_alloc(align_sz, column_indices_array_size * sizeof(size_t));
    csrsimd_matrix->values = (elem_t *)aligned_alloc(align_sz, values_array_size * sizeof(elem_t));

    total_independent_fragments = 0;
    size_t total_csrsimd_nnz = 0;

    for (int i = 0; i < csrsimd_matrix->nrows; i++)
    {
        int current_fragment_size = 1;
        int fragment_elements = 0;
        size_t fragment_start_index = csr_matrix->column_indices[csr_matrix->row_pointers[i]];

        csrsimd_matrix->row_pointers[i] = total_independent_fragments;

        // Note: if the row is empty (has no nnz) the following loop wont execute any iteration.
        for (j = csr_matrix->row_pointers[i]; j < csr_matrix->row_pointers[i + 1]; j++)
        {
            int is_last = (j == csr_matrix->row_pointers[i + 1] - 1);

            csrsimd_matrix->values[total_csrsimd_nnz] = csr_matrix->values[j];

            total_csrsimd_nnz++;
            fragment_elements++;

            // Check if the size of the fragment increased: fragment size = ceil(frag_elements/segment_length)
            current_fragment_size = (fragment_elements + segment_len - 1) / segment_len;

            if (!is_last)
            {
                // Number of 0's to next NNZ
                int distance = csr_matrix->column_indices[j + 1] - csr_matrix->column_indices[j] - 1;

                // Cost of adding a new fragment
                int cost_new_fragment = ((current_fragment_size + segment_len - 1) / segment_len) + 1;

                // Cost of extending the fragment.
                int tmp = current_fragment_size + distance + 1;
                int cost_extend = ((tmp + segment_len - 1) / segment_len);

                /* If A > B means it is not worth to add a fragment we extend to the next element */
                /* If A = B we also extend  (so we dont create a new entry in the column index ) */

                if (cost_new_fragment >= cost_extend)
                {
#ifdef SPMV_DEBUG
                    fprintf(stderr, "Extending...");
#endif
                    // Extending current fragment
                    // Fill the distance with zeros.
                    for (k = 0; k < distance; k++)
                    {
                        csrsimd_matrix->values[total_csrsimd_nnz] = 0;
                        total_csrsimd_nnz++;

#ifdef SPMV_DEBUG
                        fprintf(stderr, " 0 ");
#endif
                    }
#ifdef SPMV_DEBUG
                    fprintf(stderr, "\n");
#endif

                    fragment_elements += distance;
                }
                else
                {
                    // Finalize current fragment.

#ifdef SPMV_DEBUG
                    fprintf(stderr, "New independent fragment.. ");
#endif

                    // How many trailing zeroes should I add?
                    int tmp = fragment_elements % segment_len;
                    int filling = (segment_len - tmp) % segment_len;

                    for (int z = 0; z < filling; z++)
                    {
                        csrsimd_matrix->values[total_csrsimd_nnz] = 0;
                        total_csrsimd_nnz++;
#ifdef SPMV_DEBUG
                        fprintf(stderr, " 0 ");
#endif
                    }
#ifdef SPMV_DEBUG
                    fprintf(stderr, " \n ");
#endif

                    // Save current fragment and start new independent one.
                    csrsimd_matrix->column_indices[total_independent_fragments] = fragment_start_index;
                    csrsimd_matrix->column_indices[total_independent_fragments + 1] = current_fragment_size;
                    total_independent_fragments += 2;

                    fragment_start_index = csr_matrix->column_indices[j + 1]; //
                    current_fragment_size = 0;
                    fragment_elements = 0;
                }
            }
            else
            {
#ifdef SPMV_DEBUG
                fprintf(stderr, "Last element in row.. with %d elements and slen=%d ", fragment_elements, segment_len);
#endif
                // Last NNZ in row

                // Fill the current fragment with zeros until it fits the segment length.
                int tmp = fragment_elements % segment_len;
                int filling = (segment_len - tmp) % segment_len;

                for (int z = 0; z < filling; z++)
                {
                    csrsimd_matrix->values[total_csrsimd_nnz] = 0;
                    total_csrsimd_nnz++;
#ifdef SPMV_DEBUG
                    fprintf(stderr, " 0 ");
#endif
                }

#ifdef SPMV_DEBUG
                fprintf(stderr, " \n ");
#endif

                // Save current fragment.
                csrsimd_matrix->column_indices[total_independent_fragments] = fragment_start_index;
                csrsimd_matrix->column_indices[total_independent_fragments + 1] = current_fragment_size;
                total_independent_fragments += 2;
            }
        }
    }

    csrsimd_matrix->row_pointers[i] = total_independent_fragments;
#ifdef SPMV_VERIFY

#endif
}
