# by default, "arch" is UNKNOWN
arch ?= UNKNOWN
SRC_PATH = SPMV_SRC_PATH

setup_file = setup/$(arch).mk
include $(setup_file)

COMMON_SRC = 	$(SRCdir)/main.c $(SRCdir)/spmv_ref.c \
				$(SRCdir)/sparse_matrix.c $(SRCdir)/report_results.c \
				$(SRCdir)/mytimer.c $(SRCdir)/mmio.c $(SRCdir)/utils.c
				#$(SRCdir)/sorting_utils.c \
				#$(SRCdir)/hw_counters_ve.c

COMMON_OBJ = $(COMMON_SRC:$(SRCdir)/%.c=bin/%.o) 

KERNELS_SRC = $(SRCdir)/spmv_par_kernels.c
KERNELS_OBJ = $(KERNELS_SRC:$(SRCdir)/%.c=bin/%.o)


# SPMV_ASMS = $(SPMV_COMMON:.c=.s) # unused

all: $(TARGETS)
PHONY: objclean clean

# target: pattern: prereq-pattern
$(COMMON_OBJ): bin/%.o: $(SRCdir)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(KERNELS_OBJ): bin/%.o: $(SRCdir)/%.c
	$(LLVM) $(LLVM_FLAGS) -c $< -o $@

mkl: objclean $(COMMON_OBJ) $(SRCdir)/spmv_mkl.c
	$(CC) $(CFLAGS) -c $(SRCdir)/spmv_mkl.c -o bin/spmv_mkl.o
	$(CC) $(CFLAGS) -o bin/spmv_mkl$(BIN_SUFFIX) $(COMMON_OBJ) bin/spmv_mkl.o $(SPMV_LIBS)

sellcs_avx512: objclean $(COMMON_OBJ) libsellcs-spmv_formatonly.a $(SRCdir)/spmv_sellcs_avx.c
	$(CC) $(CFLAGS) -c $(SRCdir)/spmv_sellcs_avx.c -o bin/spmv_sellcs_avx.o
	$(CC) $(CFLAGS) -o bin/spmv_sellcs_avx$(BIN_SUFFIX) $(COMMON_OBJ) bin/spmv_sellcs_avx.o $(SPMV_LIBS)	

sellcs_autovector: objclean $(COMMON_OBJ) libsellcs-spmv_formatonly.a $(SRCdir)/spmv_sellcs_autovector.c
	$(CC) $(CFLAGS) -c $(SRCdir)/spmv_sellcs_autovector.c -o bin/spmv_sellcs_autovector.o
	$(CC) $(CFLAGS) -o bin/spmv_sellcs_autovector$(BIN_SUFFIX) $(COMMON_OBJ) bin/spmv_sellcs_autovector.o $(SPMV_LIBS)	

# Targets to compile custom versions (w/ _ver suffix) that require extra compilation step

sellcslib: objclean $(COMMON_OBJ) $(EXTRA_LIBS) $(SRCdir)/spmv_sellcslib.c
	$(CC) $(CFLAGS) -c $(SRCdir)/spmv_sellcslib.c -o bin/spmv_sellcslib.o 
	$(CC) $(CFLAGS) -o bin/spmv_sellcslib$(BIN_SUFFIX) $(COMMON_OBJ) bin/spmv_sellcslib.o  $(SPMV_LIBS)


libsellcs-spmv%.a:
	make -C $(SRC_PATH)/libsellcs-spmv CC=$(CC) clean
	make -C $(SRC_PATH)/libsellcs-spmv CC=$(CC) arch=$(arch) all

libsellcs-spmv_formatonly.a:
	make -C CC=$(CC) $(SRC_PATH)/libsellcs-spmv arch=intelavx INDEX64=1 libsellcs-spmv_formatonly.a


objclean:
	rm -f bin/*.o bin/*.L

clean: 
	-rm -f bin/spmv_* asm/spmv_* bin/*.o
