#!/bin/bash



echo -e "dataset\tref_time\tref_gflops\tvec_time\tvec_gflops"
for cage in 6 7 8 9 10
do
	oldIFS=$IFS
	IFS=;
	var=`./bin/spmv_sellcslib -v 256 -m /home/pmtest/epi/ftp/sdvs/integration-functional-tests/resources/tests/spmv/spmv_inputs/cage${cage}/cage${cage}.mtx`

	ref_t=`echo $var | grep "Reference version execution time" | awk '{print $NF}' | cut -d ',' -f1`
	ref_g=`echo $var | grep "Reference version GFLOPS/s" | awk '{print $NF}' | cut -d ',' -f1`
	vec_t=`echo $var | grep "sellcslib version execution time" | awk '{print $NF}' | cut -d ',' -f1`
	vec_g=`echo $var | grep "sellcslib version GFLOPS/s" | awk '{print $NF}' | cut -d ',' -f1`
	echo -e "cage${cage}\t$ref_t\t$ref_g\t$vec_t\t$vec_g"
	IFS=$oldIFS
done
