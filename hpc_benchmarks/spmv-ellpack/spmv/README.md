# Optimized SpMV implementations for Long Vector Architectures 

This code contains a Benchmark infrastructure to test the performance of SpMV
(GFLOPs) and several SpMV implementations to test.

This is the documentation that covers the usase of the 'riscv' branch of the repository.

THIS IS NOT A STABLE CODE, ITS MADE FOR RESEARCH PURPOSES.

## Available SpMV implementations to test

In this branch not every optimization of sell-c-sigma is implemented. There is a small subset of the available for the VE. 

Following we describe how to test each implementation:

### Plain-C implementation for Autovectorization

build with:
```
mkdir build
cd build
../configure autovector
make
```

run with:
```
./bin/spmv_sellcs_autovector -m <path_to_your_matrix>
```

### AVX512 implementation f intel systems

build with:
```
mkdir build
cd build
../configure avx512
make
```

run with:
```
./bin/spmv_sellcs_avx -m <path_to_your_matrix>
```

### MKL implementation for intel systems

build with:
```
mkdir build
cd build
../configure mkl
make
```

run with:
```
./bin/spmv_sellcs_mkl -m <path_to_your_matrix>
```

## Run the benchmarks

binaries will be placed in the build/bin folder.

Run with:

```
./< binary > -m < inputmatrix > [ options ]
```

| Parameter   | Default    | Description |
| :-----------| :-----------| :-----------|
|  -m         | None            | path to the input .mtx file        |
|  -i  (optional) | 10          | Number of SPMV iterations          |
|  -c  (optional) | check chunk_size variable | If OpenMP is enabled, specifies the number of tasks       |
|  -w  (optional) | check sigma_window variable | In sell-c-sigma implementations, specifies the reorder window (sigma)   |
|  -b  (optional) | 1 | In sell-c-sigma with blocking enabled, specifies the number of blocks   |

Except for the number of iterations we recommend to use power of two values.

Example:

```
./build/bin/spmv_sellcs_omp -m bcsstk27.mtx -i 5000 -w 8192 -c 32
```

Input matrices in MTX format can be found at: https://sparse.tamu.edu/








Next table describes how to enable, and where to find, each implementation of SpMV.

| Implementation | Platform | SPMV_FLAGS | Files |
| :------ | :-----------|:------ | :-----------|
| SELL-C-SIGMA   | RISCV | None | spmv_SELLCSv2.c, spmv_par_kernels.c |
| SELL-C-SIGMA + DFC   | Vector Engine | ENABLE_FG | spmv_SELLCSv2.c, spmv_par_kernels.c |
| SELL-C-SIGMA + unroll + DFC | Vector Engine | ENABLE_FG | spmv_SELLCS_U8v2.c, spmv_par_kernels.c |
| SELL-C-SIGMA + unroll + NC | Vector Engine | ENABLE_NC | spmv_SELLCS_U8v2.c, spmv_par_kernels.c |
| SELL-C-SIGMA + unroll + NC + DFC | Vector Engine | ENABLE_DFC ENABLE_NC | spmv_SELLCS_U8v2.c, spmv_par_kernels.c |
| SELL-C-SIGMA + blocking + NC + DFC | Vector Engine | None | spmv_SELLCS_blocked_advanced.c, spmv_par_kernels.c |
| SELL-C-SIGMA | RISC-V | None | spmv_sellcs_epi.c |
| SELL-C-SIGMA | Intel AVX512 | None | spmv_sellcs_avx.c |
| MKL | AVX512 | None | spmv_mkl.c |
| cuSPARSE | CUDA GPU | None | spmv_cusparse.c |
| NEC NLC | Vector Engine | None | spmv_nlc.c |
| CSRSIMD | x86 AVX512 | None | spmv_csrsimd.c |
| CSRSIMD | Vector Engine | None | spmv_csrsimd_ve.c |
| VBSF | x86 AVX512 | None | spmv_vbsf.c |


Note that some optimizations are new, while others previously described in the
SELL-C-Sigma¹ and ESB² formats and algorithms.


# Cites

¹ A unified sparse matrix data format for modern processors with wide {SIMD} units (http://arxiv.org/abs/1307.6209)

² Efficient Sparse Matrix-Vector Multiplication on x86-Based Many-Core Processors 

³ (https://upcommons.upc.edu/handle/2117/192586)

⁴ https://onlinelibrary.wiley.com/doi/abs/10.1002/cpe.4800

⁵ https://link.springer.com/article/10.1007/s11227-019-02835-4


# Modeling SELL-C-SIGMA Memory Bandwidth requirements on the Vector Engine

The STREAM Benchmark results on a Vector Engine 10B, with NUMA MODE disabled, and 8 threads are the following:

```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          968953.6     0.025361     0.022163     0.029368
Scale:         968682.7     0.025319     0.022169     0.029343
Add:           954213.1     0.036248     0.033758     0.041052
Triad:         959124.2     0.036084     0.033585     0.041031
```

(Triad result) / 1024 = 936.65 GB/s

In SELL-C-SIGMA for each Non-Zero, we load 1\*(A.value + A.column_index + X.value). 
In our experiments, all three are 8-byte variables.

## Model 
We can model the Min/Max bandwidth requirements like this:

Our benchmarks reports (G)FLOPS/s. 

Each Non-Zero element requires 2 FLOPS (1xFMA) to compute.

Both Load accesses to the matrix are in a stream fashion and never reused; accesses to X are random and highly reused.

Therefore: 
```
NNZ/s = FLOPS/s / 2
```
Min Bandwidth: All accesses to the X vector HIT in cache. 
```
Min.BW = 16 bytes*(NNZ/s)
```

Max Bandwidth: All accesses to the X vector MISS in LLC.
```
Max.BW = 24 bytes*(NNZ/s)
```

# Experimental results: Can we saturate all the Vector Engine Memory Bandwidth?

Now, our best result is 117 GFLOPS/s (see technical report, Figure 3).

NNZ/s = 117 GFLOPS/s / 2 = 58.5 GNNZ/s

Min. Bandwidth = 58.5 * 16 = 936 GB/s

Max. Bandwidth = 58.5 * 24 = 1404 GB/s


If the input matrix is adequate, the SELL-C-SIGMA algorithm is able to fully saturate the memory bandwidth of the Vector Engine.