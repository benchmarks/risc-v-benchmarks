# - DGEMM Directory Structure / DGEMM library 
# ----------------------------------------------------------------------

TOPdir       = ..
SRCdir       = $(TOPdir)/src
INCdir       = $(TOPdir)/src
BINdir       = $(TOPdir)/bin


# - DGEMM includes / libraries / specifics 
# ----------------------------------------------------------------------

DGEMM_INCLUDES = -I$(INCdir) -I$(MKLROOT)/include
DGEMM_LIBS     = -L$(MKLROOT)/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm


# - Compile time options 
# ----------------------------------------------------------------------
# -DDGEMM_VERIFY Define to enable DGEMM results verification
# -DUSE_OMP      Enables OpenMP parallel regions in the code
# -DUSE_OMPSS    Enables OmpSS parallel regions in the code (alternative to DUSE_OMP)
#
# By default DGEMM will:
#    *) Not verify results

DGEMM_OPTS     =  -DUSE_OMPSS # -DDGEMM_VERIFY


# ----------------------------------------------------------------------

DGEMM_DEFS     = $(DGEMM_OPTS) $(DGEMM_INCLUDES)

# ----------------------------------------------------------------------


TARGETS        = # THIS IS A TEMPLATE - NO BINARIES ARE COMPILED


# - Compilers / linkers - Optimization flags
# ----------------------------------------------------------------------

CC          = ismpcc
CFLAGS      += $(DGEMM_DEFS) -O3 --ompss -Wall -Wextra

LINKER       = $(CC)
LINKFLAGS    = $(CFLAGS)

ARCHIVER     = ar
ARFLAGS      = r
RANLIB       = echo