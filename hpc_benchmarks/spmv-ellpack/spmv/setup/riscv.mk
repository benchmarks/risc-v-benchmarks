# - SPMV Directory Structure / SPMV library 
# ----------------------------------------------------------------------

TOPdir       = $(SRC_PATH)
SRCdir       = $(TOPdir)/src
INCdir       = $(TOPdir)/src
BINdir       = $(TOPdir)/bin
LIBdir       = $(TOPdir)/libsellcs-spmv


# - SPMV includes / libraries / specifics 
# ----------------------------------------------------------------------

EXTRA_LIBS = 

ifdef INDEX64
    ifdef OMP
        EXTRA_LIBS :=libsellcs-spmv_i64_omp.a
    else
        ifneq (,$(findstring 07,$(EPI_EXT)))
            EXTRA_LIBS :=libsellcs-spmv_i64_sequential_v07.a
         else
            EXTRA_LIBS :=libsellcs-spmv_i64_sequential_v09.a
         endif
    endif
else
  ifdef OMP
        EXTRA_LIBS :=libsellcs-spmv_i32_omp.a
    else
        ifneq (,$(findstring 07,$(EPI_EXT)))
            EXTRA_LIBS :=libsellcs-spmv_i32_sequential_v07.a
        else
            EXTRA_LIBS :=libsellcs-spmv_i32_sequential_v09.a
        endif
    endif
endif

SPMV_INCLUDES = -I$(INCdir) -I$(LIBdir) # $(NLC_INC)
SPMV_LIBS     = $(LIBdir)/$(EXTRA_LIBS) 

# - Compile time options 
# ----------------------------------------------------------------------
# -DSPMV_VERIFY Define to enable SPMV results verification
# -DUSE_OMP      Enables OpenMP parallel regions in the code
# -DUSE_OMPSS2   Enables OmpSS-2 parallel regions in the code (alternative to DUSE_OMP) 
#
# By default SPMV will:
#    *) Not verify results

SPMV_OPTS     =   -DALIGN_TO=2048  -DSPMV_VERIFY -DUSE_RV_CYCLE_COUNTER

# ----------------------------------------------------------------------
BIN_SUFFIX=

ifneq (,$(findstring ENABLE_NC,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_NC
endif

ifneq (,$(findstring ENABLE_DFC,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_DFC
endif

ifneq (,$(findstring USE_OMP,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_omp
endif

ifneq (,$(findstring ENABLE_HWC_VE,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_hwc
endif

# ----------------------------------------------------------------------

SPMV_DEFS     = $(SPMV_OPTS) $(SPMV_INCLUDES)

ifdef INDEX64
    SPMV_DEFS += -DINDEX64
endif

ifdef OMP
     SPMV_DEFS += -DUSE_OMP
endif


# ----------------------------------------------------------------------

TARGETS        =  sellcslib

# - Compilers / linkers - Optimization flags
# ----------------------------------------------------------------------

CC          ?= clang
CFLAGS      = $(SPMV_DEFS) 

LLVM        ?= clang
LLVM_FLAGS  = -mepi -fno-vectorize 

ifdef OMP
     CFLAGS += -fopenmp
     BIN_SUFFIX :=$(BIN_SUFFIX)_omp
endif

ifdef DBG
   CFLAGS     += -O0 -g -ggdb -DSELLCS_DEBUG
   LLVM_FLAGS += -O0 -g -ggdb -DSELLCS_DEBUG
else
   CFLAGS    += -O3 -DNDEBUG 
   LLVM_FLAGS += -O1 -DNDEBUG 
endif

CFLAGS += -mcpu=avispado
LLVM_FLAGS += -mcpu=avispado


MASM        = 

LINKER       = $(CC)
LINKFLAGS    = $(CFLAGS)

ARCHIVER     = ar
ARFLAGS      = r
RANLIB       = echo



