# - SPMV Directory Structure / SPMV library 
# ----------------------------------------------------------------------

TOPdir       = ..
SRCdir       = $(TOPdir)/src
INCdir       = $(TOPdir)/src
BINdir       = $(TOPdir)/bin
LIBdir       = $(TOPdir)/libsellcs-spmv



# - SPMV includes / libraries / specifics 
# ----------------------------------------------------------------------

EXTRA_LIBS = libsellcs-spmv_formatonly.a
PAPI_LIBS = -L/apps/PAPI/5.7.0/lib -lpapi

SPMV_LIBS        =  $(PAPI_LIBS) $(LIBdir)/$(EXTRA_LIBS)
SPMV_INCLUDES    = -I$(INCdir) -I$(LIBdir)

# - Compile time options 
# ----------------------------------------------------------------------
# -DSPMV_VERIFY  Define to enable SPMV results verification
# -DUSE_OMP      Enables OpenMP parallel regions in the code
# -DENABLE_HWC   Enables measurements usings HWC
#
# By default SPMV will:
#    *) Not verify results

SPMV_OPTS     = -DALIGN_TO=1024 -DSPMV_VERIFY # -DENABLE_HWC #  #-DSPMV_DEBUG -DUSE_OMP
SPMV_DEFS     = $(SPMV_OPTS) $(SPMV_INCLUDES) -DINDEX64 -DPAPI
TARGETS       =  sellcs_autovector


# ----------------------------------------------------------------------

ifneq (,$(findstring USE_OMP,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_omp
endif

ifneq (,$(findstring ENABLE_HWC,$(SPMV_OPTS)))
    BIN_SUFFIX :=$(BIN_SUFFIX)_hwc
endif

# - Compilers / linkers - Optimization flagsq
# ----------------------------------------------------------------------

CC          = icc
CXX         = icpc

# Performance
# Warning! using -restrict flag on codes missing the corresponding restrict flags, will result in a performance pentalty. Double check
CFLAGS      += $(SPMV_DEFS) -O3 -Wall -Wextra -std=c11 -restrict -march=skylake-avx512 -qopenmp 
CXXFLAGS    += $(SPMV_DEFS) -O3 -Wall  -Wextra -std=c++11 -restrict -march=skylake-avx512 -qopenmp


# Debug
#CFLAGS      += $(SPMV_DEFS) -O0 -g -traceback -Wall -Wextra -std=c11
#CXXFLAGS    += $(SPMV_DEFS) -O0 -g -traceback -Wall -Wextra -std=c++11

# Profiling
#CFLAGS      += $(SPMV_DEFS) -O3 -g -Wall -Wextra -std=c11 
#CFLAGS      += $(SPMV_DEFS) -O3 -Wall -Wextra -k --Wn,-qopt-report=4,#-qopt-report-phase,ipo 
#CFLAGS      += $(SPMV_DEFS) -O0 -g -Wall -Wextra -k --Wn,-traceback -std=c11

MASM        = -masm=intel

LINKER       = $(CC)
LINKFLAGS    = $(CFLAGS)

ARCHIVER     = ar
ARFLAGS      = r
RANLIB       = echo