module purge
module load intel/2020.0
module load mkl/2020.0
module load papi/5.7.0

module list

export OMP_NUM_THREADS=1

