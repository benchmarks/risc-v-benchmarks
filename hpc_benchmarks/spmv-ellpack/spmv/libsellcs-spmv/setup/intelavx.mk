SELLCS_INCLUDES = -I. 
SELLCS_LIBS     = 
LIB_SUFFIX=



# ----------------------------------------------------------------------
# -DINDEX_64               Use 64-bit integer column indices instead of 32-bit
# -DUSE_OMP                Enables OpenMP parallel regions in the code
# -DALIGN_TO=<align_size>  Mallocs will be aligned to 'align_size' bytes. Default: 64
# -DEPI_EXT=v07            Possible values: v07 or empty; If left empty, builds with v0.9 routines

SELLCS_OPTS     =  -DALIGN_TO=1024 


# Index data type
ifdef INDEX64
    SELLCS_OPTS += -DINDEX64
    LIB_SUFFIX:=_i64
else
    LIB_SUFFIX:=_i32
endif

# Enable / diable OpenMP
ifdef OMP
    LIB_SUFFIX:=$(LIB_SUFFIX)_omp
    SELLCS_OPTS     += -DUSE_OMP
else
    LIB_SUFFIX:=$(LIB_SUFFIX)_sequential
endif


# ----------------------------------------------------------------------

SELLCS_DEFS     = $(SELLCS_OPTS) $(SELLCS_INCLUDES)

# ----------------------------------------------------------------------

# - Compilers / linkers - Optimization flags
# ----------------------------------------------------------------------

CC          = icc
AVCC        = $(CC) # Compiler used in Autovector files

ifdef DBG
   CFLAGS     = $(SELLCS_DEFS) -O0 -g -ggdb -DSELLCS_DEBUG $(PARAMS)
else
   CFLAGS     = $(SELLCS_DEFS) -std=c11 -O3 -DNDEBUG  $(PARAMS)
endif

ifdef OMP
    CFLAGS += -fopenmp
endif

AVCC_FLAGS = $(CFLAGS)

MASM        = 

LINKER       = $(CC)
LINKFLAGS    = $(CFLAGS) -static

ARCHIVER     = ar
ARFLAGS      = r
RANLIB       = echo

