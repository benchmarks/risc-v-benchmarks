# - DUMMY MAKEFILE

TOPdir       = ..
SRCdir       = $(TOPdir)/src
INCdir       = $(TOPdir)/src
BINdir       = $(TOPdir)/bin


TARGETS        = # THIS IS A TEMPLATE - NO BINARIES ARE COMPILED


# - Compilers / linkers - Optimization flags
# ----------------------------------------------------------------------

CC          = ismpcc
CFLAGS      += $(DGEMM_DEFS) -O3 --ompss -Wall -Wextra

LINKER       = $(CC)
LINKFLAGS    = $(CFLAGS)

ARCHIVER     = ar
ARFLAGS      = r
RANLIB       = echo