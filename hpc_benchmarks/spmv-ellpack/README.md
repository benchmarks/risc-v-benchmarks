This folder contains a long-vector implementation of the Sparse Matrix-Vector Multiplication kernel (SpMV).


#Compilation

To compile, load the `clang` compiler (in Arriesgado nodes, `module load llvm/EPI-0.7-development`) and run `make`.

The SpMV binary will be located at `./build/bin/spmv_sellcslib`

#Running

You can run the binary manually by executing:

```
out=`$prefix ./build/bin/spmv_sellcslib -v 256 -m path_to_inputs /home/pmtest/epi/ftp/sdvs/integration-functional-tests/resources/tests/spmv/spmv_inputs/cage${cage}/cage${cage}.mtx`
```

Note that the `-v` argument contains the maximum vector length in double-precision elements of the machine.

You can download input matrixes from [this ftp](https://ssh.hca.bsc.es/epi/ftp/sdvs/integration-functional-tests/resources/tests/spmv/spmv_inputs/).

Alternatively, run the binary with the `-p` argument insetad of `-m` to auto-generate a octodiagonal matrix of *p^2* rows and *p^2* columns:

```
out=`$prefix ./build/bin/spmv_sellcslib -v 256 -p 32
```
