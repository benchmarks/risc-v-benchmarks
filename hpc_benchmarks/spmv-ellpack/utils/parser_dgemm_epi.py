#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
import math
import argparse
import json

# def get_file_by_extension(path, extension):
#     for file in os.listdir(path):
#             if file.endswith(extension):
#                 return path + file

#     return ''


def parse_json_output(filepath):
    row = []
    
    with open(filepath, 'r') as json_file:
        d = json.load(json_file)

        benchmark   = d['Benchmark']
        version     = d['Algorithm version']
        date        = d['Execution date']
        v_test      = d['Verification test']
        num_threads = d['Number of threads']
        p_summary   = d['Problem summary']
        nx          = p_summary['Matrix NX size']
        ny          = p_summary['Matrix NY size']
        elements    = p_summary['Total elements']
        bsize       = p_summary['Block size']
        niter       = p_summary['Iterations']
        mem_usage   = d['Memory usage [MB]']
        p_stats     = d['Performance statistics']
        exe_time    = p_stats['Execution time [s]']
        gflops      = p_stats['Raw GFLOP/s']

        if (v_test == 'FAIL'):
            print('Warning: this execution {0} failed the validation test!'.format(os.path.split(filepath)[-1]))

    row = [benchmark, version, date, v_test, num_threads, nx, ny, bsize, niter, mem_usage,
        exe_time, gflops]
    return row

def main(root_folder):
    #method = 0
    table = []
    dgemm_header = ['Benchmark', 'Algorithm', 'DateTime', 'Verification', 'NumThreads', 'MatrixNx', 
                    'MatrixNy', 'BlockSize', 'NumIter', 'MemUsage', 'ExecutionTime', 'GFLOPs']
    output_files = []

    # if method == 1: # TODO: Try to avoid this method
    #     for repetition_folder in sorted(os.listdir(root_folder)):
    #         rep_folderpath = os.path.join(root_folder, repetition_folder)

    #         if not os.path.isdir(rep_folderpath):
    #                 continue

    #         for rank_folder in sorted(os.listdir(rep_folderpath)):
    #             rank_folderpath = os.path.join(rep_folderpath, rank_folder)

    #             if not os.path.isdir(rank_folderpath):
    #                 continue

    #             for thread_folder in sorted(os.listdir(rank_folderpath)):
    #                 thread_folderpath = os.path.join(rank_folderpath, thread_folder)

    #                 if not os.path.isdir(thread_folderpath):
    #                     continue

    #                 for file in sorted(os.listdir(thread_folderpath)):
    #                     if file.lower().endswith('out'):
    #                         output_files.append(os.path.join(thread_folderpath, file))

    # else:
    output_files = search_files(root_folder, 'out')

    print('Parsing: {0} files'.format( len(output_files)))

    
    for of in output_files:
        try:
            table.append(parse_json_output(of))
        except json.JSONDecodeError as jde:
            print('Could not parse this file: {0}'.format(of))
            pass

    dgemm_results = pd.DataFrame(table, columns=dgemm_header)
    
    folder_name = os.path.split(root_folder.strip('/'))[1]
    dgemm_results.to_csv('{0}/{1}_results.csv'.format(root_folder, folder_name), sep=',', index=False,
             header=True, float_format='%.5f', na_rep='nan')


def search_files(directory='.', extension=''):
    json_files = []

    extension = extension.lower()
    for dirpath, dirnames, files in os.walk(directory):
        for filename in files:
            if extension and filename.lower().endswith(extension):
                json_files.append(os.path.join(dirpath, filename))

    return json_files


if __name__ == "__main__":

    # Parse Arguments
    parser = argparse.ArgumentParser(description='Parse DGEMM EPI and dump to a CSV')
    parser.add_argument('RootFolder', type=str, nargs=1,
                            help='Path to the rootdir with DGEMM EPI benchmark ouputs.')
    args = parser.parse_args()
    root_folder = args.RootFolder[0]
    print(root_folder)

    main(root_folder)
