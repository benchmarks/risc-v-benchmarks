#!/bin/bash
trap "kill 0" EXIT

function executeBench () {

	binary=$1
	input_folder=$2
	output_folder=$3
	declare -a cores=("${!4}")
	declare -a chunks=("${!5}")
	declare -a imtx=("${!6}")
	bfolder=$7

	results_folder=${binary}_results
	mkdir -p ${results_folder}/${output_folder}

	for c in "${cores[@]}"
	do
		for cs in "${chunks[@]}"
		do
			for mtx in "${imtx[@]}"
			do
				echo "Testing matrix: ${mtx} ncores ${c} cs ${cs}. Binary=${binary}"
				# echo "OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -w 32768 -c ${cs} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.out 2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.err"
				OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -w 32768 -c ${cs} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.out 2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.err
			done
		done
	done

}


function executeBench_hwc () {
	binary=$1
	input_folder=$2
	output_folder=$3
	declare -a cores=("${!4}")
	declare -a chunks=("${!5}")
	declare -a imtx=("${!6}")
	bfolder=$7

	results_folder=${binary}_results
	mkdir -p ${results_folder}/${output_folder}

	for c in "${cores[@]}"
	do
		for cs in "${chunks[@]}"
		do
			for mtx in "${imtx[@]}"
			do
				for pm in "VECTOR-MEM" "VECTOR-OP"
				do
					echo "Testing matrix: ${mtx} ncores ${c} cs ${cs}. Binary=${binary}"
					# echo "OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -w 32768 -c ${cs} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.out 2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.err"
					 VE_PERF_MODE="${pm}" OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -w 32768 -c ${cs} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}_${pm}.out  2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}_${pm}.err
				done
			done
		done
	done
}


function executeBench_sigmasweep () {
	binary=$1
	input_folder=$2
	output_folder=$3
	declare -a cores=("${!4}")
	declare -a sigmas=("${!5}")
	declare -a imtx=("${!6}")
	bfolder=$7

	results_folder=${binary}_sigma
	mkdir -p ${results_folder}/${output_folder}

	for c in "${cores[@]}"
	do
		for sg in "${sigmas[@]}"
		do
			for mtx in "${imtx[@]}"
			do
				echo "Testing matrix: ${mtx} ncores ${c} sigma ${sg}. Binary=${binary}"
				# echo "OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -w 32768 -c ${sg} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.out 2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${cs}.err"
				OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 100 -c 4096 -w ${sg} -m ${input_folder}/${mtx}/${mtx}.mtx  > ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${sg}.out 2> ${results_folder}/${output_folder}/output_spmv_${mtx}_${c}_${sg}.err
			done
		done
	done
}

function executeBench_power () {
	binary=$1
	input_folder=$2
	output_folder=$3
	declare -a cores=("${!4}")
	declare -a chunks=("${!5}")
	declare -a imtx=("${!6}")
	bfolder=$7
	mode=$8

	results_folder=${binary}_power
	mkdir -p ${results_folder}/${output_folder}

	if [ "$mode" == "nlc" ]; then
		for c in "${cores[@]}"
		do
			for r in 1 2 3
			do
				for mtx in "${imtx[@]}"
				do
					# Read current measurements @ <VE_NODE_NUMBER> vector engine
					op=${results_folder}/${output_folder}/output_spmv_${mtx}_${c}th_r${r}
					powerfile=${op}_current.csv
					echo "Power[W]" > ${powerfile}
					echo "Testing matrix: ${mtx} ncores ${c}. Binary=${binary}"
					while true; do vecmd -N ${VE_NODE_NUMBER} info | egrep ^Current -A2 | grep -v Current | awk '{sum=sum + 12 * $5 / 1000}END{print sum}' >> ${powerfile}; sleep 1 ; done &
					PID1=$!
					
					run_nlc_cmd ${bfolder}/${binary} ${input_folder}/${mtx}/${mtx}.mtx ${c} 600000 ${op}
		
					kill -9 ${PID1}  		 # kill the background measurement process
					wait ${PID1} 2>/dev/null # Supresses Kill command messages
				done
			done
		done
	fi

	if [ "$mode" == "sellcs" ]; then
		for c in "${cores[@]}"
		do
			for cs in "${chunks[@]}"
			do
				for mtx in "${imtx[@]}"
				do
					# Read current measurements @ <VE_NODE_NUMBER> vector engine
					of=${results_folder}/${output_folder}/output_spmv_${mtx}_${c}th_${cs}cs
					powerfile=${of}_current.csv
					echo "Power[W]" > ${powerfile}
					echo "Testing matrix: ${mtx} ncores ${c} cs ${cs}. Binary=${binary}"
					while true; do vecmd -N ${VE_NODE_NUMBER} info | egrep ^Current -A2 | grep -v Current | awk '{sum=sum + 12 * $5 / 1000}END{print sum}' >> ${powerfile}; sleep 1 ; done &
					PID1=$!

					run_sellcs_cmd ${bfolder}/${binary} ${input_folder}/${mtx}/${mtx}.mtx ${c} 600000 ${cs} 32768 ${of}
					
					kill -9 ${PID1}
					wait ${PID1} 2>/dev/null
				done
			done
		done
	fi
}

function executeBench_blocking () {
	binary=$1
	input_folder=$2
	output_folder=$3
	declare -a cores=("${!4}")
	declare -a chunks=("${!5}")
	declare -a imtx=("${!6}")
	declare -a nblocks=("${!7}")
	bfolder=$8

	results_folder=${binary}_power
	mkdir -p ${results_folder}/${output_folder}

	for c in "${cores[@]}"
	do
		for nb in "${nblocks[@]}"
		do
			for cs in "${chunks[@]}"
			do
				for mtx in "${imtx[@]}"
				do
					# Read current measurements @ <VE_NODE_NUMBER> vector engine
					of=${results_folder}/${output_folder}/output_spmv_${mtx}_${c}th_${cs}cs
					echo "Testing matrix: ${mtx} ncores ${c} cs ${cs}. Binary=${binary}"
			
					OMP_NUM_THREADS=${c} ${bfolder}/${binary} -i 1000 -w 32768 -b ${nb} -c ${cs} -m ${input_folder}/${mtx}/${mtx}.mtx > ${of}.out 2> ${of}.err
			
				done
			done
		done
	done

}



function run_nlc_cmd () {
	binary_loc=$1
	input_mtx=$2
	c=$3
	it=$4
	out=$5

	OMP_NUM_THREADS=${c} ${binary_loc} -i ${it} -m ${input_mtx} > ${out}.out 2> ${out}.err
}

function run_sellcs_cmd () {
	binary_loc=$1
	input_mtx=$2
	c=$3
	it=$4
	cs=$5
	window=$6
	out=$7

	OMP_NUM_THREADS=${c} ${binary_loc} -i ${it} -w ${window} -c ${cs} -m ${input_mtx}  > ${out}.out 2> ${out}.err
}


bin_folder=../../../repos/spmv-study/spmv/build_sellcs/bin/
source ../llvm_ncc_env.sh

# input_mtx_block=( "TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1" "fullb" "fcondp2" "halfb" "BenElechi1" "ML_Laplace" "msdoor" "bundle_adj" "ML_Geer" "af_shell10")
# input_mtx_ibm=("pwtk" "mip1" "Si41Ge41H72" "ldoor" "bone010"  "crankseg_2")
# input_mtx_ibmbig=("nlpkkt240")
# input_mtx_wg=("webbase-1M" "dense2" "scircuit" "mc2depi" )
#
input_mtx_wg=("scircuit")
# input_mtx_block=("TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1")
# input_mtx_ibm=("mip1" "Si41Ge41H72" "crankseg_2")
# input_mtx_wg=("webbase-1M")

ncores=(1 2 4 8)
#ncores=(8)

chunksizes=(8192 16384 32768)
# chunksizes=(2048 4096)


###########################################################
# MAIN EXPERIMENTS
# SELLCS naive
# executeBench spmv_SELLCS_omp "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench spmv_SELLCS_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench spmv_SELLCS_omp "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS + FG
# executeBench spmv_SELLCS_FG_omp "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench spmv_SELLCS_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench spmv_SELLCS_FG_omp "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS + U8 + FG
# executeBench spmv_SELLCS_U8_FG_omp "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_FG_omp "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS U8 + NC + FG
# executeBench spmv_SELLCS_U8_NC_FG_omp "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_FG_omp "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS U8 + NC
# executeBench spmv_SELLCS_U8_NC_omp "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_omp "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"
# ##############################################
# # BIG CASES
# executeBench spmv_SELLCS_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench spmv_SELLCS_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_FG_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench spmv_SELLCS_U8_NC_omp "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"

# DENSE2
# dense_cs=(256)
# dense_mtx=("dense2")
# executeBench spmv_SELLCS_omp "../mtx_inputs/williams_group" wg ncores[@] dense_cs[@] dense_mtx[@] "$bin_folder"
# executeBench spmv_SELLCS_FG_omp "../mtx_inputs/williams_group" wg ncores[@] dense_cs[@] dense_mtx[@] "$bin_folder"

###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################


# HWC EXPERIMENTS
# SELLCS naive
# executeBench_hwc spmv_SELLCS_omp_hwc "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS + FG
# executeBench_hwc spmv_SELLCS_FG_omp_hwc "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_FG_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS + U8 + FG
# executeBench_hwc spmv_SELLCS_U8_FG_omp_hwc "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_FG_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS U8 + NC + FG
# executeBench_hwc spmv_SELLCS_U8_NC_FG_omp_hwc "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_FG_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"

# # SELLCS U8 + NC
# executeBench_hwc spmv_SELLCS_U8_NC_omp_hwc "../mtx_inputs/block_friendly" block ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder"
# ##############################################
# # VERY BIG CASES
# executeBench_hwc spmv_SELLCS_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_FG_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_U8_NC_omp_hwc "../mtx_inputs/ibm_hpc_friendly" ibm ncores[@] chunksizes[@] input_mtx_ibmbig[@] "$bin_folder"

# # DENSE2
# dense_cs=(256)
# executeBench_hwc spmv_SELLCS_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] dense_cs[@] dense_mtx[@] "$bin_folder"
# executeBench_hwc spmv_SELLCS_FG_omp_hwc "../mtx_inputs/williams_group" wg ncores[@] dense_cs[@] dense_mtx[@] "$bin_folder"

#########################
#SIGMASWEEP
sigsizes=(10000000)
input_mtx_ibm=("pwtk" "mip1" "Si41Ge41H72" "ldoor" "bone010"  "crankseg_2")
input_mtx_block=( "TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1" "fullb" "fcondp2" "halfb" "BenElechi1" "ML_Laplace" "msdoor" "bundle_adj" "ML_Geer")
input_mtx_wg=("webbase-1M" "dense2" "scircuit" "mc2depi" )

ncores=(8)
# executeBench_sigmasweep spmv_SELLCS_omp "../mtx_inputs/ibm_hpc_friendly" any ncores[@] sigsizes[@] input_mtx_ibm[@] "$bin_folder"
# executeBench_sigmasweep spmv_SELLCS_FG_omp "../mtx_inputs/ibm_hpc_friendly" any ncores[@] sigsizes[@] input_mtx_ibm[@] "$bin_folder"

# executeBench_sigmasweep spmv_SELLCS_omp "../mtx_inputs/block_friendly" any ncores[@] sigsizes[@] input_mtx_block[@] "$bin_folder"
# executeBench_sigmasweep spmv_SELLCS_FG_omp "../mtx_inputs/block_friendly" any ncores[@] sigsizes[@] input_mtx_block[@] "$bin_folder"

# executeBench_sigmasweep spmv_SELLCS_omp "../mtx_inputs/williams_group" any ncores[@] sigsizes[@] input_mtx_wg[@] "$bin_folder"
# executeBench_sigmasweep spmv_SELLCS_FG_omp "../mtx_inputs/williams_group" any ncores[@] sigsizes[@] input_mtx_wg[@] "$bin_folder"



#########################
# Power measurements
chunksizes=(2048 4096)
ncores=(8)
# input_mtx_ibm=("crankseg_2" "pwtk" "mip1" "Si41Ge41H72" "ldoor" "bone010")
# input_mtx_block=( "TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1" "fullb" "fcondp2" "halfb" "BenElechi1" "ML_Laplace" "msdoor" "bundle_adj" "ML_Geer" "af_shell10")
# input_mtx_wg=("webbase-1M" "dense2" "scircuit" "mc2depi" )

input_mtx_ibm=( "ldoor" "bone010")
input_mtx_block=( "BenElechi1" "ML_Geer" "af_shell10")
input_mtx_wg=("webbase-1M")


executeBench_power spmv_SELLCS_U8_NC_omp "../mtx_inputs/williams_group" all ncores[@] chunksizes[@] input_mtx_wg[@] "$bin_folder" "sellcs"
executeBench_power spmv_SELLCS_U8_NC_omp "../mtx_inputs/block_friendly" all ncores[@] chunksizes[@] input_mtx_block[@] "$bin_folder" "sellcs"
executeBench_power spmv_SELLCS_U8_NC_omp "../mtx_inputs/ibm_hpc_friendly" all ncores[@] chunksizes[@] input_mtx_ibm[@] "$bin_folder" "sellcs"


# NLC
nlc_bin_folder=../../../repos/spmv-study/spmv/build_nlc/bin/
source ../aurora_env.sh


input_mtx_ibm=("crankseg_2" "pwtk" "mip1" "Si41Ge41H72" "ldoor" "bone010")
input_mtx_block=( "TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1" "fullb" "fcondp2" "halfb" "BenElechi1" "ML_Laplace" "msdoor" "bundle_adj" "ML_Geer" "af_shell10")
input_mtx_wg=("webbase-1M" "dense2" "scircuit" "mc2depi" )


executeBench_power spmv_nlc "../mtx_inputs/williams_group" all ncores[@] chunksizes[@] input_mtx_wg[@] "$nlc_bin_folder" "nlc"
executeBench_power spmv_nlc "../mtx_inputs/block_friendly" all ncores[@] chunksizes[@] input_mtx_block[@] "$nlc_bin_folder" "nlc"
executeBench_power spmv_nlc "../mtx_inputs/ibm_hpc_friendly" all ncores[@] chunksizes[@] input_mtx_ibm[@] "$nlc_bin_folder" "nlc"

###################### ESB - SELLCS BLOCKING #########################
ncores=(8)
chunksizes=(4096 8192)
numblocks=(1 4 16 64 256 1024)

input_mtx_ibm=("crankseg_2" "pwtk" "mip1" "Si41Ge41H72" "ldoor" "bone010")
input_mtx_block=( "TSOPF_RS_b2383" "s4dkt3m2" "torso1" "bmw7st_1" "fullb" "fcondp2" "halfb" "BenElechi1" "ML_Laplace" "msdoor" "bundle_adj" "ML_Geer" "af_shell10")
input_mtx_wg=("webbase-1M" "dense2" "scircuit" "mc2depi" )

executeBench_blocking spmv_SELLCS_blocked_omp "../mtx_inputs/block_friendly" all ncores[@] chunksizes[@] input_mtx_block[@] numblocks[@] "$bin_folder"
executeBench_blocking spmv_SELLCS_blocked_omp "../mtx_inputs/ibm_hpc_friendly" all ncores[@] chunksizes[@] input_mtx_ibm[@] numblocks[@] "$bin_folder"
executeBench_blocking spmv_SELLCS_blocked_omp "../mtx_inputs/williams_group" all ncores[@] chunksizes[@] input_mtx_wg[@] numblocks[@] "$bin_folder"

