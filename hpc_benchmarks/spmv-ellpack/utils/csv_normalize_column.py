#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
import math
import argparse
import json
import fnmatch
import subprocess
import re
import unittest
from datetime import datetime

def main(filepath, args):
    norm_cols = args.norm_cols
    groupby_col = args.group_by[0]
    norm_wrt = args.norm_wrt[0]
    print(f'Normalizing {norm_cols}, grouping by column {groupby_col}, baseline is {norm_wrt}')
    df = pd.read_csv(filepath, header=0, comment='#', dtype=None, skipinitialspace=True, engine='python')
    group_values = pd.unique(df[groupby_col])

    new_cols = [f'Norm{c}' for c in norm_cols]
    df = df.reindex(columns=[*df.columns.tolist(), *new_cols], fill_value=0)

    for g in group_values:
        baseline_val = 1.0
        # get the baseline
        if norm_wrt == 'min':
            baseline_val = df.loc[df[groupby_col] == g, norm_cols].min()
        elif norm_wrt == 'max':
            baseline_val = df.loc[df[groupby_col] == g, norm_cols].max()
        else:
            split = norm_wrt.split(',')
            baseline_col = split[0]
            baseline_cat = split[1]
            baseline_val = df.loc[(df[groupby_col] == g) & (df[baseline_col].astype(str) == baseline_cat), norm_cols].values[0]
        
        print(f'baseline_val is {baseline_val}, for group {g}')
        

        for idx, nc, c in zip(range(len(new_cols)), new_cols, norm_cols):
            df.loc[df[groupby_col] == g, nc] = df.loc[df[groupby_col] == g, c] / baseline_val[idx]


    output_filepath = filepath.replace('.csv', '_norm.csv')
    df.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
                        na_rep='nan')

if __name__ == "__main__":

    # Parse Arguments
    parser = argparse.ArgumentParser(description='Parse SPMV EPI outputs and dump them to a CSV')
    parser.add_argument('FilePath', type=str, nargs=1, help='Path to the CSV')
    parser.add_argument('--norm-cols', type=str, nargs='*', help='Column to normalize')
    parser.add_argument('--group-by', type=str, nargs=1, help='Column to Group by')
    parser.add_argument('--norm-wrt', type=str, nargs=1, help='')

    args = parser.parse_args()
    fp = args.FilePath[0]
    print(fp)

    main(fp, args)
