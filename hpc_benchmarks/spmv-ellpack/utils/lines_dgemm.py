#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys
import seaborn as sns



def lines(df, x_columns, values_col, series_col, output_filepath, title=""):
    
    series_values   = sorted(pd.unique(df[series_col]))
    order_mask = [True for col in x]
    
    #print(df["x_labels"])
    
    fig, ax = plt.subplots(figsize=(10,4)) 

    df.sort_values(x_columns, ascending=order_mask, inplace=True)

    df['x_labels'] = df[x_columns].apply(lambda x: ', '.join(x[x.notnull()].astype(str)), axis = 1)
    x_tick_labels = df['x_labels'].tolist()
    x_ticks_position = np.arange(1, len(x_tick_labels)+1)
    ax.set_xticks(x_ticks_position)
    ax.xaxis.grid(True, linestyle=":", color="grey")


    ax.set_xticklabels(x_tick_labels, rotation=45, fontsize=8)

    for sv in series_values:

        current_serie_dataframe = df[df[series_col] == sv]
        #current_serie_dataframe.sort_values(x_columns, ascending=order_mask, inplace=True)
        #current_serie_dataframe['x_labels'] = current_serie_dataframe[x_columns].apply(lambda x: ', '.join(x[x.notnull()].astype(str)), axis = 1)

        #x_tick_labels = current_serie_dataframe['x_labels'].tolist()
        #x_ticks_position = np.arange(1, len(x_tick_labels)+1)
        values = current_serie_dataframe[values_col].tolist()

        print(values)

        #ax.plot(x_ticks_position, values)
        sns.lineplot('x_labels', values_col, current_serie_dataframe)
        #ax.set_xticklabels(x_tick_labels, rotation=45, fontsize=8)

    plt.show()
    #plt.savefig(output_filepath, bbox_inches='tight', face_color="white", transparent="true")




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates a heatmap plot')
    parser.add_argument('path_to_csv', nargs=1, type=str, help='File with input data in CSV format')
    parser.add_argument('--x', nargs='*', type=str, help='Name of the column(s) to use in the X axis.')
    parser.add_argument('--series', nargs=1, type=str, help='Name of the column of the series.')
    parser.add_argument('--values', nargs=1, type=str, help='Name of the column to use as Values.')
    parser.add_argument('--filterIn', type=str, nargs=2, help='filter the dataframe')
    parser.add_argument('--title', nargs=1, type=str, help='')

    args = parser.parse_args()

    f = args.path_to_csv[0]
    x = args.x
    values = args.values[0]
    series = args.series[0]
    title = args.title[0]

    input_dataframe = pd.read_csv(f, header=0, comment='#', dtype=None, skipinitialspace=True, engine='python')
    
    # Filter dataframe
    if args.filterIn:
        filter_column1  = args.filterIn[0]
        filter_value1   = args.filterIn[1]
        input_dataframe = input_dataframe[input_dataframe[filter_column1].astype(str) == filter_value1]
        print('Column -{0}- filtered in by \'{1}\''.format(filter_column1, filter_value1))

    
    output_filepath = '{0}-lines-{1}-{2}.pdf'.format(os.path.splitext(f)[0], str(filter_column1), str(filter_value1) )
    print(output_filepath)


    lines(input_dataframe, x, values, series, title, output_filepath)


