#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
import math
import argparse
import json
import fnmatch
import subprocess
import re
import unittest
from datetime import datetime


common_fields = ['USRCC', 'EX', 'VX', 'FPEC']
vop_fields = ['VE', 'VECC', 'L1MCC', 'VE2', 'VAREC', 'VLDEC', 'PCCC', 'VLDCC', 'VLEC', 'VLCME']
mem_fields = ['L1IMC', 'L1IAC', 'L1OMC', 'L1OAC', 'L2MC', 'L2AC', 'BREC', 'BPFC', 'VLXC', 'VLCMX', 'FMAXC']

# def get_file_by_extension(path, extension):
#     for file in os.listdir(path):
#             if file.endswith(extension):
#                 return path + file

#     return ''
def column_to_row_reference_results(df):
    for index, row in df.iterrows():
        df = df.append({'Benchmark' : 'SPMV EPI',
                            'Algorithm' : 'reference',
                            'MatrixName': row['MatrixName'],
                            'MatrixNumRows': row['MatrixNumRows'],
                            'GFLOPs' : row['RefGFLOPs']},
                            ignore_index=True)

    return df

def fix_json_file(filepath):

    # Regex to match a comma out of place at the end of a JSON block
    rgx_comma = re.compile(r'(?P<comma>,)(\n\s*})', flags = re.MULTILINE)
    rgx_section = re.compile(r'(?P<txt>All iterations),', flags = re.MULTILINE)

    with open(filepath, "r+") as f:
        text_to_fix = f.read() # read everything in the file

        text_to_fix = re.sub(rgx_comma, r'\2', text_to_fix)
        text_to_fix = re.sub(rgx_section, r'"\g<txt>",', text_to_fix)
        f.seek(0) # rewind
        # print(fixed_text)
        f.write(text_to_fix) # write the new line before


def pmc_text_to_dict(block_text, mode):
    d = json.loads(block_text)
    counters = d['VE counters']
    row = []
    pmc_dict = dict()
    pmc_dict['USRCC'] = counters.get('User Cycles', np.nan)
    pmc_dict['EX'] = counters.get('Exec. Inst.', np.nan)                        # PMC00 - Execution count (EX)
    pmc_dict['VX'] = counters.get('Vec. Inst. Execution', np.nan)               # PMC01 - Vector execution count (VX)
    pmc_dict['FPEC'] = counters.get('FP Data Elements', np.nan)                 # PMC02 - Floating point data element count (FPEC) ENABLE with new results
    # VECTOR-OP
    pmc_dict['VE']    = counters.get('Vec. Elements', np.nan)                   # PMC03 - Vector elements count
    pmc_dict['VECC']  = counters.get('Vec. Exec. Cycles', np.nan)               # PMC04 - Vector execution clock count
    pmc_dict['L1MCC'] = counters.get('L1$ Miss Cycles', np.nan)                # PMC05 - L1 cache miss clock count
    pmc_dict['VE2']   = counters.get('Vec. Elements 2', np.nan)                # PMC06 - Vector elements count 2
    pmc_dict['VAREC'] = counters.get('Vec. Arithmetic exec. Cycles', np.nan)    # PMC07 - Vector arithmetic execution clock count
    pmc_dict['VLDEC'] = counters.get('Vec. Load execution Cycles', np.nan)      # PMC08 - Vector load execution clock count
    pmc_dict['PCCC']  = counters.get('Port Conflict Cycles', np.nan)            # PMC09 - Port conflict clock count
    pmc_dict['VLDCC'] = counters.get('Vec. Loaded Packets', np.nan)             # PMC10 - Vector load delayed clock count !!!! ESTE ESTA MAL EN EL PDF OFICIAL???
    pmc_dict['VLEC']  = counters.get('Vec. Loaded elements', np.nan)            # PMC11 - Vector load element count
    pmc_dict['VLCME'] = counters.get('Vec. Load Cache Miss Elements', np.nan)   # PMC12 -Vector load cache miss element count
    # VECTOR-MEM
    pmc_dict['L1IMC'] = counters.get('L1I$ Miss Count', np.nan)             # PMC03 - L1 instruction cache miss count (L1IMC)       
    pmc_dict['L1IAC'] = counters.get('L1I$ Access Count', np.nan)           # PMC04 - L1 instruction cache access count (L1IAC)
    pmc_dict['L1OMC'] = counters.get('L1D$ Miss Count', np.nan)             # PMC05 - L1 operand cache miss count (L1OMC)
    pmc_dict['L1OAC'] = counters.get('L1D$ Access Count', np.nan)           # PMC06 - L1 operand cache access count (L1OAC)
    pmc_dict['L2MC']  = counters.get('L2$ Miss Count', np.nan)              # PMC07 - L2 cache miss count (L2MC)
    pmc_dict['L2AC']  = counters.get('L2$ Access Count', np.nan)            # PMC08 - L2 cache access count (L2AC)
    pmc_dict['BREC']  = counters.get('Branch Exec. Count', np.nan)          # PMC09 - Branch execution count (BREC)
    pmc_dict['BPFC']  = counters.get('Branch Pred. Fail Count', np.nan)     # PMC10 - Branch prediction failure count (BPFC)
    pmc_dict['VLXC']  = counters.get('Vec. Load Exec. Count', np.nan)       # PMC11 - Vector load execution count (VLXC)
    pmc_dict['VLCMX'] = counters.get('Vec. Load Miss Exec. Count', np.nan)  # PMC12 - Vector load cache miss execution count (VLCMX)
    pmc_dict['FMAXC'] = counters.get('FMA Exec. Count', np.nan)             # PMC13 - Fused multiply add execution count (FMAXC)

    return pmc_dict



def parse_hwc_file(filepath):
    base_name = os.path.basename(filepath)
    pmc_mode = 'VECTOR-{0}'.format(re.search('VECTOR-(.*)\.err', base_name).group(1))
    all_fields = common_fields + vop_fields + mem_fields

    # Regular expression to match separately the PMC values for each core
    rgx_block = re.compile(r'(?P<block>{.*?}\n})', flags = re.MULTILINE | re.S | re.U)

    '''
        Load the text file into dictionaries
    '''
    cores_pmc_info_raw = None
    with open(filepath, "r+") as f:
        full_text = f.read()
        cores_pmc_info_raw = rgx_block.finditer(full_text)

    results = [pmc_mode]
    num_cores = 0
    
    dicts = []
    for m in cores_pmc_info_raw:
        dicts.append(pmc_text_to_dict(m['block'], pmc_mode))
        num_cores += 1

    '''
        Place the pmc values in an array and return it
    '''
    for pmc in all_fields:
        for i in range(8):
            if i < num_cores:
                results.append(dicts[i][pmc])
            else:
                results.append(np.nan)
                
    assert(len(results) == int(len(all_fields)*8)+1)
    return results

def parse_json_output(filepath):
    row = []
    print(filepath)
    with open(filepath, 'r') as json_file:
        d = json.load(json_file)

        benchmark   = d['Benchmark']
        version     = d['Algorithm version']
        date        = d['Execution date']
        v_test      = d['Verification test']
        num_threads = d['Number of threads']

        p_summary   = d['Problem summary']
        mtx_name    = p_summary['Input name']
        nrows       = p_summary['Matrix Num. Rows']
        ncols       = p_summary['Matrix Num. Columns']
        nnz         = p_summary['Total non-zero elements']
        nnz_per_row = p_summary['Non-zero elements per row']
        niter       = p_summary['Num. averaged iterations']

        mem_summary     = d['Memory statistics']
        mem_alloc       = mem_summary['Total memory allocated [MB]']
        ref_read_bw     = mem_summary['Reference version read BW [GB/s]']
        ref_write_bw    = mem_summary['Reference version read WR [GB/s]']
        custom_read_bw  = mem_summary['{0} version read BW [GB/s]'.format(version)]
        custom_write_bw = mem_summary['{0} version write BW [GB/s]'.format(version)]

        p_stats         = d['Performance statistics']
        alloc_time      = p_stats['Time allocating and loading data [s]']
        preprocess_time = p_stats['Time converting to {0} format [s]'.format(version)]
        ref_exe_time    = p_stats['Reference version execution time [s]']
        ref_gflops      = p_stats['Reference version GFLOPS/s']
        custom_exe_time = p_stats['{0} version execution time [s]'.format(version)]
        custom_gflops   = p_stats['{0} version GFLOPS/s'.format(version)]

        if 'Version specific stats' in d:
            p_vsstats   = d['Version specific stats']
            task_size   = p_vsstats.get('Task Size', 0)
            sigma_window = p_vsstats.get('Row order sigma window', 0)
            num_blocks = p_vsstats.get('Num. Blocks', 0)
        else:
            print(filepath)
            task_size    = 0
            sigma_window = 0
            num_blocks = 0

        if (v_test == 'FAIL'):
            print('Warning: this execution {0} failed the validation test!'.format(os.path.split(filepath)[-1]))

    row = [benchmark, version, date, v_test, num_threads, task_size, sigma_window, num_blocks, mtx_name, nrows, ncols, nnz, nnz_per_row, niter, mem_alloc,
           ref_read_bw, ref_write_bw, custom_read_bw, custom_write_bw, alloc_time, preprocess_time, ref_exe_time, ref_gflops,
           custom_exe_time, custom_gflops]

    return row

def get_ftrace_output(filepath):

    base_filename = os.path.basename(filepath)
    cmd = 'ssh aurora \'source /home/bsc/gcrespo/EPI/repos/vectorized_codes/spmv/aurora_environment.sh && ftrace -f /home/bsc/gcrespo/EPI/results/spmv/adaptgather_v2_ve_block_002_mretainx_ftrace/{0}\''.format(base_filename)
    try:
        result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
       
        return result.stdout.decode('utf-8')
    except subprocess.CalledProcessError as cpe:
        print('ERROR # Exception captured while running cmd: [{0}]'.format(cpe.cmd))
        print('ERROR # Output: {0}'.format(cpe.output))
    
def insert_ftrace_stats(df, row, new_columns, keys_dict):
    # # Keys dict can have variable number of key/value pairs, create dynamically a query
    # query = ' and '.join([f'{k}==\"{v}\"' for k, v in keys_dict.items()]) 
    new_col_values = [row[i] for i in range(len(new_columns))] ## ??? why is this necessary

    df.loc[(df['MatrixName'] == keys_dict['MatrixName']) & (df['NumThreads'] == int(keys_dict['NumThreads'])) & (df['FTMode'] == keys_dict['FTMode']), new_columns] = new_col_values
    # print(df.loc[(df['MatrixName'] == keys_dict['MatrixName']) & (df['NumThreads'] == int(keys_dict['NumThreads'])), new_columns])
    return df

def get_ftrace_stats_byfunc(text, function_name, header, mode):
    row = []

    if mode == "VECTOR-OP":
        pattern = re.compile('(?:[\s]*(?P<Freq>[0-9]+)'
                                '[\s]*(?P<Time>[0-9]*[.][0-9]*)\([\s]*(?P<TimePercent>[0-9]*[.][0-9]*)\)'
                                '[\s]*(?P<AlgAvgRuntime>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<MOPS>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<MFLOPS>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VOPRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VOPAvgLen>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VecTime>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<L1Miss>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<PortConflict>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VLDLLCHitRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<FuncName>{0})\n)'.format(function_name))

    elif mode ==  "VECTOR-MEM":

        pattern = re.compile('(?:[\s]*(?P<Freq>[0-9]+)'
                                '[\s]*(?P<Time>[0-9]*[.][0-9]*)\([\s]*(?P<TimePercent>[0-9]*[.][0-9]*)\)'
                                '[\s]*(?P<AlgAvgRuntime>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<MOPS>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<MFLOPS>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VOPRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VOPAvgLen>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<VecTime>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<L1IMissRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<L1DMissRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<L2MissRatio>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<RBpF>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<RSTBpF>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<RLDBpF>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<ACTVLDBpF>[0-9]*[.][0-9]*)'
                                '[\s]*(?P<FLOPCount>[0-9]+)'
                                '[\s]*(?P<FMAEle>[0-9]+)'
                                '[\s]*(?P<FuncName>{0})\n)'.format(function_name))
    
    matches = re.finditer(pattern, text)

    function_datarows = []
    for m in matches:
        row = [m[col_name] for col_name in header]
        function_datarows.append(row)

    return row

def get_ets_ve_output(power_filepath):
    df = pd.read_csv(power_filepath, header=0, comment='#', dtype=None, skipinitialspace=True, engine='python')
    average_power = df['Power[W]'].astype(float).mean() # ITS ACTUALLY POWER it is already multiplied by 12, the voltage.
    energy_to_solution =  df['Power[W]'].astype(float).sum() # It is very basic but samples are obtained every second (sleep 1)

    return [average_power, energy_to_solution]

def parse_output_and_current_ve(spmv_header, output_files):
    spmv_header.extend(["AvgPower", "EnergyToSolution"])
    table = []

    for of in output_files:
        base_name = os.path.basename(of)
        power_filepath = of.replace('.out','_current.csv')

        try:
            basic_output = parse_json_output(of)
            basic_output.extend(get_ets_ve_output(power_filepath))
            table.append(basic_output)

        except json.JSONDecodeError as jde:
            print(f'Could not parse this file: {of}')
            pass
    
    spmv_results_with_jobenergy = pd.DataFrame(table, columns=spmv_header)
    # Add power per iteration
    spmv_results_with_jobenergy["ETS/iteration"] = spmv_results_with_jobenergy['EnergyToSolution'] / spmv_results_with_jobenergy['NumIter']
    folder_name = os.path.split(root_folder.strip('/'))[1]
    output_filepath = f'{root_folder}/{folder_name}_stats.csv'
    print(f'Writting stats file to: {output_filepath}')
    spmv_results_with_jobenergy.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')

def get_ets_nvsmi_output(power_filepath):
    df = pd.read_csv(power_filepath, header=0, comment='#', dtype=None, skipinitialspace=True, engine='python')
    # Get the total seconds between start and end of measurements
    df['timestamp'] = pd.to_datetime(df.timestamp)
    init_time = df['timestamp'].min() 
    finish_time = df['timestamp'].max()
    delta_time = finish_time - init_time        # The resulting delta_time is a DeltaTime object. DeltaTime = (DateTime.a - DateTime.b)
    total_seconds = delta_time.total_seconds()

    df['Power'] = df['power.draw [W]'].str.replace(' W', '')
    average_power = df['Power'].astype(float).mean()

    consumed_energy = total_seconds * average_power

    return [average_power, consumed_energy]

def parse_output_and_nvsmi(spmv_header, output_files):
    spmv_header.extend(["AvgPower", "EnergyToSolution"])
    table = []

    for of in output_files:
        base_name = os.path.basename(of)
        job_id = re.search('_(\d*)\.out', base_name).group(1)
        power_filepath = of.replace(job_id, f'power_{job_id}').replace('.out','.csv')

        try:
            basic_output = parse_json_output(of)
           
            basic_output.extend(get_ets_nvsmi_output(power_filepath))
            table.append(basic_output)

        except json.JSONDecodeError as jde:
            print(f'Could not parse this file: {of}')
            pass
    
    spmv_results_with_jobenergy = pd.DataFrame(table, columns=spmv_header)
    spmv_results_with_jobenergy["ETS/iteration"] = spmv_results_with_jobenergy['EnergyToSolution'] / spmv_results_with_jobenergy['NumIter']


    folder_name = os.path.split(root_folder.strip('/'))[1]
    output_filepath = f'{root_folder}/{folder_name}_stats.csv'
    print(f'Writting stats file to: {output_filepath}')
    spmv_results_with_jobenergy.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')

def get_ets_sacct_output(jobid):
    # rgx_ets = re.compile(r'^\d+\s+([0-9]*).*\n', flags= re.M)
    rgx_ets = re.compile(r'^\d+\s+(\d{2}:\d{2}:\d{2})\s+([0-9]*).*\n', flags= re.M)

    cmd = f'sacct -j {jobid} -o \"JobID, Elapsed, ConsumedEnergyRaw\"'

    try:
        result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
        # Get the energy to solution refering only to the executed command
        txt = result.stdout.decode('utf-8')
        timestring = re.search(rgx_ets, txt).group(1)
        energy_to_solution = int(re.search(rgx_ets, txt).group(2))

        pt = datetime.strptime(timestring,'%H:%M:%S')
        total_seconds = pt.second + pt.minute*60 + pt.hour*3600
        
        power = energy_to_solution / total_seconds
        # print(res)
        return [power, energy_to_solution]
    except subprocess.CalledProcessError as cpe:
        print(f'ERROR # Exception captured while running cmd: [{cpe.cmd}]')
        print(f'ERROR # Output: {cpe.output}')

def parse_output_and_sacct(spmv_header, output_files):
    spmv_header.extend(["AvgPower", "EnergyToSolution"])
    table = []

    for of in output_files:
        try:
            basic_output = parse_json_output(of)
            base_name = os.path.basename(of)
            job_id = int(re.search('_(\d*)\.out', base_name).group(1))

            basic_output.extend(get_ets_sacct_output(job_id))
            table.append(basic_output)

        except json.JSONDecodeError as jde:
            print(f'Could not parse this file: {of}')
            pass

   
    spmv_results_with_jobenergy = pd.DataFrame(table, columns=spmv_header)
    spmv_results_with_jobenergy["ETS/iteration"] = spmv_results_with_jobenergy['EnergyToSolution'] / spmv_results_with_jobenergy['NumIter']


    folder_name = os.path.split(root_folder.strip('/'))[1]
    output_filepath = f'{root_folder}/{folder_name}_stats.csv'
    print(f'Writting stats file to: {output_filepath}')
    spmv_results_with_jobenergy.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')


def generate_pmc_stats(df):
    max_num_cores = 8
    all_fields = common_fields + vop_fields + mem_fields

    columns_to_drop = []
    for counter in all_fields:
        counter_cols = [f'{counter}#{x}' for x in range(max_num_cores)]
        df[f'Total{counter}'] = df[counter_cols].sum(axis=1)
        df[f'Max{counter}'] = df[counter_cols].max(axis=1)
        df[f'Avg/Max{counter}'] =  df[counter_cols].mean(axis=1) / df[counter_cols].max(axis=1)
        # df[f'STD{counter}'] = df[counter_cols].std(axis=1)
        # df[f'Avg{counter}'] = df[counter_cols].mean(axis=1)

        columns_to_drop.extend(counter_cols)

    # We generate another DataFrame (df_stats) without the RAW counters per core
    df_stats = df.drop(columns_to_drop, axis=1)

    ###################################################################################
    # Add more stats to that dataframe
    df_stats['ROITime'] = (df_stats['MaxUSRCC'] / 1400000000 ) # Should be ~ ExeTime * NumIter
    df_stats['IPC'] = df_stats['TotalEX'] / df_stats['MaxUSRCC']
    df_stats['VIPC'] = df_stats['TotalVX'] / df_stats['MaxUSRCC']
    df_stats['VIRatio'] = df_stats['TotalVX'] / df_stats['TotalEX']

    # VECTOR-MEM
    df_stats['L1MissRatio'] = df_stats['TotalL1OMC'] / df_stats['TotalL1OAC']
    df_stats['L1DMPKI'] = (df_stats['TotalL1OMC'] / df_stats['TotalEX']) * 1000
    df_stats['L2MissRatio'] = df_stats['TotalL2MC'] / df_stats['TotalL2AC']
    df_stats['L2MPKI'] = (df_stats['TotalL2MC'] / df_stats['TotalEX']) * 1000
    df_stats['L2MPKVI'] = (df_stats['TotalL2MC'] / df_stats['TotalVX']) * 1000

    for counter in vop_fields:
        df_stats[f'{counter}/cycle'] = df_stats[f'Total{counter}'] / df_stats['MaxUSRCC']

    for counter in mem_fields:
        df_stats[f'{counter}/s'] = df_stats[f'Total{counter}'] / (df_stats['MaxUSRCC'] / 1400000000)

    #df_stats['MOPS'] = ( df_stats['TotalEX'] - df_stats['TotalVX'] +  df_stats['TotalVE'] + df_stats['TotalVE'] ) #did not parse FMAEC
    df_stats['MFLOPS'] = df_stats['TotalFPEC'] / (df_stats['MaxUSRCC'] / 1400000000)
    df_stats['VOPRatio'] =  df_stats['TotalVE'] / ((df_stats['TotalEX'] - df_stats['TotalVX'] +  df_stats['TotalVE'] )) * 100
    df_stats['VOPAvgLen'] =  df_stats['TotalVE'] / df_stats['TotalVX'] # Average Vector Len = V Instructions / V Elements
    df_stats['VectorTime'] = df_stats['TotalVECC'] / 1400000000
    df_stats['L1MissTime'] =  df_stats['TotalL1MCC'] / 1400000000
    df_stats['PortConflictTime'] = df_stats['TotalPCCC'] / 1400000000
    df_stats['VLDLLCHitRatio'] = (1 - (df_stats['TotalVLCME'] / df_stats['TotalVLEC'])) * 100

    return df_stats

def parse_output_and_pmc_ve(spmv_header, output_files, error_files):
    max_num_cores = 8        
    all_fields = common_fields + vop_fields + mem_fields

    extended_header = []
    for counter in all_fields:
        for i in range(max_num_cores):
            extended_header.append(f'{counter}#{i}')

    spmv_header += ['PMCMode'] + extended_header
    print('Length of SPMV header is {0}.'.format(len(spmv_header)))

    assert(len(output_files) == len(error_files))
    
    table = []
    for of, ef in zip(output_files, error_files):
        assert(of[:-3] == ef[:-3])

        try:
            basic_output = parse_json_output(of)
            hwc_output = parse_hwc_file(ef)
            merged = basic_output + hwc_output
            assert(len(merged) == len(spmv_header))

            table.append(merged)
        except json.JSONDecodeError as jde:
            print('Could not parse this file: {0}'.format(of))
            pass

    spmv_results = pd.DataFrame(table, columns=spmv_header)

    folder_name = os.path.split(root_folder.strip('/'))[1]
    output_filepath = '{0}/{1}_results.csv'.format(root_folder, folder_name)
    print('Writting results to: {0}'.format(output_filepath))
    spmv_results.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')

    spmv_pmc_stats = generate_pmc_stats(spmv_results)
    folder_name = os.path.split(root_folder.strip('/'))[1]
    output_filepath = '{0}/{1}_stats.csv'.format(root_folder, folder_name)
    print('Writting stats file to: {0}'.format(output_filepath))

    spmv_pmc_stats.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
                            na_rep='nan')

    ## AGREGAR rows de diferentes PMCs para marc # Esto se puede automatizar para muchos counters (first en common, max en vop y mem)

    aggregation_functions = {   'GFLOPs': 'first', 'PMCMode': 'first', 'TotalUSRCC': 'first', 'Avg/MaxUSRCC': 'first',
                                'TotalEX': 'first', 'MaxEX': 'first', 'Avg/MaxEX': 'first',
                                'TotalVX': 'first', 'MaxVX': 'first', 'Avg/MaxVX': 'first',
                                'TotalVE': 'max', 'MaxVE': 'max', 'Avg/MaxVE': 'max',
                                'TotalVECC': 'max', 'MaxVECC': 'max', 'Avg/MaxVECC': 'max',
                                'TotalL1MCC': 'max', 'MaxL1MCC': 'max', 'Avg/MaxL1MCC': 'max',
                                'TotalVE2': 'max', 'MaxVE2': 'max', 'Avg/MaxVE2': 'max',
                                'TotalVAREC': 'max', 'MaxVAREC': 'max', 'Avg/MaxVAREC': 'max',
                                'TotalVLDEC': 'max', 'MaxVLDEC': 'max', 'Avg/MaxVLDEC': 'max',
                                'TotalPCCC': 'max', 'MaxPCCC': 'max', 'Avg/MaxPCCC': 'max',
                                'TotalVLDCC': 'max', 'MaxVLDCC': 'max', 'Avg/MaxVLDCC': 'max',
                                'TotalVLEC': 'max', 'MaxVLEC': 'max', 'Avg/MaxVLEC': 'max',
                                'TotalVLCME': 'max', 'MaxVLCME': 'max', 'Avg/MaxVLCME': 'max',

                                'TotalL1IMC': 'max', 'MaxL1IMC': 'max', 'Avg/MaxL1IMC': 'max',
                                'TotalL1IAC': 'max', 'MaxL1IAC': 'max', 'Avg/MaxL1IAC': 'max',
                                'TotalL1OMC': 'max', 'MaxL1OMC': 'max', 'Avg/MaxL1OMC': 'max',
                                'TotalL1OAC': 'max', 'MaxL1OAC': 'max', 'Avg/MaxL1OAC': 'max',
                                'TotalL2MC': 'max', 'MaxL2MC': 'max', 'Avg/MaxL2MC': 'max',
                                'TotalL2AC': 'max', 'MaxL2AC': 'max', 'Avg/MaxL2AC': 'max',
                                'TotalBREC': 'max', 'MaxBREC': 'max', 'Avg/MaxBREC': 'max',
                                'TotalBPFC': 'max', 'MaxBPFC': 'max', 'Avg/MaxBPFC': 'max',
                                'TotalVLXC': 'max', 'MaxVLXC': 'max', 'Avg/MaxVLXC': 'max',
                                'TotalFMAXC': 'max', 'MaxFMAXC': 'max', 'Avg/MaxFMAXC': 'max',

                                'ROITime': 'first', 'IPC': 'first', 'VIPC': 'first',
                                'VIRatio': 'first', 'VOPAvgLen': 'max', 'L1MissRatio': 'max',
                                'L1DMPKI': 'max', 'L2MissRatio': 'max', 'L2MPKI': 'max',
                                'L2MPKVI': 'max', 'VE/cycle': 'max', 'VECC/cycle': 'max',
                                'L1MCC/cycle': 'max', 'VE2/cycle': 'max', 'VAREC/cycle': 'max',
                                'VLDEC/cycle': 'max', 'PCCC/cycle': 'max', 'VLDCC/cycle': 'max',
                                'VLEC/cycle': 'max', 'VLCME/cycle': 'max', 'L1IMC/s': 'max',
                                'L1IAC/s': 'max', 'L1OMC/s': 'max', 'L1OAC/s': 'max',
                                'L2MC/s': 'max', 'L2AC/s': 'max', 'BREC/s': 'max',
                                'BPFC/s': 'max', 'VLXC/s': 'max', 'VLCMX/s': 'max', 'FMAXC/s': 'max',                                
                                'MFLOPS': 'first', 'VOPRatio': 'max', 'VOPAvgLen': 'max',
                                'VectorTime': 'max', 'L1MissTime': 'max', 'PortConflictTime': 'max', 'VLDLLCHitRatio': 'max'
                                }
                       

    fields = ['Benchmark',	'Algorithm', 'NumThreads', 'TaskSize',	'MatrixName', 'MatrixNumRows']                        
    spmv_pmc_stats_merged = spmv_pmc_stats.groupby(fields).aggregate(aggregation_functions).reset_index()
    output_filepath = '{0}/{1}_pmc_merged.csv'.format(root_folder, folder_name)
    spmv_pmc_stats_merged.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
                            na_rep='nan')
    # spmv_pmc_stats.loc[(spmv_pmc_stats[mandatory_fields[0]] == comb[0]) & (df[mandatory_fields[1]] == comb[1])]

def main(root_folder):
    table = []
    spmv_header = ['Benchmark', 'Algorithm', 'DateTime', 'Verification', 'NumThreads', 'TaskSize', 'RowOrderWindow', 'NumBlocks',  'MatrixName', 
                    'MatrixNumRows', 'MatrixNumCols', 'MatrixNNZ', 'MatrixNNZ/row',  'NumIter', 'MemAlloc', 'RefReadBW', 'RefWriteBW',
                    'ReadBW', 'WriteBW', 'SetupTime', 'PreProcTime', 'RefExeTime', 'RefGFLOPs', 'ExeTime', 'GFLOPs']

    output_files = sorted(search_files(root_folder, '*.out'))
    error_files = sorted(search_files(root_folder, '*.err'))
    ftrace_files = sorted(search_files(root_folder, 'ftrace_*'))
    if args.fix:
        for ef in error_files:
            fix_json_file(ef)
        exit(0)


    print('SPMV Perf: Parsing {0} files...'.format( len(output_files)))

    if args.with_sacct:
        parse_output_and_sacct(spmv_header, output_files)
        exit(0)

    if args.with_nvsmi:
        parse_output_and_nvsmi(spmv_header, output_files)
        exit(0)

    if args.with_current:
        parse_output_and_current_ve(spmv_header, output_files)
        exit(0)

    if args.pmc:
        parse_output_and_pmc_ve(spmv_header, output_files, error_files)
        exit(0)
    else:
        for of in output_files:

            try:
                basic_output = parse_json_output(of)
                if args.ftrace:
                    base_name = os.path.basename(of)
                    ftrace_mode = 'VECTOR-{0}'.format(re.search('VECTOR-(.*)\.out', base_name).group(1))
                    basic_output += [ftrace_mode]

                table.append(basic_output)
            except json.JSONDecodeError as jde:
                print('Could not parse this file: {0}'.format(of))
                pass

        if args.ftrace:
            spmv_header += ['FTMode']
        
        spmv_results = pd.DataFrame(table, columns=spmv_header)

    # print(table)
  
    #spmv_results = column_to_row_reference_results(spmv_results)
        
        folder_name = os.path.split(root_folder.strip('/'))[1]
        output_filepath = '{0}/{1}_results.csv'.format(root_folder, folder_name)
        print('Writting results to: {0}'.format(output_filepath))

        spmv_results.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
                            na_rep='nan')


    #####################################
    if args.ftrace:
        # Load file (spmv_results)

        print('SPMV Ftrace: Parsing {0} files...'.format(len(ftrace_files)))
        function_name = 'SPMV_Kernel'

        # Expand table
        ftrace_header_op = ['Freq', 'Time', 'TimePercent', 'AlgAvgRuntime', 'MOPS', 'MFLOPS', 'VOPRatio', 'VOPAvgLen',
                         'VecTime', 'L1Miss', 'PortConflict', 'VLDLLCHitRatio', 'FuncName']
        
        # ftrace_header_mem = ['Freq', 'Time', 'TimePercent', 'AlgAvgRuntime', 'MOPS', 'MFLOPS', 'VOPRatio', 'VOPAvgLen',
        #                     'VecTime', 'L1IMissRatio', 'L1DMissRatio', 'L2MissRatio', 'RBpF', 'RSTBpF', 'ACTVLDBpF', 'FLOPCount', 'FMAEle', 'FuncName']
                
        ftrace_header_mem = ['L1IMissRatio', 'L1DMissRatio', 'L2MissRatio', 'RBpF', 'RSTBpF', 'RLDBpF', 'ACTVLDBpF', 'FLOPCount', 'FMAEle']

        df_results_ftrace = spmv_results.reindex(columns=[*spmv_results.columns.tolist(), *ftrace_header_op, *ftrace_header_mem])

        for ft in ftrace_files:
            # Extract info from the file name: MTX name, num. threads, ftrace mode
            base_name = os.path.basename(ft)
            ftrace_mode = 'VECTOR-{0}'.format(re.search('VECTOR-(.*)\.txt', base_name).group(1))
            mtx_name = '{0}.mtx'.format(re.search('ftrace_(.*)_[0-9]_VECTOR-.*\.txt', base_name).group(1))
            nthreads = re.search('ftrace_.*_([0-9])_VECTOR-.*\.txt', base_name).group(1)
            header = ftrace_header_op if ftrace_mode == 'VECTOR-OP' else ftrace_header_mem
            # ftrace_text = get_ftrace_output(ft) // not needed anymore
            # LOAD file
            with open(ft, 'r') as file:
                ftrace_text = file.read()

                # Get matrix + num threads
                row = get_ftrace_stats_byfunc(ftrace_text, function_name, header, ftrace_mode)

                keys_dict = { 'MatrixName' : mtx_name, 'NumThreads' : nthreads, 'FTMode' : ftrace_mode }
                df_results_ftrace = insert_ftrace_stats(df_results_ftrace, row, header, keys_dict)         

        ftrace_results_filepath = '{0}/{1}_ftrace_results.csv'.format(root_folder, folder_name)
        print('Writting results to: {0}'.format(ftrace_results_filepath))
        df_results_ftrace.to_csv(ftrace_results_filepath, sep=',', index=False, header=True, float_format='%.5f',
                        na_rep='nan')


def search_files(directory='.', pattern=''):
    files = []

    pattern = pattern.lower()
    for dirpath, dirnames, filenames in os.walk(directory):
        for fn in filenames:
            if pattern and fnmatch.fnmatch(fn.lower(), pattern):
                files.append(os.path.join(dirpath, fn))

    return files

if __name__ == "__main__":

    # Parse Arguments
    parser = argparse.ArgumentParser(description='Parse SPMV EPI outputs and dump them to a CSV')
    parser.add_argument('RootFolder', type=str, nargs=1,
                        help='Path to the rootdir with DGEMM EPI benchmark ouputs.')

    parser.add_argument('--ftrace', action='store_true')
    parser.add_argument('--proginf', action='store_true')
    parser.add_argument('--pmc', action='store_true')
    parser.add_argument('--fix', action='store_true')
    parser.add_argument('--with-sacct', action='store_true')
    parser.add_argument('--with-nvsmi', action='store_true')
    parser.add_argument('--with-current', action='store_true')

    # parser.add_argument('RootFolder', type=str, nargs=1,
    #                     help='Path to the rootdir with DGEMM EPI benchmark ouputs.')
    args = parser.parse_args()
    root_folder = args.RootFolder[0]
    print(root_folder)

    main(root_folder)
