#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse
import os
import sys
import seaborn

def customize_plot(ax):
    ax.set_title(title)
    ax.set_yticklabels(ax.get_yticklabels(), fontsize=10, rotation=0)


def plot_heatmap(df, x_col, y_col, values_col, output_filepath, title, eval_axis=False, cmap='Purples'):
    '''
        Drop duplicate data
        Pivot data into a seaborn.Heatmap() compatible format
    '''
    df.drop_duplicates(subset=[x_col, y_col], inplace=True)
    my_data = df.pivot(index=x_col, columns=y_col, values=values_col)   

    '''
        First we create the Figure and Axes.
        Then we create a Heatmap plot with seaborn
    '''
    fig, ax = plt.subplots(figsize=(10,4)) 
    ax = seaborn.heatmap(my_data, cmap=cmap, annot=True, fmt='.2f', ax=ax, linewidths=.01)
    customize_plot(ax)

    # Save to disk
    plt.savefig(output_filepath, bbox_inches='tight', face_color="white", transparent="true")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates a heatmap plot')
    parser.add_argument('path_to_csv', nargs=1, type=str, help='File with input data in CSV format')
    parser.add_argument('--x', nargs=1, type=str, help='Name of the column to use in the X axis.')
    parser.add_argument('--y', nargs=1, type=str, help='Name of the column to use in the Y axis.')
    parser.add_argument('--cmap', nargs=1, type=str, help='ColorMap')
    parser.add_argument('--values', nargs=1, type=str, help='Name of the column to use as Values.')
    parser.add_argument('--title', nargs=1, type=str, help='')
    parser.add_argument('--filterIn', type=str, nargs=2, help='filter the dataframe')

    args = parser.parse_args()

    f = args.path_to_csv[0]
    x = args.x[0]
    y = args.y[0]
    cmap = args.cmap[0]
    title = args.title[0]
    values = args.values[0]
    output_filepath = '{0}.pdf'.format(os.path.splitext(f)[0])
    output_filepath = '{0}.pdf'.format(os.path.split(f)[1][:-4])
    print('Plot will be saved at: {0}'.format(output_filepath))

    input_dataframe = pd.read_csv(f, header=0, comment='#', dtype=None, skipinitialspace=True, engine='python')
    
    # Filter dataframe
    if args.filterIn:
        filter_column1  = args.filterIn[0]
        filter_value1   = args.filterIn[1]
        input_dataframe = input_dataframe[input_dataframe[filter_column1] == int(filter_value1)]
        print('Column -{0}- filtered in by \'{1}\''.format(filter_column1, filter_value1))

    plot_heatmap(input_dataframe, x, y, values, output_filepath, title, True, cmap)
