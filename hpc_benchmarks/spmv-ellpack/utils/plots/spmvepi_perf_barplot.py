#!/usr/bin/env python3
# Author. Constantino Gomez @ Barcelona Supercomputing Center
import os
import argparse
import math
import itertools
import subprocess
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter, AutoMinorLocator
import cgplots.cgbarplots as cgbp
import pandas as pd


def perf_plot(df, args):

    input_fn = os.path.splitext(args.fileName[0])[0]
    suffix = '-' + args.suffix[0] if args.suffix else ''
    output_fn = f'{input_fn}{suffix}'
    kw = {
        'top_title'      : '',
        'xaxis_label'    : '',
        'yaxis_label'    :  args.values[1] if args.values else 'GFLOPs',
        'values_col_tag' :  args.values[0] if args.values else 'GFLOPs',
        # 'valuesstd_col_tag' : '',
        'values_label'   :  args.values[0] if args.values else 'GFLOPs',
        'level1_col_tag' :  args.l1[0] if args.l1 else 'Algorithm',
        'level2_col_tag' :  args.l2[0] if args.l2 else 'MatrixName',
        'level3_col_tag' :  args.l3[0] if args.l3 else 'Benchmark',
        'publish_format' : args.publish,
        'legend_label'   : args.legend_title[0],
        'label_bars'     : args.labels,
        'errorbars'      : None,
        'values_label_col_tag' : args.values[0] if args.values else 'GFLOPs',
        'color_palette'  : args.palette[0] if args.palette else 'plasma',
        'output_filename': f'{output_fn}.pdf',
        'logscale'       : False
    }

    orderbars = True
    if orderbars:
        level1_order = args.l1order

        df[kw['level1_col_tag']] = pd.Categorical(df[kw['level1_col_tag']], level1_order )
        # df.sort_values(by=[kw['level1_col_tag'], 'MatrixNNZ' ] , inplace=True)
        df.sort_values(by=['MatrixNNZ', kw['level1_col_tag'] ], inplace=True)
        df['MatrixName'].replace(regex=True,inplace=True,to_replace=r'.mtx',value=r'')
    
    cgbp.three_level_barplot(df, args, **kw)


def get_args():
    parser = argparse.ArgumentParser(description='Generates a Bar plot with groups.')

    parser.add_argument('fileName', metavar='FileName', type=str, nargs=1,
                        help='Path to the CSV file with the input data.')
    parser.add_argument('--burst-id',  type=str, nargs=1,
                        help='burst to filter the data')
    parser.add_argument('-p', '--publish', action='store_true',
                        help='b/w paper publish format')
    parser.add_argument('-l', '--labels', action='store_true',
                        help='shows labels values')
    parser.add_argument('--errorbars', '-eb', action='store_true', dest = 'errorbars',
                        help='Enables error bars')
    parser.add_argument('--filterIn', type=str, nargs='*', help='filter the dataframe')
    parser.add_argument('--filterOut', type=str, nargs='*', help='filter the dataframe')
    parser.add_argument('--legend-title', type=str, nargs=1, help='title for the legend')

    parser.add_argument('--l1',  type=str, nargs=1, help='')
    parser.add_argument('--l2',  type=str, nargs=1, help='')
    parser.add_argument('--l3',  type=str, nargs=1, help='')
    parser.add_argument('--palette',  type=str, nargs=1, help='')

    parser.add_argument('--values',  type=str, nargs='*', help='')

    parser.add_argument('--suffix',  type=str, nargs=1, help='')
    parser.add_argument('--l1order',  type=str, nargs='*', help='')
    parser.add_argument('--ylim',  type=float, nargs=1, help='')
    parser.add_argument('--colors',  type=str, nargs='*', help='')
    parser.add_argument('--l2lblsize',  type=int, nargs=1, help='')

    return parser.parse_args()

def main(args):
    f = args.fileName[0]
    # Loading Data: Read CSV into pandas DataFrame
    input_dataframe = pd.read_csv(f, header=0, comment='#', dtype=None, skipinitialspace=True,
                      engine='python')

    if args.filterIn:
        filter_column1  = args.filterIn[0]
        # filter_value1   = args.filterIn[1]
        filter_values   = args.filterIn[1:]
        # input_dataframe = input_dataframe[input_dataframe[filter_column1] == filter_value1]

        input_dataframe = input_dataframe[input_dataframe[filter_column1].isin(filter_values)]
        print(input_dataframe)

        print(f'Column -{filter_column1}- contains only: {filter_values}')
    
    if args.filterOut:
        filter_column2  = args.filterOut[0]
        filter_value2   = args.filterOut[1:]
        print('Column -{0}- filtered out by \'{1}\''.format(filter_column2, filter_value2))
        # input_dataframe = input_dataframe[input_dataframe[filter_column2] != int(filter_value2)]
        input_dataframe = input_dataframe[~input_dataframe[filter_column2].isin(filter_value2)]

    input_dataframe.replace(to_replace='\_', value="-", inplace=True, regex=True)
    perf_plot(input_dataframe, args)
    # other_plot(input_dataframe, args)

    # setuptime_plot(input_dataframe, args)

if __name__ == "__main__":
    # Parse Arguments
    args = get_args()
    main(args)