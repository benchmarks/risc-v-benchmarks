#!/usr/bin/env python3
# import os
import pandas
import argparse
import numpy
import itertools
import json

root_folder = '../../../results/testmerge.csv'

def create_missing_rows(df, mandatory_fields, value_col):

    # CREATE MISSING ROWS with NAN values

    # Get all the mandatory fields possible combinations (think of it as the primary key)
    # For example we will ensure that for each algorithm type and matrix we have a row
    unique_values_array = []
    for key in mandatory_fields:
        unique_values_array.append(pandas.unique(df[key].values))

    arg_combinations = list(itertools.product(*unique_values_array))

    # print(arg_combinations)
    failed = 0
    for comb in arg_combinations:
        if df.loc[(df[mandatory_fields[0]] == comb[0]) & (df[mandatory_fields[1]] == comb[1])].size == 0:
            num_rows = pandas.unique(df.loc[ df[mandatory_fields[1]] == comb[1], 'MatrixNumRows'])
            num_nnz = pandas.unique(df.loc[ df[mandatory_fields[1]] == comb[1], 'MatrixNNZ'])

            print('Row {0}:{1}, {2}:{3}'.format(mandatory_fields[0], comb[0], mandatory_fields[1], comb[1]))
            # https://thispointer.com/python-pandas-how-to-add-rows-in-a-dataframe-using-dataframe-append-loc-iloc/
            df = df.append({"Benchmark" : "SPMV EPI", mandatory_fields[0] : comb[0], mandatory_fields[1] : comb[1],
                             'MatrixNumRows' : int(num_rows), 'MatrixNNZ' : int(num_nnz)},
                             ignore_index=True)
            failed += 1

    # TODO DELETE the ones not present.

    return df

def merge_csv(args, csv_filepaths, output_filepath):
    fill_missing_rows = False
    average_by = args.avg_col[0] if args.avg_col else 'Algorithm'
    
    dataframes = []

    # Load and Merge all dataframes into one.
    for csv in csv_filepaths:
        tmp_df = pandas.read_csv(csv, header=0, comment='#', dtype=None, skipinitialspace=True,
                                 engine='python')
        dataframes.append(tmp_df)

    merged_df = pandas.concat(dataframes)

    mandatory_fields = args.indexby if args.indexby else ['Algorithm', 'MatrixName']
    value_cols = args.norm_cols if args.norm_cols else 'GFLOPs'
    print(args.agg_function[0])
    aggregation_functions = json.loads(args.agg_function[0])  

    # aggregation_functions = dictionary if dictionary { 'GFLOPs': 'max', 'EnergyToSolution': 'min' }

    # IF the merged datasets have some rows missing to be coherent, this will create them with nan values.
    # This way bars wont appear on the plots but they can be printed anyway.
    merged_df = create_missing_rows(merged_df, mandatory_fields, value_cols)
    merged_df = merged_df.groupby(mandatory_fields).aggregate(aggregation_functions).reset_index()

    # merged_df = merged_df.groupby(mandatory_fields).max().reset_index()
    print(merged_df)
 
    # add average
    algorithms = pandas.unique(merged_df[average_by])

    for alg in algorithms:
        avg = merged_df.loc[merged_df[average_by] == alg, value_cols].mean(axis=0)
        # print(avg)

        append_dict = {'Benchmark': 'SPMV EPI', 'MatrixName' : 'AVERAGE', average_by : alg }
        for c in value_cols:
            append_dict[c] = avg[c]

        merged_df = merged_df.append( append_dict , ignore_index=True)

    merged_df.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
        na_rep='nan')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Loads multiple CSV with equal header and merges them into one.')
    parser.add_argument('--input', nargs='*')
    parser.add_argument('--output', nargs=1)
    parser.add_argument('--indexby', nargs='*')
    parser.add_argument('--norm-cols', nargs='*')
    parser.add_argument('--avg-col', nargs=1)
    parser.add_argument('--agg-function', nargs=1)

    #description='Path to CSVs: CSV(1), ... CSV(n). '

    args = parser.parse_args()
    csv_filepaths = args.input
    output_filepath = args.output[0]

    merge_csv(args, csv_filepaths, output_filepath)
