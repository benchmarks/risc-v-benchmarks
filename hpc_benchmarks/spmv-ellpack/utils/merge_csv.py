#!/usr/bin/env python3
# import os
import pandas
import argparse
import numpy
import itertools

root_folder = '../../../results/testmerge.csv'

def merge_csv(csv_filepaths, output_filepath):

  

    dataframes = []

    for csv in csv_filepaths:
        tmp_df = pandas.read_csv(csv, header=0, comment='#', dtype=None, skipinitialspace=True,
            engine='python')
        dataframes.append(tmp_df)
    # merge all dataframes into one.
    merged_df = pandas.concat(dataframes)


    # CREATE MISSING ROWS with NAN values

    filter_keys=['Version', 'BlockSize', 'MatrixSize', 'NumThreads']
    value_col = 'GFLOPs'

    unique_values_array = []
    for key in filter_keys:
          unique_values_array.append(pandas.unique(merged_df[key].values))

        

    arg_combinations = list(itertools.product(*unique_values_array))

    # print(arg_combinations)

    failed =[]
    for comb in arg_combinations:
        print(merged_df[merged_df[filter_keys] == comb].as_matrix())
        try:
            print(merged_df[merged_df[filter_keys] == comb.as_matrix()])
        except:
            failed.append(comb)


    print("...........................")
    # print(failed)
    merged_df.to_csv(output_filepath, sep=',', index=False, header=True, float_format='%.5f',
        na_rep='nan')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Loads multiple CSV with equal header and merges them into one.')
    parser.add_argument('CSV', nargs='*')
    #description='Path to CSVs: CSV(1), ... CSV(n). '

    args = parser.parse_args()

    csv_filepaths = args.CSV
    output_filepath = root_folder

    merge_csv(csv_filepaths, output_filepath)
