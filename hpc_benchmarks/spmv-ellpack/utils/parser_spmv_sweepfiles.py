#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
import math
import argparse
import json
import fnmatch
import subprocess
import re
import unittest
from datetime import datetime
import io
import itertools

def create_missing_rows(df, mandatory_fields):
    # CREATE MISSING ROWS with NAN values
  
    unique_values_array = []
    for key in mandatory_fields:
        unique_values_array.append(pd.unique(df[key].values))

    arg_combinations = list(itertools.product(*unique_values_array))

    # print(arg_combinations)
    failed = 0
    for comb in arg_combinations:
        if df.loc[(df[mandatory_fields[0]] == comb[0]) & (df[mandatory_fields[1]] == comb[1])].size == 0:
            num_nnz = pd.unique(df.loc[ df[mandatory_fields[1]] == comb[1], 'MatrixNNZ'])

            print('Row {0}:{1}, {2}:{3}'.format(mandatory_fields[0], comb[0], mandatory_fields[1], comb[1]))
            # https://thispointer.com/python-pandas-how-to-add-rows-in-a-dataframe-using-dataframe-append-loc-iloc/
            df = df.append({"Benchmark" : "SPMV EPI", mandatory_fields[0] : comb[0], mandatory_fields[1] : comb[1],
                         'MatrixNNZ' : int(num_nnz)},
                             ignore_index=True)
            failed += 1

    # TODO DELETE the ones not present.

    return df


def load_sweep_file(filepath):
    text = ""
    with open(filepath, "r+") as f:
        text = f.read()  # read everything in the file

        idx = text.find("Benchmark")
        if idx > 0:
            text = text[idx:]

        idx = text.find("Warning")
        if idx > 0:
            text = text[:idx]

    textIO = io.StringIO(text)
    df = pd.read_csv(textIO, header=0, comment="#", dtype=None,skipinitialspace=True, engine="python")
    return df


def main(root_folder):

    error_files = sorted(search_files(root_folder, "*.err"))
    alldf = []
    bestdf = []  # pd.DataFrame().reindex_like(aux)

    for ef in error_files:
        #try:
            print(f'Processing: {ef}')
            sweep_df = load_sweep_file(ef)

            windows = pd.unique(sweep_df['SigmaWindow'])
        
            for w in windows:
                filtered = sweep_df.loc[sweep_df['SigmaWindow'] == w]
                # print(filtered)
                best_row_idx = filtered["GFLOPs"].idxmax()
                # print(best_row_idx)
                best_row = sweep_df.iloc[best_row_idx]
                bestdf.append(best_row)

            # best_row_idx = sweep_df["GFLOPs"].idxmax()
            # best_row = sweep_df.iloc[best_row_idx]
            # print(best_row)
            # bestdf.append(best_row)
            alldf.append(sweep_df)

        #except Exception:
	 
        #    print(f'Error processing: {ef}')
        #    continue

    df_mergeall = pd.concat(alldf)
    df_mergebest = pd.concat(bestdf, axis=1).T
    # print(df_mergeall)
    # print(df_mergebest)

    # Replace _omp
    df_mergeall['Algorithm'].replace(regex=True,inplace=True,to_replace=r'_omp',value=r'')
    df_mergebest['Algorithm'].replace(regex=True,inplace=True,to_replace=r'_omp',value=r'')

    # Missing rows
    df_mergebest = create_missing_rows(df_mergebest, ['Algorithm', 'MatrixName'])

    # Add average
    value_cols = ['GFLOPs']
    average_by = 'Algorithm'
    algorithms = pd.unique(df_mergebest[average_by])
    for alg in algorithms:
        avg = df_mergebest.loc[df_mergebest[average_by] == alg, value_cols].mean(axis=0)
        append_dict = {'Benchmark': 'SPMV EPI', 'MatrixName' : 'AVERAGE', average_by : alg }
        for c in value_cols:
            append_dict[c] = avg[c]

        df_mergebest = df_mergebest.append(append_dict , ignore_index=True)

    # Write to disk
    folder_name = os.path.split(root_folder.strip("/"))[1]
    output_filepath = f'{root_folder}/{folder_name}'
    print("Writting results to: {0}".format(output_filepath))

    df_mergeall.to_csv(f'{output_filepath}_all.csv', sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')
    df_mergebest.to_csv(f'{output_filepath}_best.csv', sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')


    #####################################

    grouper = df_mergebest.groupby(['SigmaWindow'])
    print(grouper)
             
    # print(grouper.groups[512].values['MatrixName'])
    df = []
    for k, v in grouper:
        if (len(df)==0):
            df.append(pd.Series(v['MatrixName'].tolist(), name='MatrixName'))


        series = pd.Series(v['PreProcTime'].tolist(), name=k)
        # somt = v['PreProcTime', 'MatrixName']


        df.append(series)
     

    df = pd.concat(df, axis=1)
    df.to_csv(f'{output_filepath}_transpose.csv', sep=',', index=False, header=True, float_format='%.5f', na_rep='nan')


def search_files(directory=".", pattern=""):
    files = []

    pattern = pattern.lower()
    for dirpath, dirnames, filenames in os.walk(directory):
        for fn in filenames:
            if pattern and fnmatch.fnmatch(fn.lower(), pattern):
                files.append(os.path.join(dirpath, fn))

    return files


if __name__ == "__main__":

    # Parse Arguments
    parser = argparse.ArgumentParser(
        description="Parse SPMV EPI outputs and dump them to a CSV")
    parser.add_argument(
        "RootFolder",
        type=str,
        nargs=1,
        help="Path to the rootdir with DGEMM EPI benchmark ouputs.",
    )

    parser.add_argument("--ftrace", action="store_true")
    parser.add_argument("--proginf", action="store_true")
    parser.add_argument("--pmc", action="store_true")
    parser.add_argument("--fix", action="store_true")
    parser.add_argument("--with-sacct", action="store_true")
    parser.add_argument("--with-nvsmi", action="store_true")
    parser.add_argument("--with-current", action="store_true")

    # parser.add_argument('RootFolder', type=str, nargs=1,
    #                     help='Path to the rootdir with DGEMM EPI benchmark ouputs.')
    args = parser.parse_args()
    root_folder = args.RootFolder[0]
    print(root_folder)

    main(root_folder)
