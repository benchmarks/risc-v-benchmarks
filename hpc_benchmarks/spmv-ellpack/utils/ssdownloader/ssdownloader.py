#!/usr/bin/env python3
import pandas
import re
import argparse
import numpy
import itertools
import requests
from bs4 import BeautifulSoup
import os
import string
import paramiko

listing_filepath = 'ss_listing.txt'
download_folder = 'mtx_inputs'
tmp_folder = '/tmp/'

def save_matrix_info(mtx_dict):
    mtx_info_json = " {{\n" \
        "   \"Name\" : \"{Name}\",\n" \
        "   \"Group\" : \"{Group}\",\n" \
        "   \"DownloadURL\" : \"{DownloadURL}\",\n" \
        "   \"MatrixID\" : \"{MatrixID}\",\n" \
        "   \"NumRows\" : \"{NumRows}\",\n" \
        "   \"NumCols\" : \"{NumCols}\",\n" \
        "   \"Nonzeros\" : \"{Nonzeros}\",\n" \
        "   \"PatternEntries\" : \"{PatternEntries}\",\n" \
        "   \"Kind\" : \"{Kind}\",\n" \
        "   \"Symmetric\" : \"{Symmetric}\",\n" \
        "   \"Date\" : \"{Date}\",\n" \
        "   \"Author\" : \"{Author}\",\n" \
        "   \"Editor\" : \"{Editor}\",\n" \
        "   \"StructuralRank\" : \"{StructuralRank}\",\n" \
        "   \"PatternSymmetry\" : \"{PatternSymmetry}\",\n" \
        "   \"Type\" : \"{Type}\"\n" \
        " }}" \

    mtx_info_json = mtx_info_json.format(**mtx_dict)
    mtx_info_json_filepath = os.path.join(matrix_folderpath, '{0}_info.json'.format(mtx_dict['Name']))

    with open(mtx_info_json_filepath, 'w') as wf:
        wf.write(mtx_info_json)

def fetch_and_untar(url, matrix_folder):
    targz_filepath = os.path.join(tmp_folder, 'package.tar.gz')
    download_cmd = 'wget {0} -O {1}'.format(url, targz_filepath)
    copy_cmd = 'copy {0} {1}'.format(targz_filepath, matrix_folder)
    os.system(download_cmd)
    os.system(copy_cmd)
    
    # Connect remotely to unrar
    untar_cmd = 'tar --extract -f {0} --directory {1} --strip 1'.format(targz_filepath, matrix_folder)
    full_cmd = 'cd EPI/repos/vectorized_codes/utils/ssdownloader && {0}'.format(untar_cmd)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.connect('mn3.bsc.es', username='bsc18880')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(full_cmd, get_pty=True)

    os.remove(targz_filepath)

def ss_scrap_matrix(matrix_url):
    main_table_dict = {}
    strip_nonalpha_regex = re.compile('[\W_]+')

    print('Reading matrix: {0}'.format(matrix_url))
    website = requests.get(matrix_url).text
    soup = BeautifulSoup(website, 'html5lib')
    html_tables = soup.find_all('table', {'class': 'table'})

    for table in html_tables:
        for row in table.find_all('tr'):
            key = strip_nonalpha_regex.sub('', row.th.get_text())
            if key == 'Download':
                main_table_dict['DownloadURL'] = row.td.find_all('a')[2].get('href')
            if row.td:
                value = strip_nonalpha_regex.sub('', row.td.get_text())
                main_table_dict[key] = value

    # Remove noise
    main_table_dict['Notes'] = ''

    return main_table_dict


def print_matrix_list(ss_listings):
    with open(ss_listings) as sf:
        print(sf.read())


def download_matrix(matrix_name):
    regex = r'(?:(?P<group>[\S].*)\/(?P<name>[\S].*))'
    matrix_folderpath = os.path.join(download_folder, matrix_name)
    mtx_dict = {}
    mtx_info_json = ''
    not_found = []

    with open(listing_filepath) as f:
        filetext = f.read()
        # TODO CHECK if website can be downloaded.

        if matrix_name in filetext:

            if os.path.exists(matrix_folderpath):
                print('Scraping {0} website...'.format(matrix_name))
                mtx_dict = ss_scrap_matrix(
                    'https://sparse.tamu.edu/{0}'.format(matrix_name))
            else:
                print('Creating folder {0}...'.format(matrix_folderpath))
                os.makedirs(matrix_folderpath)
                print('Scraping {0} info...'.format(matrix_name))
                mtx_dict = ss_scrap_matrix(
                    'https://sparse.tamu.edu/{0}'.format(matrix_name))

            if not os.path.exists(os.path.join(matrix_folderpath, '{0}.mtx'.format(matrix_name.split('/')[1]))):
                print('Downloading {0} .mtx file ...'.format(matrix_name))
                fetch_and_untar(mtx_dict['DownloadURL'], matrix_folderpath)
            else:
                print('Matrix {0}.mtx found in folder! It wont be downloaded again.'.format(matrix_name))
        else:
            print('Could not find matrix ({0}) in listings file'.format(matrix_name))
            not_found.append(matrix_name)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Sparse Matrix Downloader. A tool that scraps matrixes and metadata from https://sparse.tamu.edu/ Matrix Collection')
    parser.add_argument('--all', action='store_true')
    parser.add_argument('--list', action='store_true')
    parser.add_argument('--name', nargs=1)

    # --all
    # --url [url] | group[/matrix]  |
    # --name
    # --list names by group (check if exists)

    args = parser.parse_args()

    if (args.list):
        print_matrix_list(listing_filepath)

    if (args.all):
        download_all()

    if (args.name):
        download_matrix(args.name[0])
