#!/bin/bash

echo -n "`basename $1` " #version
echo -n "`basename $2` " #size
echo "$3" | grep Time | awk '{ print $4,$7 }' #time, MFLOP/s
