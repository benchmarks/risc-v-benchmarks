# GEMM

## Compile

```bash
make <version-name>
```

**Note:** The rvv0.7 version only works for `double type` variables.

## Run

```bash
./bin/<executable-version-name> <matrix-size>
```

**Note:** For the moment this benchmark only works for square matrices.

Exampe:

```bash
./bin/gemm-scalar 256
```

## Available Versions

| Target      | Versions                                            |
| ----        | ----                                                |
| base        | scalar, autovec                                     |
| omp         | simd                                                |
| cblas       | _\<default\>_                                       |
| rvv (0.7)   | _\<default\>_                                       |
| rvv (1.0)   |                                                     |
| bmetal      |                                                     |


Some versions could be non-compatible with the compiler and selected options as
they are configured in the setup files (see the root README.md file). In these
cases, either the build process will not generate the binary, or the compilation
process will fail. Check your compiler manual and/or the setup files accordingly.

