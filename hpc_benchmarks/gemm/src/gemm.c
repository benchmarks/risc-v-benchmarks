#include "common.h"
void matmul(int M, int K, int N, ELEM_T (* restrict c)[N], ELEM_T (* restrict a)[K], ELEM_T (* restrict b)[N]) {
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      for (int k = 0; k < K; k++) {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}
