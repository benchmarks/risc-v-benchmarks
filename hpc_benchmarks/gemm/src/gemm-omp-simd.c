#include "common.h"
void matmul(const int M, const int K, const int N, ELEM_T (* restrict c)[N], const ELEM_T (* restrict a)[K], const ELEM_T (* restrict  b)[N]) {
  for (int i = 0; i < M; i++) {
    for (int k = 0; k < K; k++) {
      #pragma omp simd
      for (int j = 0; j < N; j++) {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}
