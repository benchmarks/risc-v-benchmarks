#include <stdlib.h>
#include <stdio.h>
#include "elapsed_time.h"
#include "common.h"

#ifdef USE_FLOAT
#define compare_array(dy_opt, dy_ref, n) compare_array_float(dy_opt, dy_ref, n)
#else
#define compare_array(dy_opt, dy_ref, n) compare_array_double(dy_opt, dy_ref, n)
#endif

extern void matmul(int M, int K, int N, ELEM_T (* restrict c)[N],
                     ELEM_T (* restrict a)[K], ELEM_T (* restrict b)[N]);

static void matmul_reference(int M, int K, int N, ELEM_T (*c)[N], ELEM_T (*a)[K], ELEM_T (*b)[N]) {
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      for (int k = 0; k < K; k++) {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

typedef enum {
  INIT_ALL_ONES,
  INIT_IOTA,
} init_mode_t;

static void init_matrix(int m, int n, ELEM_T (*a)[n], init_mode_t init_mode) {
  switch (init_mode) {
  case INIT_ALL_ONES:
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        a[i][j] = 1.0;
      }
    }
    break;
  case INIT_IOTA: {
    ELEM_T v = 1.0;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        a[i][j] = v;
        v += 1.0;
      }
    }
    break;
  }
  }
}

static void zero_matrix(int m, int n, ELEM_T (*a)[n]) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      a[i][j] = 0.0;
    }
  }
}

int main(int argc, char *argv[])
{
  if(argc != 2){
    printf("USAGE: ./dgemm size\n");
    exit(1);
  }
  printf("Size of ELEM_T: %zu (bytes)\n", sizeof(ELEM_T));

  int m;
  int k;
  int n = m = k = atol(argv[1]);
  long long start,end;

  void *char_p;
  void *orig_ref;
  void *orig_c;
  void *orig_a;
  void *orig_b;

  ELEM_T (*ref)[n] __attribute__ ((aligned (128)));
  ELEM_T (*c)[n]   __attribute__ ((aligned (128)));
  ELEM_T (*a)[k]   __attribute__ ((aligned (128)));
  ELEM_T (*b)[n]   __attribute__ ((aligned (128)));

  orig_ref = malloc(sizeof(*ref) * m + 256);
  orig_c   = malloc(sizeof(*c) * m   + 256);
  orig_a   = malloc(sizeof(*a) * m   + 256);
  orig_b   = malloc(sizeof(*b) * k   + 256);
  ref = orig_ref;

  char_p = orig_c; 
  while  ((unsigned long)char_p % 64 != 0) char_p++; 
  c = char_p;
  char_p = orig_a; 
  while  ((unsigned long)char_p % 64 != 0) char_p++;
  a = char_p;
  char_p = orig_b; 
  while  ((unsigned long)char_p % 64 != 0) char_p++;
  b = char_p;

  zero_matrix(m, n, c);
  zero_matrix(m, n, ref);

  init_matrix(m, k, a, INIT_IOTA);
  init_matrix(k, n, b, INIT_IOTA);

  //NAIVE
  start = get_time();
  matmul_reference(m, k, n, ref, a, b);
  end = get_time();

  double kflop = (double)((2*(n*n*n))/1000);
  printf("kflops: %f\n", kflop);
  float elapsedTime = (double)elapsed_time(start, end);

  printf("Naive_ref  - Time: %f s, MFLOPS/s: %f\n", elapsedTime, kflop/elapsedTime/1000);

  start = get_time();
  matmul(m, k, n, c, a, b);
  end = get_time();

  elapsedTime = (double)elapsed_time(start, end);
  printf("Benchmark  - Time: %f s, MFLOPS/s: %f\n", elapsedTime, kflop/elapsedTime/1000);

  for(int i = 0; i < m; i++){
    compare_array(ref[i], c[i], m);
  }

  free(orig_c);

  free(orig_b);
  free(orig_a);
  free(orig_ref);

  return 0;
}
