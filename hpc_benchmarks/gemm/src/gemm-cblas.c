#include "blis.h"
#include "common.h"
void matmul(int M, int K, int N, ELEM_T (* restrict c)[N], ELEM_T (* restrict a)[K], ELEM_T (* restrict b)[N])
{
  ELEM_T alpha=1.0;
  ELEM_T beta=1.0;
#ifdef USE_FLOAT
  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, M, N, K, alpha, a, K, b, N, beta, c, N);
#else
  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, M, N, K, alpha, a, K, b, N, beta, c, N);
#endif
}
