#include <assert.h>
#include "common.h"

#if __riscv_vector_version==800
#define BROADCAST_f64 __builtin_epi_vfmv_v_f_1xf64
#else
#define BROADCAST_f64 __builtin_epi_vbroadcast_1xf64
#endif

#define FMA( vc, vb, sa, gvl ) do { \
  __epi_1xf64 vta  = BROADCAST_f64(sa, gvl); \
  vc  = __builtin_epi_vfmacc_1xf64(vc, vta, vb, gvl); \
} while(0)

void matmul(int M, int K, int N, ELEM_T (* restrict c)[N], ELEM_T (* restrict a)[K], ELEM_T (* restrict b)[N])
{

#ifdef USE_FLOAT
   sprintf(stderr, "Error: Float version with intrinsics has not been implemented\n");
   return;
#endif
  //Vectorial parameters
  int bi = 24;
  int bj = 256;
  int bk = 1;

  unsigned long MAXVL = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
  bk=262144/(8*MAXVL);          //estimate k to fit in cache

  unsigned long int ii, jj;
  unsigned long gvl ; 

  for (jj = 0; jj < N; ) {
     __epi_1xf64 vc0, vc1, vc2, vc3, vc4, vc5, vc6, vc7, vc8, vc9, vc10, vc11, vc12, vc13, vc14, vc15;
     __epi_1xf64 vc16, vc17, vc18, vc19, vc20, vc21, vc22, vc23;
     __epi_1xf64 vb0;

     gvl = __builtin_epi_vsetvl(N-jj, __epi_e64, __epi_m1); // system selected columm block size (C & B)

     for (int KK = 0; KK < K; KK += bk) {
        int upper_kk = (KK + bk) < K ? KK+bk: K; 
        for (ii = 0; ii < M-(bi-1); ii += bi) { //unroll rows (no possibility of indirection on register addressing)
           vc0 = __builtin_epi_vload_nt_1xf64(&c[ii][jj], gvl);
           vc1 = __builtin_epi_vload_nt_1xf64(&c[ii+1][jj], gvl);
           vc2 = __builtin_epi_vload_nt_1xf64(&c[ii+2][jj], gvl);
           vc3 = __builtin_epi_vload_nt_1xf64(&c[ii+3][jj], gvl);
           vc4 = __builtin_epi_vload_nt_1xf64(&c[ii+4][jj], gvl);
           vc5 = __builtin_epi_vload_nt_1xf64(&c[ii+5][jj], gvl);
           vc6 = __builtin_epi_vload_nt_1xf64(&c[ii+6][jj], gvl);
           vc7 = __builtin_epi_vload_nt_1xf64(&c[ii+7][jj], gvl);
           vc8 = __builtin_epi_vload_nt_1xf64(&c[ii+8][jj], gvl);
           vc9 = __builtin_epi_vload_nt_1xf64(&c[ii+9][jj], gvl);
           vc10 = __builtin_epi_vload_nt_1xf64(&c[ii+10][jj], gvl);
           vc11 = __builtin_epi_vload_nt_1xf64(&c[ii+11][jj], gvl);
           vc12 = __builtin_epi_vload_nt_1xf64(&c[ii+12][jj], gvl);
           vc13 = __builtin_epi_vload_nt_1xf64(&c[ii+13][jj], gvl);
           vc14 = __builtin_epi_vload_nt_1xf64(&c[ii+14][jj], gvl);
           vc15 = __builtin_epi_vload_nt_1xf64(&c[ii+15][jj], gvl);
           vc16 = __builtin_epi_vload_nt_1xf64(&c[ii+16][jj], gvl);
           vc17 = __builtin_epi_vload_nt_1xf64(&c[ii+17][jj], gvl);
           vc18 = __builtin_epi_vload_nt_1xf64(&c[ii+18][jj], gvl);
           vc19 = __builtin_epi_vload_nt_1xf64(&c[ii+19][jj], gvl);
           vc20 = __builtin_epi_vload_nt_1xf64(&c[ii+20][jj], gvl);
           vc21 = __builtin_epi_vload_nt_1xf64(&c[ii+21][jj], gvl);
           vc22 = __builtin_epi_vload_nt_1xf64(&c[ii+22][jj], gvl);
           vc23 = __builtin_epi_vload_nt_1xf64(&c[ii+23][jj], gvl);

           for (int kk = KK; kk < upper_kk; kk ++) {
              vb0 = __builtin_epi_vload_1xf64(&b[kk][jj], gvl);
              {
              FMA( vc0,  vb0,   a[ii][kk], gvl );
              FMA( vc1,  vb0,  a[ii+1][kk], gvl );
              FMA( vc2,  vb0,  a[ii+2][kk], gvl );
              FMA( vc3,  vb0,  a[ii+3][kk], gvl );
              FMA( vc4,  vb0,  a[ii+4][kk], gvl );
              FMA( vc5,  vb0,  a[ii+5][kk], gvl );
              FMA( vc6,  vb0,  a[ii+6][kk], gvl );
              FMA( vc7,  vb0,  a[ii+7][kk], gvl );
              FMA( vc8,  vb0,  a[ii+8][kk], gvl );
              FMA( vc9,  vb0,  a[ii+9][kk], gvl );
              FMA( vc10, vb0, a[ii+10][kk], gvl );
              FMA( vc11, vb0, a[ii+11][kk], gvl );
              FMA( vc12, vb0, a[ii+12][kk], gvl );
              FMA( vc13, vb0, a[ii+13][kk], gvl );
              FMA( vc14, vb0, a[ii+14][kk], gvl );
              FMA( vc15, vb0, a[ii+15][kk], gvl );
              FMA( vc16, vb0, a[ii+16][kk], gvl );
              FMA( vc17, vb0, a[ii+17][kk], gvl );
              FMA( vc18, vb0, a[ii+18][kk], gvl );
              FMA( vc19, vb0, a[ii+19][kk], gvl );
              FMA( vc20, vb0, a[ii+20][kk], gvl );
              FMA( vc21, vb0, a[ii+21][kk], gvl );
              FMA( vc22, vb0, a[ii+22][kk], gvl );
              FMA( vc23, vb0, a[ii+23][kk], gvl );
              }
           }
           __builtin_epi_vstore_nt_1xf64(&c[ii][jj], vc0, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+1][jj], vc1, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+2][jj], vc2, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+3][jj], vc3, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+4][jj], vc4, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+5][jj], vc5, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+6][jj], vc6, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+7][jj], vc7, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+8][jj], vc8, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+9][jj], vc9, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+10][jj], vc10, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+11][jj], vc11, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+12][jj], vc12, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+13][jj], vc13, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+14][jj], vc14, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+15][jj], vc15, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+16][jj], vc16, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+17][jj], vc17, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+18][jj], vc18, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+19][jj], vc19, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+20][jj], vc20, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+21][jj], vc21, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+22][jj], vc22, gvl);
           __builtin_epi_vstore_nt_1xf64(&c[ii+23][jj], vc23, gvl);
       }
     }
     jj += gvl;
  }
  bk=1;   //JL: did not look adapt tail
  int ii_left=ii;
  for (int jj = 0; jj < N; ) {
     __epi_1xf64 vc0, vc1, vc2, vc3, vc4, vc5, vc6, vc7;
     __epi_1xf64 vb0;
     unsigned long int gvl = __builtin_epi_vsetvl(N-jj, __epi_e64, __epi_m1);
     for (ii=ii_left; ii < M; ii += 8) {
        vc0  = BROADCAST_f64(0.0, gvl); 
        vc1  = BROADCAST_f64(0.0, gvl); 
        vc2  = BROADCAST_f64(0.0, gvl); 
        vc3  = BROADCAST_f64(0.0, gvl); 
        vc4  = BROADCAST_f64(0.0, gvl);
        vc5  = BROADCAST_f64(0.0, gvl);
        vc6  = BROADCAST_f64(0.0, gvl);
        vc7  = BROADCAST_f64(0.0, gvl);

        for (int kk = 0; kk < K; kk += bk) {
           vb0 = __builtin_epi_vload_1xf64(&b[kk][jj], gvl);
           {
           FMA( vc0,  vb0,   a[ii][kk], gvl );
           if (ii+1 < M) FMA( vc1,  vb0,   a[ii+1][kk], gvl );
           if (ii+2 < M) FMA( vc2,  vb0,   a[ii+2][kk], gvl );
           if (ii+3 < M) FMA( vc3,  vb0,   a[ii+3][kk], gvl );
           if (ii+4 < M) FMA( vc4,  vb0,   a[ii+4][kk], gvl );
           if (ii+5 < M) FMA( vc5,  vb0,   a[ii+5][kk], gvl );
           if (ii+6 < M) FMA( vc6,  vb0,   a[ii+6][kk], gvl );
           if (ii+7 < M) FMA( vc7,  vb0,   a[ii+7][kk], gvl );
           }
        }
        __builtin_epi_vstore_1xf64(&c[ii][jj], vc0, gvl);
        if (ii+1 < M)  __builtin_epi_vstore_1xf64(&c[ii+1][jj], vc1, gvl);
        if (ii+2 < M)  __builtin_epi_vstore_1xf64(&c[ii+2][jj], vc2, gvl);
        if (ii+3 < M)  __builtin_epi_vstore_1xf64(&c[ii+3][jj], vc3, gvl);
        if (ii+4 < M)  __builtin_epi_vstore_1xf64(&c[ii+4][jj], vc4, gvl);
        if (ii+5 < M)  __builtin_epi_vstore_1xf64(&c[ii+5][jj], vc5, gvl);
        if (ii+6 < M)  __builtin_epi_vstore_1xf64(&c[ii+6][jj], vc6, gvl);
        if (ii+7 < M)  __builtin_epi_vstore_1xf64(&c[ii+7][jj], vc7, gvl);
     }
  jj += gvl;
  }
}
