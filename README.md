## License

Each of the benchmarks contains its own license file and its use is subject to
the terms and conditions stated in that license. Subfolders inherit license
from the parent folder.

## About

Set of vectorized RISC-V benchmarks:

| Benchmark category | List of benchmarks |
| ----               | ---- |
| HPC benchmarks     | axpy, fft, fftp, jacobi-2d, lulesh, gemm, somier, spmv. |
| Desktop benchmarks | blackscholes, canneal, particlefilter, pathfinder, streamcluster, swaptions. |
| Micro benchmarks   | BuffCopyUnit, BuffCopyStrided, BuffCopyIndexed, FpuMicroKernel, InstrNopBalance, MemArithBalance, LatencyVrgather. |

## Getting the benchmarks

In order to obtain all the suite benchmarks you should use:

```
git clone https://gitlab.bsc.es/benchmarks/risc-v-benchmarks.git --recursive
```

Notice the recurive parameter will clone resursively other related
repositories. If you don't use that option, the git command will only clone
local benchmarks.

## Configuring

The configuration is composed by two different files:
* settings/<config_file>, containing Makefile options (compiler flags, ...)
* settings/<config_file.sh>, containing software folders, etc.

```
source configure settings/<config_file>
```

or edit the default configuration:

```
$vi settings/default
$vi settings/default.sh
```

## Available versions

In general, each benchmark will include a `README.md`  file including a
detailed table of the available versions. Usually benchmarks follow the
`<program>-<version>` naming convention. Versions could be:

- `scalar`: Builds and creates scalar executable.
- `autovec`: Builds and creates an autovectorized executable (relying on the compiler analyisis).
- `omp-simd`: Builds and creates an explicit vectorization executable.
- `cblas`: Builds and creates CBLAS executable.
- `rvv0.7`: Builds and creates a vector (based on RVV-0.7 intrinsics) executable. 
- `rvv1.0`: Builds and creates a vector (based on RVV-1.0 intrinsics) executable.

## Compiling

To compile all benchmarks with their settings defined in individual Makefiles, run `make`.

Individual compiling settings, as described in the previous section, assume compilation on HiFive platform using the LLVM toolchain.

To perform make clean for all benchmarks, run `make clean`.

## Tracing on HiFive platform

Each benchmark contains a script `trace.sh` for tracing on HiFive platform using the Vehave emulator.

The script traces the following vector sizes: 128b, 256b, 512b, 1024b, 2048b, 4096b.

NOTES: 

* fft includes only 1024b-4096b vector sizes
* SpMV includes only 512b-4096b vector sizes

The inputs for tracing the codes produce traces with individual sizes between 50 MB and 1.5 GB.



## Code size

When these traces are simulated with MUSA, they give the following number of
committed instructions for 1024b vector size:

| Benchmark      | Num. of lines |
| ----           | ----          |
| axpy           |  204800       |
| blackscholes   |  199201       |
| canneal        |   36692       |
| fft            | 1346225       |
| jacobi-2d      |   23856       |
| Lulesh         |  292908       |
| MxM            | 1106995       |
| particlefilter |   16493       |
| pathfinder     |  113400       |
| somier         |  322638       |
| SpMV           | 4536738       |
| streamcluster  |  130728       |
| swaptions      |  125378       |


