#!/bin/bash
#SBATCH --partition=fpga-sdv
#SBATCH --nodes=1
#SBATCH --time=1:00:00
#SBATCH --job-name=CI-RVB-test-$1

#############################################
## Construct SDV_HOST from X86_HOST        ##
#############################################
# X86_HOST: pickle-{1,2,3}
# SDV_HOST: fpga-sdv-{1,2,3}

X86_HOST="`hostname`"
SDV_HOST="fpga-sdv-`echo ${X86_HOST} | cut -d '-' -f 2`"

printf "******************************\n"
printf "* x86 node: %s\n" "${X86_HOST}"
printf "* SDV node: %s\n" "${SDV_HOST}"
printf "******************************\n\n"

ssh -o StrictHostKeyChecking=no ${SDV_HOST} "cd $3/.gitlab-ci/$2/$1 ; ./test_fpga_$1.sh $3"
