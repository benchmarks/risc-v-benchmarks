#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/micro_benchmarks/BuffCopyUnit/bin

source configure settings/riscv64-arriesgado
source ./environment.sh

for pipeline in 1 2 4 8 ; do
  VEHAVE_TRACE_SINGLE_THREAD=1 VEHAVE_DEBUG_LEVEL=0 VEHAVE_VECTOR_LENGTH=16384 VEHAVE_TRACE=0 ${VEHAVE_HOME}/bin/vehave ${BINARY}/BuffCopyUnit_intrinsic_pipelining${pipeline} 2048 256 256
done
