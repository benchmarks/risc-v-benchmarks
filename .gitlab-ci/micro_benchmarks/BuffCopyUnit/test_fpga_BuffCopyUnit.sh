#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/micro_benchmarks/BuffCopyUnit/bin

for pipeline in 1 2 4 8 ; do
  for i in `seq 5` ; do
    ${BINARY}/BuffCopyUnit_intrinsic_pipelining${pipeline} 2048 256 256
  done
done
