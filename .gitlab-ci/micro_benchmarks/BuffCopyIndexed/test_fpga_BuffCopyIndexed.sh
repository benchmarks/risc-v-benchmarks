#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/micro_benchmarks/BuffCopyIndexed/bin

for i in `seq 5` ; do
  ${BINARY}/BuffCopyIndexed 2048 8 256
done
