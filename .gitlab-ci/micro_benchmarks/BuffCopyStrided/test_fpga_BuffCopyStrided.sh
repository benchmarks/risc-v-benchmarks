#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/micro_benchmarks/BuffCopyStrided/bin

for i in `seq 5` ; do
  ${BINARY}/BuffCopyStrided 2048 16
done
