#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/micro_benchmarks/BuffCopyStrided/bin

source configure settings/riscv64-arriesgado
source ./environment.sh

VEHAVE_TRACE_SINGLE_THREAD=1 VEHAVE_DEBUG_LEVEL=0 VEHAVE_VECTOR_LENGTH=16384 VEHAVE_TRACE=0 ${VEHAVE_HOME}/bin/vehave ${BINARY}/BuffCopyStrided 2048 16
