#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/hpc_benchmarks/axpy/bin

for i in `seq 5` ; do
  ${BINARY}/axpy-rvv0.7 1048576 100
done
