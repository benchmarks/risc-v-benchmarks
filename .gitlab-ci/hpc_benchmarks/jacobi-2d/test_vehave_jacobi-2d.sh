#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/hpc_benchmarks/jacobi-2d/bin

source configure settings/riscv64-arriesgado
source ./environment.sh

VEHAVE_TRACE_SINGLE_THREAD=1 VEHAVE_DEBUG_LEVEL=0 VEHAVE_VECTOR_LENGTH=16384 VEHAVE_TRACE=0 ${VEHAVE_HOME}/bin/vehave ${BINARY}/jacobi2d_intrinsic_vanilla 128 8
