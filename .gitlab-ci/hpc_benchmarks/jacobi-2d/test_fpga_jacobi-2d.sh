#!/bin/bash

REPOSITORY=$1
BINARY=${REPOSITORY}/hpc_benchmarks/jacobi-2d/bin

for i in `seq 5` ; do
  ${BINARY}/jacobi2d_intrinsic_vanilla 128 8
done
