#!/bin/bash

#Bare-metal default version; availables: coyote, ariane, and lagarto
RVB_BMETAL_VER=coyote

#The LLVM compiler path
#LLVM_HOME=/apps/riscv/llvm/EPI/development
LLVM_HOME=/apps/riscv/llvm/EPI-0.7/cross/development

# LLVM RISC-V Vector extension version
RVV=0.7

# Does the compiler support Inter Leaving
LLVM_IL=false

# Does the compiler support Un-Rolling
LLVM_UR=true

#The MPI library path
MPI_HOME=

#The Vehave tool path
#VEHAVE_HOME=/apps/riscv/vehave/EPI/development
VEHAVE_HOME=

#The CBLAS library
CBLAS_HOME=

# ####################################################
# Following paths usually do not require modifications
# ####################################################

#Compiler flavour
CC=${LLVM_HOME}/bin/clang
CXX=${LLVM_HOME}/bin/clang++

LLVM_BIN=${LLVM_HOME}/bin
LLVM_INC=${LLVM_HOME}/include
LLVM_LIB=${LLVM_HOME}/lib

#MPI directories
MPI_INC=
MPI_LIB=

#Vehave tools
VEHAVE_BIN=
VEHAVE_LIB=
VEHAVE_INC=

VEHAVE_LIB_SO=
VEHAVE_TO_PRV=

#BLAS-like library directories
CBLAS_INC=
CBLAS_LIB=

#Bare-metal directory (internal)
RVB_BMETAL_DIR=${RVB_COMMON_DIR}/bmetal/${RVB_BMETAL_VER}

