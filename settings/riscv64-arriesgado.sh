#!/bin/bash

#Bare-metal default version; availables: coyote, ariane, and lagarto
RVB_BMETAL_VER=coyote

#The LLVM compiler path
#LLVM_HOME=/apps/riscv/llvm/EPI/development
LLVM_HOME=/apps/riscv/llvm/EPI-0.7/development

# LLVM RISC-V Vector extension version
RVV=0.7

# Does the compiler support Inter Leaving
LLVM_IL=false

# Does the compiler support Un-Rolling
LLVM_UR=true

#The MPI library path
MPI_HOME=/opt/local

#The Vehave tool path
#VEHAVE_HOME=/apps/riscv/vehave/EPI/development
VEHAVE_HOME=/apps/riscv/vehave/EPI-0.7/development

#The CBLAS library
CBLAS_HOME=/apps/riscv/ubuntu/blis/bscdec59f9f-llvmEPI0.7dev

# ####################################################
# Following paths usually do not require modifications
# ####################################################

#Compiler flavour
CC=${LLVM_HOME}/bin/clang
CXX=${LLVM_HOME}/bin/clang++

LLVM_BIN=${LLVM_HOME}/bin
LLVM_INC=${LLVM_HOME}/include
LLVM_LIB=${LLVM_HOME}/lib

#MPI directories
MPI_INC=${MPI_HOME}/include/openmpi
MPI_LIB=${MPI_HOME}/lib

#Vehave tools
VEHAVE_BIN=${VEHAVE_HOME}/bin
VEHAVE_LIB=${VEHAVE_HOME}/lib64
VEHAVE_INC=${VEHAVE_HOME}/include/vehave

VEHAVE_LIB_SO=${VEHAVE_LIB}/libvehave.so 
VEHAVE_TO_PRV=${VEHAVE_HOME}/share/vehave2prv/vehave2prv

#BLAS-like library directories
CBLAS_INC=${CBLAS_HOME}/include/blis
CBLAS_LIB=${CBLAS_HOME}/lib

#Bare-metal directory (internal)
RVB_BMETAL_DIR=${RVB_COMMON_DIR}/bmetal/${RVB_BMETAL_VER}

