module purge
module load intel/2020.1

export CFLAGS="-Wall -Wextra -std=gnu99 -xSKYLAKE-AVX512 -O3"
export CFLAGS_SCALAR="${CFLAGS} -no-vec -no-simd"
export CFLAGS_VECTORIAL="${CFLAGS} -no-vec -no-simd"
export CFLAGS_EXPLICIT_AUTOVECTORIZATION="${CFLAGS} -no-vec -qopenmp-simd"
export CFLAGS_AUTOVECTORIZATION="${CFLAGS} -qopenmp-simd"
