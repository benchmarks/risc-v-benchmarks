export EPI_LLVM_HOME=/apps/riscv/llvm/EPI-0.7/development
export CC=${EPI_LLVM_HOME}/bin/clang
export CXX=${EPI_LLVM_HOME}/bin/clang++

export CFLAGS="-Wall -Wextra -march=rv64g -mepi -I/usr/include/riscv64-linux-gnu --target=riscv64-linux-gnu -O2 -mcpu=avispado -static"
