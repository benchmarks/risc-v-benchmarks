#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <assert.h>
#include "compare_results.h"

implementation_compare_array_fp(float);
implementation_compare_array_fp(double);

implementation_compare_array_int(int);
implementation_compare_array_int(int8_t);
implementation_compare_array_int(int16_t);
implementation_compare_array_int(int32_t);
implementation_compare_array_int(int64_t);

implementation_compare_array_int(uint8_t);
implementation_compare_array_int(uint16_t);
implementation_compare_array_int(uint32_t);
implementation_compare_array_int(uint64_t);
