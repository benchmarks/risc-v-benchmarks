#pragma once

#ifdef __cplusplus
extern "C" {
#endif

long long get_time();
float elapsed_time(long long start_time, long long end_time);

#ifdef __cplusplus
}
#endif
