#ifndef __CUSTOM_DEF_H
#define __CUSTOM_DEF_H

void simfence();

#define BARRIER()   simfence()
#define MAIN()      thread_entry(int cid, int nc) 
#define INIT_CID()

#endif

