#ifndef __CUSTOM_DEF_H
#define __CUSTOM_DEF_H

static void __attribute__((noinline)) barrier(int ncores);

#define BARRIER()   barrier(nc)
#define MAIN()      main(int argc, char** argv)  
#define INIT_CID()      uint32_t cid, nc; \
	cid = argv[0][0]; \
	nc = argv[0][1]; \


#endif

