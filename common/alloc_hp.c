#define USE_MALLOC_HP
#include "alloc_hp.h"

void* malloc_hp(size_t size){
  if(size == 0) return NULL;
  size_t num_pages = (((size-1)/HUGE_PAGE_SIZE)+1);
  void* pointer = mmap(NULL, num_pages * HUGE_PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, -1, 0);
  return pointer;
}

void free_hp(void* pointer, size_t size){
  size_t num_pages = (((size-1)/HUGE_PAGE_SIZE)+1);
  munmap(pointer, num_pages * HUGE_PAGE_SIZE);
}
