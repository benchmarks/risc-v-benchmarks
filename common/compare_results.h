#pragma once
#include <stdint.h>

#define signature_compare_array(TYPE) \
	void compare_array_##TYPE(TYPE* x, TYPE* y, size_t elements)

#define ERROR_THRESHOLD 100 //Num errors to print before exit
#define FP_ABS_ERROR 0.0000001

#define implementation_compare_array_fp(TYPE) \
        signature_compare_array(TYPE){ \
          int nerrs=0;\
          for (size_t i=0; i<elements; i++) {\
             double error = x[i] - y[i];\
             if (fabs(error) > FP_ABS_ERROR)  {\
                printf("x[%lu]=%.16f != y[%lu]=%.16f  INCORRECT RESULT !!!! \n ", i, x[i], i, y[i]);\
                nerrs++;\
                if (nerrs == ERROR_THRESHOLD) break;\
             }\
          }\
          if (nerrs != 0){ printf ("Result incorrect exiting with error code 1!!!\n"); exit(1);}\
        }\

#define implementation_compare_array_int(TYPE) \
        signature_compare_array(TYPE){ \
          printf("size = %ld\n", elements);\
          int nerrs=0;\
          for (size_t i=0; i<elements; i++) {\
            if(x[i] != y[i]){\
              printf("x[%lu]=%d != y[%lu]=%d  INCORRECT RESULT !!!! \n ", i, (int) x[i], i, (int) y[i]);\
              nerrs++;\
              if(nerrs == ERROR_THRESHOLD)break;\
            }\
            if (nerrs != 0){ printf ("Result incorrect exiting with error code 1!!!\n"); exit(1);}\
          }\
        }\

#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(float);

#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(double);

#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(int);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(int8_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(int16_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(int32_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(int64_t);

#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(uint8_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(uint16_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(uint32_t);
#ifdef __cplusplus
extern "C"
#endif
signature_compare_array(uint64_t);
