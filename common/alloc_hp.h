#include <stddef.h>
#include <sys/mman.h>

#define HUGE_PAGE_SIZE (1<<21)

void* malloc_hp(size_t size);
void free_hp(void* pointer, size_t size);

#ifndef USE_MALLOC_HP
#define malloc_hp(size) malloc(size)
#define free_hp(ptr,size) free(ptr)
#endif
