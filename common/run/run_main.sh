#!/bin/bash
source ../../environment.sh

if test $# -lt 1; then
  echo usage: ./run.sh RUN_MODE [PATH_CONFIG_FILE]
  exit 1
fi

RUN_MODE=$1;

# Preset configs
if [ "$RUN_MODE" = "vehave" ]; then
  export IF_FAIL=""
  export VEHAVE_TRACE_SINGLE_THREAD=1;
  export VEHAVE_DEBUG_LEVEL=0;
  export VEHAVE_TRACE=1
  export RUN_PREFIX=${VEHAVE_BIN}/vehave
  export TEST_VL_AGNOSTIC=1
  export VECTOR_LENGTH_LIST="512 1024 2048 4096 8192 16384"
  runs=1
  mkdir -p traces
elif [ "$RUN_MODE" = "native" ]; then
  export IF_FAIL=""
  export VECTOR_LENGTH_LIST="16384"
  runs=5
fi

# Load user defined mode
if test $# -eq 2; then
  PATH_CONFIG_FILE=$2
  source ${PATH_CONFIG_FILE} ${RUN_MODE}
fi

# Execute prolog
${RUN_PROLOG}
