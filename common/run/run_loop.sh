#!/bin/bash

run_binary="$1"
run_args="$2"
run_parsing="$3"
run_outputfile="$4"

for VVL in $VECTOR_LENGTH_LIST; do #only use in vehave mode
  export VEHAVE_VECTOR_LENGTH=${VVL}
  for r in `seq 1 $runs`; do
     export VEHAVE_TRACE_FILE=${PREFIX_VEHAVE_TRACE_FILE}-${VEHAVE_VECTOR_LENGTH}bits_vehaveVL.trace #only used in vehave mode
     var=`${RUN_PREFIX} ${run_binary} ${run_args}`
     oldIFS=$IFS;
     IFS=;
     ${run_parsing} ${run_binary} ${run_args} `echo "${var}"`  >> ${run_outputfile}
     if test $? -ne 0; then
        echo "Bad result for ${RUN_PREFIX} ${run_binary} ${run_args}"
        ${IF_FAIL}
     fi
     IFS=$oldIFS;
  done
done
