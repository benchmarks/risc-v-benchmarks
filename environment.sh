#!/bin/bash
RVB_ROOT=$( dirname $(readlink -f ${BASH_SOURCE[0]}) )

echo RISC-V Benchmark suite root directory: $RVB_ROOT

SCRIPT_NAME=$( readlink -f ${RVB_ROOT}/Makefile.in )
SCRIPT_FILE=$( readlink -f ${RVB_ROOT}/Makefile.in )".sh"

CONFIG_NAME=$( basename ${SCRIPT_NAME} )

echo RISC-V Benchmark suite profile\'s set to $CONFIG_NAME


RVB_COMMON_DIR=${RVB_ROOT}/common
RVB_RUN_DIR=${RVB_ROOT}/common/run

if [ -f ${SCRIPT_FILE} ]; then
   source ${SCRIPT_FILE}
fi

