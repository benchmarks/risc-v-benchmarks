# Guide on running the HPL bechmark on HCA cluster 


## Step 1 : Downloading the package

Download the latest version of the package from the HPL official website: https://www.netlib.org/benchmark/hpl/


Untar the downloaded package file

``` tar -zxf hpl-version.tar.gz ```

## Step 2 : Setup

Enter the untarred directory . Within it you will find the *setup* folder which contains Makefile templates for different architectures. These templates are used to customise how we want to build the hpl library. For this example, let's modify the Make.Linux_PII_CBLAS. You might want to copy the Makefile outside the setup folder inorder to preserve the original template.


``` user@hca-server:~/hpl/hpl-2.3/setup$ cp Make.Linux_PII_CBLAS ..```

##  Step 3 : Modifying the MakeFile

The first thing to edit is the TOPdir. It should be set to the directory of the hpl file that you downloaded.

``` TOPdir       = /home/username/hpl/hpl-2.3 ```

We then proceed to set the MPdir. This should be set to the PATH environment variable that is set when the mpi module is loaded. You can see the this directory by running *module show "mpi module"*

```
   MPdir        = /apps/riscv/openmpi/4.1.4_gcc10.3.0
   MPinc        = -I$(MPdir)/include
   MPlib        = $(MPdir)/lib/libmpi.so
```


We follow the same procedure with the linear algebra library directory. Run *module show "openblas module"* to see the respective path.

```
   LAdir         = /apps/riscv/ubuntu/openBLAS/0.3.20_gcc10.3.0
   LAinc         = -I$(LAdir)/include
   LAlib         = $(LAdir)/lib/libopenblas.so
```

We also need to set the right directories for the compilers and linkers

```
   CC           = /usr/bin/gcc
   CCNOOPT      = $(HPL_DEFS)
   CCFLAGS      = $(HPL_DEFS) -O3
```

```LINKER       = /usr/bin/gfortran ```

*NB* : The fortran linker is g77 in some systems and gfortran in others

## Step 4 : Building and running

Now we are all set to buid the program .

Make sure to do the following on a compute node and not the login node.

```make arch=Linux_PII_CBLAS```

If you need to rebuild , it is suggessted that you run:

```make clean arch=Linux_PII_CBLAS```

then rebuild.

Building takes a couple of minutes. Feel free to make a cup of coffee or stretch.

If the build is successful , you will notice new folders (include , lib and bin) in your directory . The bin folder contains the executable xhpl which is what we run for the benchmark. I suggest making a copy of the HPL.dat file from the bin folder to your current directory. We'll proceed to make changes to this HPL.dat file to suite our needs.

Here's one way we can modify the file.

```
    HPLinpack benchmark input file
    Innovative Computing Laboratory, University of Tennessee
    HPL.out      output file name (if any)
    6            device out (6=stdout,7=stderr,file)
    1            # of problems sizes (N)
    20
    1            # of NBs
    4            NBs
    0            PMAP process mapping (0=Row-,1=Column-major)
    1            # of process grids (P x Q)
    2             Ps
    2             Qs
    16.0         threshold
    1            # of panel fact
    2            PFACTs (0=left, 1=Crout, 2=Right)
    1            # of recursive stopping criterium
    4           NBMINs (>= 1)
    1            # of panels in recursion
    2            NDIVs
    1            # of recursive panel fact.
    1            RFACTs (0=left, 1=Crout, 2=Right)
    1            # of broadcast
    1            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)
    1            # of lookahead depth
    1            DEPTHs (>=0)
    2            SWAP (0=bin-exch,1=long,2=mix)
    64           swapping threshold
    0            L1 in (0=transposed,1=no-transposed) form
    0            U  in (0=transposed,1=no-transposed) form
    1            Equilibration (0=no,1=yes)
    8            memory alignment in double (> 0)

```
Once this is set , we are now ready to run the benchmarks. For this purpose we can submit a job script. Below is an example of a job script.

```
#!/bin/bash
#SBATCH --output=hpl_%j.out
#SBATCH --error=hpl_%j.err
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=1
#SBATCH --time=01:00:00
#SBATCH --partition=arriesgado-hirsute

source /etc/profile.d/modules.sh

export LD_LIBRARY_PATH=/apps/riscv/ubuntu/openBLAS/0.3.20_gcc10.3.0/lib

module load openBLAS/ubuntu/0.3.20_gcc10.3.0
module load openmpi/ubuntu/4.1.4_gcc10.3.0

mpirun -np 4 /home/username/hpl/hpl-2.3/bin/Linux_PII_CBLAS/xhpl
```


*Remember* : You will need to rebuild (rerun make) if you switch to a different partition / compute node.