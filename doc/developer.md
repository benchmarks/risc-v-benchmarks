Developer guide
===============

Brief introduction, include diagram or similar

Makefiles
---------
All the Makefile(s) should include the common configuration:

```
include ../../Makefile.in
```

Makefile -> Makefile.in -> settings/<configuration>.sh

The Makefile.in defines the following variables:
* CC
* CFLAGS
* etc

The Makefile should start with a default target, including all versions that
may run in all platforms with all compilers.


Scripts
-------

All the scripts should include the common configuration:

```
source ../../environment.sh
```



Conding guidelines
------------------

