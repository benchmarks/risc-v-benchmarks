
#define REP_0(str)\
        ; 

#define REP_1(str)\
        str

#define REP_2(str)\
        REP_1(str)\
        REP_1(str)

#define REP_4(str)\
        REP_2(str)\
        REP_2(str)

#define REP_8(str)\
        REP_4(str)\
        REP_4(str)

#define REP_16(str)\
        REP_8(str)\
        REP_8(str)

#define REP_32(str)\
        REP_16(str)\
        REP_16(str)

#ifndef REPS
  #define REPS REP_4
  #error
#endif

//#define ntimes 1000

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "elapsed_time.h"
int main(int argc, char **argv){
  if (argc != 3){
     printf("Usage: ./MemArithBalance-X array_size[elems] ntimes\n");
     exit(1);
  }
  int array_size = atoi(argv[1]);
  if(array_size < 512){
     printf("Please put array_size >= 512\n");
     exit(1);
  }
  int ntimes = atoi(argv[2]);
 printf("Array size=%d(elements), 2*array_size=%f(MiB)\n", array_size, 2*(array_size*sizeof(double)/(float)(1024*1024)));
 double* buffer = (double*)aligned_alloc(64, array_size*sizeof(double));
 double* buffer_out = (double*)aligned_alloc(64, array_size*sizeof(double));
 double* accum_buffer = (double*)aligned_alloc(64, 256*sizeof(double));

 for(int i = 0; i < array_size; i++){
    buffer[i] = i%9;
 }

  long long time_ini = get_time();
  const unsigned long int gvl_max = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
  __epi_1xf64 accum = __builtin_epi_vbroadcast_1xf64(0.0, gvl_max);
  __epi_1xf64 increment = __builtin_epi_vbroadcast_1xf64(1.0, gvl_max);

  //Prbar 9 nops
  for(int n = 0; n < ntimes; n++){
    unsigned gvl = __builtin_epi_vsetvl(array_size, __epi_e64, __epi_m1);
    for(int i = 0; i < array_size; i+=gvl){
      gvl = __builtin_epi_vsetvl(array_size-i, __epi_e64, __epi_m1);
      __epi_1xf64 data = __builtin_epi_vload_1xf64(&buffer[i], gvl);
      REPS(accum = __builtin_epi_vfadd_1xf64(accum, increment, gvl););
      __builtin_epi_vstore_1xf64(&buffer_out[i], data, gvl);
      REPS(accum = __builtin_epi_vfadd_1xf64(accum, increment, gvl););
    }
  }
  __builtin_epi_vstore_1xf64(accum_buffer, accum, gvl_max);
  long long time_fin = get_time();

  for(int i = 0; i < array_size; i++){
    if( buffer_out[i] != buffer[i]){
      printf("Error in i=%d\n",i);
      exit(1);
    }
  }

  for(uint64_t i = 1; i < gvl_max; ++i){
   accum_buffer[0] += accum_buffer[i];
  }

  double elapsedTime = elapsed_time(time_ini,time_fin);
  uint64_t bytes = sizeof(double)*2*array_size*ntimes;
  printf("header:\tTime(s)\tBW(MB/s)\tFLOPs/byte\tMFLOP/s\tMBOP/s\n");
  printf("result:\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n", elapsedTime, (bytes/elapsedTime)/1000000.0, accum_buffer[0]/bytes, (accum_buffer[0]/elapsedTime)/1000000.0, (accum_buffer[0]*8/elapsedTime)/1000000.0);
  return 0;
}
