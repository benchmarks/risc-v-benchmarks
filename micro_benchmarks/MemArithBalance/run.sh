#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

for i_size in `seq 11 19`; do
  size=$(( 2 ** i_size))
  ntimes=32
  ARGS="${size} ${vl} ${ntimes}"
  for version in 1 2 4 8 16 32; do
    BINARY=`pwd`/bin/MemArithBalance-${version}
    export PREFIX_VEHAVE_TRACE_FILE=traces/MemArithBalance_${version}-${size}_elements-${ntimes}_loops
    echo "Executing ${BINARY} ${ARGS}"
    ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
  done
done
