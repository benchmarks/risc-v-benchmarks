#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

ntimes=128
size=8192
ARGS="${size} ${ntimes}"
for version in 0 1 2 4 8 16 32; do
  BINARY=`pwd`/bin/InstrNopBalance-${version}
  export PREFIX_VEHAVE_TRACE_FILE=traces/InstrNopBalance_${version}-${size}_elements-${ntimes}_loops
  echo "Executing ${BINARY} ${ARGS}"
  ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
done
