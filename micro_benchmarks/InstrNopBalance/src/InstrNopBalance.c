
#define REP_0(str)\
        ; 

#define REP_1(str)\
        str

#define REP_2(str)\
        REP_1(str)\
        REP_1(str)

#define REP_4(str)\
        REP_2(str)\
        REP_2(str)

#define REP_8(str)\
        REP_4(str)\
        REP_4(str)

#define REP_9(str)\
        REP_8(str)\
        REP_1(str)

#define REP_16(str)\
        REP_8(str)\
        REP_8(str)

#define REP_32(str)\
        REP_16(str)\
        REP_16(str)

#ifndef REPS
  #error
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include "elapsed_time.h"

int main(int argc, char **argv){
  if (argc != 3){
    printf("Usage: ./InstrNopBalance-X  array_size[elems] ntimes\n");
    exit(1);
  }

  const int array_size = atoi(argv[1]);
  const int ntimes = atoi(argv[2]);

  if(array_size < 512){
     printf("Please put array_size >= 512\n");
     exit(1);
  }
 printf("Array size=%d(elements), 2*array_size=%f(MiB)\n", array_size, 2*(array_size*sizeof(double)/(float)(1024*1024)));
 double* buffer = (double*)aligned_alloc(64, array_size*sizeof(double));
 double* buffer_out = (double*)aligned_alloc(64, array_size*sizeof(double));

 for(int i = 0; i < array_size; i++){
    buffer[i] = i%9;
 }

  long long time_ini = get_time();

  //Prbar 9 nops
  unsigned gvl;
  for(int n = 0; n < ntimes; n++){
    for(int i = 0; i < array_size; i+=gvl){
      gvl = __builtin_epi_vsetvl(array_size-i, __epi_e64, __epi_m1);
      __epi_1xf64 data = __builtin_epi_vload_1xf64(&buffer[i], gvl);
      REPS(asm("nop"););
      __builtin_epi_vstore_1xf64(&buffer_out[i], data, gvl);
      REPS(asm("nop"););
    }
  }
  asm("fence\n");
  long long time_fin = get_time();

  for(int i = 0; i < array_size; i++){
    if( buffer_out[i] != buffer[i]){
      printf("Error in i=%d\n",i);
      exit(1);
    }
  }

  double elapsedTime = elapsed_time(time_ini,time_fin);
  uint64_t bytes = sizeof(double)*2*array_size*ntimes;
  printf("header:\tTime(s)\tBW(MB/s)\n");
  printf("result:\t%.8f\t%.8f\n", elapsedTime, (bytes/elapsedTime)/1000000.0);

}
