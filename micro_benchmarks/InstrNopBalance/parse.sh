#!/bin/bash

echo -n "`basename $1` " #version
echo -n "$2 " #elements, ntimes (times copy the buffer)
echo "$3" | grep result | awk '{first = $1; $1 = ""; print $0}' #time
