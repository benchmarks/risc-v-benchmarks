
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include "elapsed_time.h"

int main(int argc, char **argv){

  if (argc != 3){
    printf("Usage: ./BuffCopyStrided array_size[elems] ntimes\n");
    exit(-1);
  }

  int array_size = atoi(argv[1]);
  if(array_size < 512){
     printf("Please put array_size >= 512\n");
     exit(1);
  }
  int ntimes = atoi(argv[2]);

 const unsigned gvl_max = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
 printf("maxvl = %u\n",gvl_max);

 printf("Array size=%d(elements), array_size=%f(MiB)\n", array_size, (array_size*sizeof(double)/(float)(1024*1024)));
 double* buffer = (double*)aligned_alloc(64, 8*array_size*sizeof(double));
 double* accum_buffer = (double*)aligned_alloc(64, gvl_max*sizeof(double));

 for(int i = 0; i < array_size*8; i++){
    buffer[i] = 1.0;
 }

 // struct timeval t1, t2;
  __epi_1xf64 accum = __builtin_epi_vbroadcast_1xf64(0, gvl_max);

//  gettimeofday(&t1, NULL);
  unsigned gvl;
  __epi_1xf64 data;
  long long time_ini = get_time();
  for(int n = 0; n < ntimes; n++){
//    unsigned gvl = __builtin_epi_vsetvl(array_size, __epi_e64, __epi_m1);
    for(int i = 0; i < array_size; i+=gvl){
      gvl = __builtin_epi_vsetvl(array_size-i, __epi_e64, __epi_m1);
      data = __builtin_epi_vload_strided_1xf64(&buffer[8*i], 64, gvl);
      accum = __builtin_epi_vfadd_1xf64(accum, data, gvl);
    }
  }
  __builtin_epi_vstore_1xf64(accum_buffer, accum, gvl_max);
  asm("fence\n");
  long long time_fin = get_time();

  for(uint64_t i = 1; i < gvl_max; ++i){
   accum_buffer[0] += accum_buffer[i];
  }
  printf("accum[0]=%f\n",accum_buffer[0]);

  double elapsedTime = elapsed_time(time_ini,time_fin) ;
  //elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
  //elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
  uint64_t bytes = sizeof(double)*array_size*ntimes;
  printf("header:\tTime(s)\tBW(MB/s)\n");
  printf("result:\t%.8f\t%.8f\n", elapsedTime, (bytes/elapsedTime)/1000000.0);
  return 0;
}
