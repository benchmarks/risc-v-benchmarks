//#define ntimes 1024*32

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include "elapsed_time.h"

int main(int argc, char * argv[]){
  if (argc != 3){
    printf("Usage: FpuMicroKernel VL ntimes\n- VL: if VL == -1 then VLMAX will set\n");
    exit(-1);
  }
  const int rvl = atoi(argv[1]);
  const int ntimes = atoi(argv[2]);

  unsigned long  gvl = 0;
  if(rvl == -1){
    gvl = __builtin_epi_vsetvlmax( __epi_e64, __epi_m1);
  }else{
    gvl = __builtin_epi_vsetvl(rvl, __epi_e64, __epi_m1);
  }

  printf("requested vl=%d, granted vl=%lu\n", rvl, gvl);
  asm(
    "vxor.vv v1, v1, v1\n"
    "vxor.vv v2, v2, v2\n"
    "vxor.vv v3, v3, v3\n"
    "vxor.vv v4, v4, v4\n"
    "vxor.vv v5, v5, v5\n"
    "vxor.vv v6, v6, v6\n"
    "vxor.vv v7, v7, v7\n"
    "vxor.vv v8, v8, v8\n"
    "vxor.vv v9, v9, v9\n"
    "vxor.vv v10, v10, v10\n"
    "vxor.vv v11, v11, v11\n"
    "vxor.vv v12, v12, v12\n"
    "vxor.vv v13, v13, v13\n"
    "vxor.vv v14, v14, v14\n"
    "vxor.vv v15, v15, v15\n"
    "vxor.vv v16, v16, v16\n"
    "vxor.vv v17, v17, v17\n"
    "vxor.vv v18, v18, v18\n"
    "vxor.vv v19, v19, v19\n"
    "vxor.vv v20, v20, v20\n"
    "vxor.vv v21, v21, v21\n"
    "vxor.vv v22, v22, v22\n"
    "vxor.vv v23, v23, v23\n"
    "vxor.vv v24, v24, v24\n"
    "vxor.vv v25, v25, v25\n"
    "vxor.vv v26, v26, v26\n"
    "vxor.vv v27, v27, v27\n"
    "vxor.vv v28, v28, v28\n"
    "vxor.vv v29, v29, v29\n"
    "vxor.vv v30, v30, v30\n"
  );
  long long time_ini = get_time();
  for(int n = 0; n < ntimes; n++){
    asm(
      "vfmacc.vv v1, v1, v1\n"
      "vfmacc.vv v2, v2, v2\n"
      "vfmacc.vv v3, v3, v3\n"
      "vfmacc.vv v4, v4, v4\n"
      "vfmacc.vv v5, v5, v5\n"
      "vfmacc.vv v6, v6, v6\n"
      "vfmacc.vv v7, v7, v7\n"
      "vfmacc.vv v8, v8, v8\n"
      "vfmacc.vv v9, v9, v9\n"
      "vfmacc.vv v10, v10, v10\n"
      "vfmacc.vv v11, v11, v11\n"
      "vfmacc.vv v12, v12, v12\n"
      "vfmacc.vv v13, v13, v13\n"
      "vfmacc.vv v14, v14, v14\n"
      "vfmacc.vv v15, v15, v15\n"
      "vfmacc.vv v16, v16, v16\n"
      "vfmacc.vv v17, v17, v17\n"
      "vfmacc.vv v18, v18, v18\n"
      "vfmacc.vv v19, v19, v19\n"
      "vfmacc.vv v20, v20, v20\n"
      "vfmacc.vv v21, v21, v21\n"
      "vfmacc.vv v22, v22, v22\n"
      "vfmacc.vv v23, v23, v23\n"
      "vfmacc.vv v24, v24, v24\n"
      "vfmacc.vv v25, v25, v25\n"
      "vfmacc.vv v26, v26, v26\n"
      "vfmacc.vv v27, v27, v27\n"
      "vfmacc.vv v28, v28, v28\n"
      "vfmacc.vv v29, v29, v29\n"
      "vfmacc.vv v30, v30, v30\n"
      "vfmacc.vv v31, v31, v31\n"
      "vfmacc.vv v1, v1, v1\n"
      "vfmacc.vv v2, v2, v2\n"
      "vfmacc.vv v3, v3, v3\n"
      "vfmacc.vv v4, v4, v4\n"
      "vfmacc.vv v5, v5, v5\n"
      "vfmacc.vv v6, v6, v6\n"
      "vfmacc.vv v7, v7, v7\n"
      "vfmacc.vv v8, v8, v8\n"
      "vfmacc.vv v9, v9, v9\n"
      "vfmacc.vv v10, v10, v10\n"
      "vfmacc.vv v11, v11, v11\n"
      "vfmacc.vv v12, v12, v12\n"
      "vfmacc.vv v13, v13, v13\n"
      "vfmacc.vv v14, v14, v14\n"
      "vfmacc.vv v15, v15, v15\n"
      "vfmacc.vv v16, v16, v16\n"
      "vfmacc.vv v17, v17, v17\n"
      "vfmacc.vv v18, v18, v18\n"
      "vfmacc.vv v19, v19, v19\n"
      "vfmacc.vv v20, v20, v20\n"
      "vfmacc.vv v21, v21, v21\n"
      "vfmacc.vv v22, v22, v22\n"
      "vfmacc.vv v23, v23, v23\n"
      "vfmacc.vv v24, v24, v24\n"
      "vfmacc.vv v25, v25, v25\n"
      "vfmacc.vv v26, v26, v26\n"
      "vfmacc.vv v27, v27, v27\n"
      "vfmacc.vv v28, v28, v28\n"
      "vfmacc.vv v29, v29, v29\n"
      "vfmacc.vv v30, v30, v30\n"
      "vfmacc.vv v31, v31, v31\n"
    );
  }
  long long time_fin = get_time();
  double elapsedTime = elapsed_time(time_ini,time_fin);
  const unsigned long flops = ntimes*62*gvl*2;
  printf("header:\tTime(s)\tMFLOP/s\tFlop/c\n");
  printf("result:\t%.8f\t%.8f\t%.8f\n", elapsedTime, (flops/elapsedTime)/1000000.0, flops/(elapsedTime*50000000.0));
  return 0;
}
