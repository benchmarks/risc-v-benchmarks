#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

ntimes=16384
ARGS="-1 ${ntimes}"
BINARY=`pwd`/bin/FpuMicroKernel
export PREFIX_VEHAVE_TRACE_FILE=traces/FpuMicroKernel-${ntimes}_loops
echo "Executing ${BINARY} ${ARGS}"
${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
