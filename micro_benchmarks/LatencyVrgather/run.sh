#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

mkdir patterns
cd patterns
ln -sf ../riscv_patterns.py .
python3 riscv_patterns.py
cd ..

for VL in 16 32 64 128 256; do
  for version in "" _mimic _mimic_mask; do
    BINARY=`pwd`/bin/LatencyVrgather${version}
    for SEW in 16 32 64; do
      for pattern in none seq rev 2_constant pair odd onlypair onlyodd 2_swap 4_swap 8_swap 16_swap 32_swap 32_swap 64_swap 128_swap 256_swap 512_swap; do
        for pattern_possible in $(find patterns -name pattern-${pattern}\*-${VL}_vl.csv); do
          pattern_file=$(basename ${pattern_possible} )
          pattern_arg=$(cat patterns/${pattern_file})
          ARGS="${VL} ${SEW} ${pattern_arg}"
          export PREFIX_VEHAVE_TRACE_FILE=traces/LatencyVrgather${version}-${SEW}_sew-${pattern_file}
          echo "Executing ${BINARY} ${ARGS}"
          ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
        done
      done
    done
  done
done
