#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define MAX_NUM_BYTES 256*8

extern void first(void* buffer);
#define read_counter(reg)                                                      \
  ({                                                                           \
    uint64_t counter;                                                 \
    asm volatile("csrr %0, " #reg : "=r"(counter));                            \
    counter;                                                                   \
  })

unsigned long int setVL(unsigned long int rvl, unsigned long int sew)__attribute__ ((optnone));
unsigned long int setVL(unsigned long int rvl, unsigned long int sew){
 switch (sew){
   case 8:
     return __builtin_epi_vsetvl(rvl, __epi_e8, __epi_m1);
   case 16:
     return __builtin_epi_vsetvl(rvl, __epi_e16, __epi_m1);
   case 32:
     return __builtin_epi_vsetvl(rvl, __epi_e32, __epi_m1);
   case 64:
     return __builtin_epi_vsetvl(rvl, __epi_e64, __epi_m1);
   default:
     printf("ERROR: sew=%lu not supported\n", sew);
     exit(1);
 }
}

void vidRegisters(int vl, int sew)__attribute__ ((optnone));
void vidRegisters(int vl, int sew){
  setVL(vl, sew);
  asm("vid.v v2");
  asm("vid.v v3");
  asm("vid.v v4");
  asm("vid.v v5");
  asm("vid.v v6");
  asm("vid.v v7");
  asm("vid.v v8");
  asm("vid.v v9");
  asm("vid.v v10");
  asm("vid.v v11");
  asm("vid.v v12");
  asm("vid.v v13");
  asm("vid.v v14");
  asm("vid.v v15");
  asm("vid.v v16");
  asm("vid.v v17");
  asm("vid.v v18");
  asm("vid.v v19");
  asm("vid.v v20");
  asm("vid.v v21");
  asm("vid.v v22");
  asm("vid.v v23");
  asm("vid.v v24");
  asm("vid.v v25");
  asm("vid.v v26");
  asm("vid.v v27");
  asm("vid.v v28");
  asm("vid.v v29");
  asm("vid.v v30");
  asm("vid.v v31");
}

int main(int argc, char *argv[]){
  if(argc < 4){
    printf("USAGE: vl sew pattern_0 ... pattern_vl-1\n");
    exit(1);
  }

  int vl = atoi(argv[1]);
  int sew = atoi(argv[2]);
  if(sew != 16 && sew != 32 && sew != 64){
    printf("Invalid SEW\n");
    exit(1);
  }
  if(vl < 1){
    printf("Invalid VL\n");
    exit(1);
  }
  if(sew*vl > MAX_NUM_BYTES*8){
    printf("ERROR\n");
    exit(1);
  }
  if(argc != vl+3){
    printf("ERROR: missign numbers in pattern\n");
    exit(1);
  }

  void* pattern = (void*)malloc(vl*8);
  void* mask = (void*)malloc(vl*8);
  void* buffer = (void*)malloc(vl*8+1);
//  char* fmt = "%lu";
  if(sew == 16){
    for(int i = 0; i < vl; i++){
      sscanf( argv[i + 3], "%hu", &((uint16_t*)pattern)[i]);
      ((uint16_t*)mask)[i] = ((uint16_t*)pattern)[i] == (uint16_t)-1? 0 : 1;
      ((uint16_t*)pattern)[i] = ((uint16_t*)pattern)[i] == (uint16_t)-1? vl : ((uint16_t*)pattern)[i];
      ((uint16_t*)pattern)[i] = ((uint16_t*)pattern)[i] << 1;
    }
  }else if(sew == 32){
    for(int i = 0; i < vl; i++){
      sscanf( argv[i + 3], "%u", &((uint32_t*)pattern)[i]);
      ((uint32_t*)mask)[i] = ((uint32_t*)pattern)[i] == (uint32_t)-1? 0 : 1;
      ((uint32_t*)pattern)[i] = ((uint32_t*)pattern)[i] == (uint32_t)-1? vl : ((uint32_t*)pattern)[i];
      ((uint32_t*)pattern)[i] = ((uint32_t*)pattern)[i] << 2;
    }
  }else{
    for(int i = 0; i < vl; i++){
      sscanf( argv[i + 3], "%lu", &((uint64_t*)pattern)[i]);
      ((uint64_t*)mask)[i] = ((uint64_t*)pattern)[i] == (uint64_t)-1? 0 : 1;
      ((uint64_t*)pattern)[i] = ((uint64_t*)pattern)[i] == (uint64_t)-1? vl : ((uint64_t*)pattern)[i];
      ((uint64_t*)pattern)[i] = ((uint64_t*)pattern)[i] << 3;
    }
  }

  vidRegisters(vl, sew);
  unsigned long gvl = setVL(vl, sew);
  asm volatile("vle.v v2, (%0)"::"r"(pattern));
  asm volatile("vle.v v1, (%0)"::"r"(pattern));
  asm volatile("vle.v v0, (%0)"::"r"(mask));
  first(buffer);

  uint64_t start_insts = read_counter(instret);
  uint64_t start_cycles = read_counter(cycle);
  first(buffer);
  uint64_t end_cycles = read_counter(cycle);
  uint64_t end_insts = read_counter(instret);
  uint64_t cycles = end_cycles-start_cycles;
  uint64_t insts = end_insts-start_insts;
  double cpi = (double)cycles/(double)insts;
  double ipc = (double)insts/(double)cycles;
  printf("header:\tgVL\tVL\tSEW\tcycles\tinstr\tCPI\tIPC\n");
  printf("result:\t%lu\t%d\t%d\t%lu\t%lu\t%lf\t%lf\n", gvl, vl, sew, cycles, insts, cpi, ipc);
  return 0;
}

