#!/bin/bash

def pattern_to_file(filename, pattern):
    f = open(filename, "w")
    pattern_to_string = " ".join(str(x) for x in pattern)
    f.write(pattern_to_string)
    f.close()

patterns = []
patterns_const = []
for vl_exp in range(1,11):
    vl = pow(2, vl_exp)
    for i in range(vl):
        filename="pattern-{}_constant-{}_vl.csv".format(i, vl)
        patterns_const=[i]*vl
        pattern_to_file(filename, patterns_const)

    patterns_none=[-1]*vl
    filename="pattern-none-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_none)

    patterns_seq = []
    patterns_rev = []
    patterns_pair = []
    patterns_odd = []
    patterns_onlypair = []
    patterns_onlyodd = []
    for i in range(vl):
        patterns_seq.append(i);
        patterns_rev.append(vl-i-1);
        patterns_pair.append((int)(i-i%2));
        patterns_odd.append((int)(i+abs((i%2)-1)));
        patterns_onlypair.append(i*((i+1)%2) -1*(i%2));
        patterns_onlyodd.append(i*(i%2) -1*((i+1)%2));

    filename="pattern-seq-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_seq)
    filename="pattern-rev-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_rev)
    filename="pattern-pair-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_pair)
    filename="pattern-odd-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_odd)
    filename="pattern-onlypair-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_onlypair)
    filename="pattern-onlyodd-{}_vl.csv".format(vl)
    pattern_to_file(filename, patterns_onlyodd)

    for j in range(1,11):
       swap_range = pow(2, j)
       if(vl <= swap_range):
           continue
       swap_list = []
       chunks = (int)(vl/swap_range)
       for x in range(chunks):
         for y in range(swap_range*(x+1)-1, swap_range*x-1, -1):
             swap_list.append(y)
       
       filename="pattern-{}_swap-{}_vl.csv".format(swap_range, vl)
       pattern_to_file(filename, swap_list)
