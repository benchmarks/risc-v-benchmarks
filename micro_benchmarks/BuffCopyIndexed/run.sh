#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

for i_size in `seq 11 19`; do
  size=$(( 2 ** i_size))
  for stride in 8 16 64 256 1024; do
  ntimes=32
  ARGS="${size} ${stride} ${ntimes}"
    for version in Indexed; do
      BINARY=`pwd`/bin/BuffCopy${version}
      export PREFIX_VEHAVE_TRACE_FILE=traces/BuffCopy${version}-${size}_elements-${stride}_stride-${ntimes}_loops
      echo "Executing ${BINARY} ${ARGS}"
      ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
    done
  done
done
