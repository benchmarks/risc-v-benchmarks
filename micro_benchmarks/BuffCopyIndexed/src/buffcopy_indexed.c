//#define ntimes 10000

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include "elapsed_time.h"

int main(int argc, char **argv){

  if(argc != 4){
    printf("USAGE: ./indexed_bw num_elements stride_bytes ntimes\n");
    exit(1);
  }
  const int num_elements = atoi(argv[1]);
  const int stride_bytes = atoi(argv[2]);
  const int ntimes = atoi(argv[3]);
  const int stride_pow = log2(stride_bytes);
  const int stride_elements = stride_bytes/sizeof(double);

  if(stride_bytes%sizeof(double) != 0 || stride_bytes <= 0){
    printf("stride_bytes must multiple of %lu and must by bigger than 0\n", sizeof(double));
    exit(1);
  }

  if(num_elements < 512){
     printf("Please put num_elements >= 512\n");
     exit(1);
  }

  const uint64_t bytes_allocated = stride_bytes*num_elements;
  const uint64_t bytes_effective = sizeof(double)*num_elements*ntimes;

 printf("stride_bytes=%d stride_pow=%d stride_elements=%d\n", stride_bytes, stride_pow, stride_elements);
 printf("effective_elements=%d(elements), allocated_array=%f(MiB)\n", num_elements, ((float)bytes_allocated/(1024.0*1024.0)));

 double* buffer = (double*)aligned_alloc(64, bytes_allocated);
 double* buffer_out = (double*)aligned_alloc(64, num_elements*sizeof(double));

 memset(buffer, 0, bytes_allocated);
 for(int i = 0; i < num_elements; i++){
    buffer[stride_elements*i] = 1.0;
 }

  const unsigned gvl_max = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);

  __epi_1xf64 accum = __builtin_epi_vbroadcast_1xf64(0, gvl_max);
  __epi_1xi64 indices = __builtin_epi_vid_1xi64(gvl_max);
  indices = __builtin_epi_vsll_1xi64(indices, __builtin_epi_vbroadcast_1xi64(stride_pow, gvl_max), gvl_max);

    unsigned gvl;
    __epi_1xf64 data;
  long long time_ini = get_time();
  for(int n = 0; n < ntimes; n++){
//    unsigned gvl = __builtin_epi_vsetvl(num_elements, __epi_e64, __epi_m1);
//    __epi_1xf64 data;
    for(int i = 0; i < num_elements; i+=gvl){
      gvl = __builtin_epi_vsetvl(num_elements-i, __epi_e64, __epi_m1);
      data = __builtin_epi_vload_indexed_1xf64(&buffer[stride_elements*i], indices, gvl);
      accum = __builtin_epi_vfadd_1xf64(accum, data, gvl);
    }
  }
  __builtin_epi_vstore_1xf64(buffer_out, accum, gvl_max);
  asm("fence\n");
  long long time_fin = get_time();
  printf("accum[0]=%f\n",buffer_out[0]);

  double elapsedTime = elapsed_time(time_ini,time_fin);
  printf("header:\tTime(s)\tBW(MB/s)\n");
  printf("result:\t%.8f\t%.8f\n", elapsedTime, (bytes_effective/elapsedTime)/1000000.0);
  return 0;
}
