#include "intrinsic_wrapper.h"

#define pipelining16(out, in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      __epi_1xf64 reg_0;\
      __epi_1xf64 reg_1;\
      __epi_1xf64 reg_2;\
      __epi_1xf64 reg_3;\
      __epi_1xf64 reg_4;\
      __epi_1xf64 reg_5;\
      __epi_1xf64 reg_6;\
      __epi_1xf64 reg_7;\
      __epi_1xf64 reg_8;\
      __epi_1xf64 reg_9;\
      __epi_1xf64 reg_10;\
      __epi_1xf64 reg_11;\
      __epi_1xf64 reg_12;\
      __epi_1xf64 reg_13;\
      __epi_1xf64 reg_14;\
      __epi_1xf64 reg_15;\
      for(uint64_t index = 0; index < num_vloads; index+=16){\
       reg_0  = vload_1xf64(&in[(index+0)*gvl],   gvl);\
       reg_1  = vload_1xf64(&in[(index+1)*gvl],   gvl);\
       reg_2  = vload_1xf64(&in[(index+2)*gvl],   gvl);\
       reg_3  = vload_1xf64(&in[(index+3)*gvl],   gvl);\
       reg_4  = vload_1xf64(&in[(index+4)*gvl],   gvl);\
       reg_5  = vload_1xf64(&in[(index+5)*gvl],   gvl);\
       reg_6  = vload_1xf64(&in[(index+6)*gvl],   gvl);\
       reg_7  = vload_1xf64(&in[(index+7)*gvl],   gvl);\
       reg_8  = vload_1xf64(&in[(index+8)*gvl],   gvl);\
       reg_9  = vload_1xf64(&in[(index+9)*gvl],   gvl);\
       reg_10 = vload_1xf64(&in[(index+10)*gvl],  gvl);\
       reg_11 = vload_1xf64(&in[(index+11)*gvl],  gvl);\
       reg_12 = vload_1xf64(&in[(index+12)*gvl],  gvl);\
       reg_13 = vload_1xf64(&in[(index+13)*gvl],  gvl);\
       reg_14 = vload_1xf64(&in[(index+14)*gvl],  gvl);\
       reg_15 = vload_1xf64(&in[(index+15)*gvl],  gvl);\
       vstore_1xf64(&out[(index+0)*gvl],  reg_0,  gvl);\
       vstore_1xf64(&out[(index+1)*gvl],  reg_1,  gvl);\
       vstore_1xf64(&out[(index+2)*gvl],  reg_2,  gvl);\
       vstore_1xf64(&out[(index+3)*gvl],  reg_3,  gvl);\
       vstore_1xf64(&out[(index+4)*gvl],  reg_4,  gvl);\
       vstore_1xf64(&out[(index+5)*gvl],  reg_5,  gvl);\
       vstore_1xf64(&out[(index+6)*gvl],  reg_6,  gvl);\
       vstore_1xf64(&out[(index+7)*gvl],  reg_7,  gvl);\
       vstore_1xf64(&out[(index+8)*gvl],  reg_8,  gvl);\
       vstore_1xf64(&out[(index+9)*gvl],  reg_9,  gvl);\
       vstore_1xf64(&out[(index+10)*gvl], reg_10, gvl);\
       vstore_1xf64(&out[(index+11)*gvl], reg_11, gvl);\
       vstore_1xf64(&out[(index+12)*gvl], reg_12, gvl);\
       vstore_1xf64(&out[(index+13)*gvl], reg_13, gvl);\
       vstore_1xf64(&out[(index+14)*gvl], reg_14, gvl);\
       vstore_1xf64(&out[(index+15)*gvl], reg_15, gvl);\
      }\
    }while(0)
