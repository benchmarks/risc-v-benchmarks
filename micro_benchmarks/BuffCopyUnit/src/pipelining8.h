#include "intrinsic_wrapper.h"

#define pipelining8(out, in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      const uint64_t unroll = 8;\
      __epi_1xf64 reg_0;\
      __epi_1xf64 reg_1;\
      __epi_1xf64 reg_2;\
      __epi_1xf64 reg_3;\
      __epi_1xf64 reg_4;\
      __epi_1xf64 reg_5;\
      __epi_1xf64 reg_6;\
      __epi_1xf64 reg_7;\
      for(uint64_t index = 0; index < num_vloads; index+=unroll){\
       reg_0 = vload_1xf64(&in[(index+0)*gvl], gvl);\
       reg_1 = vload_1xf64(&in[(index+1)*gvl], gvl);\
       reg_2 = vload_1xf64(&in[(index+2)*gvl], gvl);\
       reg_3 = vload_1xf64(&in[(index+3)*gvl], gvl);\
       reg_4 = vload_1xf64(&in[(index+4)*gvl], gvl);\
       reg_5 = vload_1xf64(&in[(index+5)*gvl], gvl);\
       reg_6 = vload_1xf64(&in[(index+6)*gvl], gvl);\
       reg_7 = vload_1xf64(&in[(index+7)*gvl], gvl);\
       vstore_1xf64(&out[(index+0)*gvl], reg_0, gvl);\
       vstore_1xf64(&out[(index+1)*gvl], reg_1, gvl);\
       vstore_1xf64(&out[(index+2)*gvl], reg_2, gvl);\
       vstore_1xf64(&out[(index+3)*gvl], reg_3, gvl);\
       vstore_1xf64(&out[(index+4)*gvl], reg_4, gvl);\
       vstore_1xf64(&out[(index+5)*gvl], reg_5, gvl);\
       vstore_1xf64(&out[(index+6)*gvl], reg_6, gvl);\
       vstore_1xf64(&out[(index+7)*gvl], reg_7, gvl);\
      }\
    }while(0)
