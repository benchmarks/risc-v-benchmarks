#define clangvectorize(out, in, size, gvl) \
    do{\
      _Pragma("clang loop vectorize(enable) unroll(enable) interleave(enable)")\
      for(uint64_t i = 0; i < size; i++){\
        out[i] = in[i];\
      } \
    }while(0)
