#define ompsimd(out, in, size, gvl) \
    do{\
      _Pragma("omp simd")\
      for(uint64_t i = 0; i < size; i++){\
        out[i] = in[i];\
      } \
    }while(0)
