#include "intrinsic_wrapper.h"

#define pipelining2(out, in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      const uint64_t unroll = 8;\
       __epi_1xf64 reg_0;\
       __epi_1xf64 reg_1;\
      for(uint64_t index = 0; index < num_vloads; index+=unroll){\
         reg_0 = vload_1xf64(&in[(index+0)*gvl], gvl);\
         reg_1 = vload_1xf64(&in[(index+1)*gvl], gvl);\
          vstore_1xf64(&out[(index+0)*gvl], reg_0, gvl);\
          vstore_1xf64(&out[(index+1)*gvl], reg_1, gvl);\
         reg_0 = vload_1xf64(&in[(index+2)*gvl], gvl);\
         reg_1 = vload_1xf64(&in[(index+3)*gvl], gvl);\
          vstore_1xf64(&out[(index+2)*gvl], reg_0, gvl);\
          vstore_1xf64(&out[(index+3)*gvl], reg_1, gvl);\
         reg_0 = vload_1xf64(&in[(index+4)*gvl], gvl);\
         reg_1 = vload_1xf64(&in[(index+5)*gvl], gvl);\
          vstore_1xf64(&out[(index+4)*gvl], reg_0, gvl);\
          vstore_1xf64(&out[(index+5)*gvl], reg_1, gvl);\
         reg_0 = vload_1xf64(&in[(index+6)*gvl], gvl);\
         reg_1 = vload_1xf64(&in[(index+7)*gvl], gvl);\
          vstore_1xf64(&out[(index+6)*gvl], reg_0, gvl);\
          vstore_1xf64(&out[(index+7)*gvl], reg_1, gvl);\
      }\
    }while(0)
