#include <stdio.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include "counters.h"

void setup_counter(uint16_t csr, uint64_t event){
    int64_t ret = syscall(__NR_arch_specific_syscall+20, csr, event);
    printf("SYCALL TELLS = %ld\n", ret);
}
