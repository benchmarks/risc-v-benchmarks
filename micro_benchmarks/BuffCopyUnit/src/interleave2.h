#include "intrinsic_wrapper.h"

#define interleave2(out, in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      const uint64_t unroll = 8;\
      const uint64_t distance = 2;\
      __epi_1xf64 reg_0 = vload_1xf64(&in[0*gvl], gvl);\
      __epi_1xf64 reg_1 = vload_1xf64(&in[1*gvl], gvl);\
      vstore_1xf64(&out[0*gvl], reg_0, gvl);\
      reg_0 = vload_1xf64(&in[2*gvl], gvl);\
      vstore_1xf64(&out[1*gvl], reg_1, gvl);\
      reg_1 = vload_1xf64(&in[3*gvl], gvl);\
      vstore_1xf64(&out[2*gvl], reg_0, gvl);\
      reg_0 = vload_1xf64(&in[4*gvl], gvl);\
      for(uint64_t index = unroll-distance-1; index < num_vloads-unroll; index+=unroll){\
        const uint64_t prev = index-distance;\
        const uint64_t next = index;\
        vstore_1xf64(&out[(prev+0)*gvl], reg_1, gvl);\
        reg_1 = vload_1xf64(&in[(next+0)*gvl], gvl);\
        vstore_1xf64(&out[(prev+1)*gvl], reg_0, gvl);\
        reg_0 = vload_1xf64(&in[(next+1)*gvl], gvl);\
        vstore_1xf64(&out[(prev+2)*gvl], reg_1, gvl);\
        reg_1 = vload_1xf64(&in[(next+2)*gvl], gvl);\
        vstore_1xf64(&out[(prev+3)*gvl], reg_0, gvl);\
        reg_0 = vload_1xf64(&in[(next+3)*gvl], gvl);\
        vstore_1xf64(&out[(prev+4)*gvl], reg_1, gvl);\
        reg_1 = vload_1xf64(&in[(next+4)*gvl], gvl);\
        vstore_1xf64(&out[(prev+5)*gvl], reg_0, gvl);\
        reg_0 = vload_1xf64(&in[(next+5)*gvl], gvl);\
        vstore_1xf64(&out[(prev+6)*gvl], reg_1, gvl);\
        reg_1 = vload_1xf64(&in[(next+6)*gvl], gvl);\
        vstore_1xf64(&out[(prev+7)*gvl], reg_0, gvl);\
        reg_0 = vload_1xf64(&in[(next+7)*gvl], gvl);\
      }\
      const uint64_t next_tail = num_vloads - distance - 1;\
      const uint64_t prev_tail = next_tail - distance;\
      vstore_1xf64(&out[(prev_tail+0)*gvl], reg_1, gvl);\
      reg_1 = vload_1xf64(&in[(next_tail+0)*gvl], gvl);\
      vstore_1xf64(&out[(prev_tail+1)*gvl], reg_0, gvl);\
      reg_0 = vload_1xf64(&in[(next_tail+1)*gvl], gvl);\
      vstore_1xf64(&out[(prev_tail+2)*gvl], reg_1, gvl);\
      reg_1 = vload_1xf64(&in[(next_tail+2)*gvl], gvl);\
      vstore_1xf64(&out[(prev_tail+3)*gvl], reg_0, gvl);\
      vstore_1xf64(&out[(prev_tail+4)*gvl], reg_1, gvl);\
    }while(0)
