#include "intrinsic_wrapper.h"

#define interleave8(out, in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      const uint64_t unroll = 8;\
      const uint64_t distance = 2;\
      __epi_1xf64 reg_0 = vload_1xf64(&in[0*gvl], gvl);\
      __epi_1xf64 reg_1 = vload_1xf64(&in[1*gvl], gvl);\
      __epi_1xf64 reg_2 = vload_1xf64(&in[2*gvl], gvl);\
      __epi_1xf64 reg_3 = vload_1xf64(&in[3*gvl], gvl);\
      __epi_1xf64 reg_4 = vload_1xf64(&in[4*gvl], gvl);\
      __epi_1xf64 reg_5 = vload_1xf64(&in[5*gvl], gvl);\
      __epi_1xf64 reg_6 = vload_1xf64(&in[6*gvl], gvl);\
      __epi_1xf64 reg_7 = vload_1xf64(&in[7*gvl], gvl);\
      for(uint64_t index = unroll; index < num_vloads; index+=unroll){\
        const uint64_t prev = index - unroll; \
        const uint64_t next = index;\
        vstore_1xf64(&out[(prev+0)*gvl], reg_0, gvl);\
        reg_0 = vload_1xf64(&in[(next+0)*gvl], gvl);\
        vstore_1xf64(&out[(prev+1)*gvl], reg_1, gvl);\
        reg_1 = vload_1xf64(&in[(next+1)*gvl], gvl);\
        vstore_1xf64(&out[(prev+2)*gvl], reg_2, gvl);\
        reg_2 = vload_1xf64(&in[(next+2)*gvl], gvl);\
        vstore_1xf64(&out[(prev+3)*gvl], reg_3, gvl);\
        reg_3 = vload_1xf64(&in[(next+3)*gvl], gvl);\
        vstore_1xf64(&out[(prev+4)*gvl], reg_4, gvl);\
        reg_4 = vload_1xf64(&in[(next+4)*gvl], gvl);\
        vstore_1xf64(&out[(prev+5)*gvl], reg_5, gvl);\
        reg_5 = vload_1xf64(&in[(next+5)*gvl], gvl);\
        vstore_1xf64(&out[(prev+6)*gvl], reg_6, gvl);\
        reg_6 = vload_1xf64(&in[(next+6)*gvl], gvl);\
        vstore_1xf64(&out[(prev+7)*gvl], reg_7, gvl);\
        reg_7 = vload_1xf64(&in[(next+7)*gvl], gvl);\
      }\
      const uint64_t prev_tail = num_vloads - unroll;\
      vstore_1xf64(&out[(prev_tail+0)*gvl], reg_0, gvl);\
      vstore_1xf64(&out[(prev_tail+1)*gvl], reg_1, gvl);\
      vstore_1xf64(&out[(prev_tail+2)*gvl], reg_2, gvl);\
      vstore_1xf64(&out[(prev_tail+3)*gvl], reg_3, gvl);\
      vstore_1xf64(&out[(prev_tail+4)*gvl], reg_4, gvl);\
      vstore_1xf64(&out[(prev_tail+5)*gvl], reg_5, gvl);\
      vstore_1xf64(&out[(prev_tail+6)*gvl], reg_6, gvl);\
      vstore_1xf64(&out[(prev_tail+7)*gvl], reg_7, gvl);\
    }while(0)
