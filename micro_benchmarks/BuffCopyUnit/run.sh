#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

intrinsics_versions="interleave2 interleave4 interleave8 pipelining1 pipelining2 pipelining4 pipelining8 pipelining16"
autovectorization_versions="clangvectorize ompsimd"
scalar_versions="vanilla"

versions=

for i in ${intrinsics_versions}; do
  versions="${versions} intrinsic_${i}"
  versions="${versions} intrinsic_${i}_vload-nt"
  versions="${versions} intrinsic_${i}_vstore-nt"
  versions="${versions} intrinsic_${i}_vload-nt_vstore-nt"
done

for i in ${autovectorization_versions}; do
  versions="${versions} autovectorization_${i}"
done

for i in ${scalar_versions}; do
  versions="${versions} scalar_${i}"
done

for i_size in `seq 11 19`; do
  size=$(( 2 ** i_size))
  for vl in 8 16 32 64 128 256; do
  ntimes=32
  ARGS="${size} ${vl} ${ntimes}"
    for version in ${versions}; do
      BINARY=`pwd`/bin/BuffCopyUnit_${version}
      export PREFIX_VEHAVE_TRACE_FILE=traces/BuffCopyUnit_${version}-${size}_elements-${vl}_rvl-${ntimes}_loops
      echo "Executing ${BINARY} ${ARGS}"
      ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
    done
  done
done
