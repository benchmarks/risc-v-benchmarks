# COMPILATION
Run `make`.  One can run `make PAGES=HUGE` to activate huge pages.

# RUNNING

usage: `./latency.x BUFFER_SIZE(B) BATCHES FREQ(MHz) PADDING(elems) MODE MEASURE {gVL}`
Where:
- **BUFFER_SIZE**: Size of the allocated buffer in bytes
- **BATCHES**: Number of time the buffer is traversed. Consider a value greater than 1 when the buffer is small, in order to avoid timing errors.
- **FREQ**: Frequency in MHz, to go from cycles to microseconds and the inverse. (50 in the SDV) 
- **PADING**: Number of elements between one access and the other. It is recommended to put the size of the cache line here (e.g. 8 for SDV).
- **MODE**: can be `write`, `read`, `chase`, `vread`. Chase is normally used to compute scalar memory latency.
- **MEASURE**: can be either `cycles` or `time`.
- **gVL**: Optional, only if MODE==vread. Vector length of the vector memory accesses.

Examples:
- `./latency.x 32768 1000 50 8 chase cycles` : Sequential pointer chase in a 32KB array with a stride of 8 elements, 1000 times.
- `./latency.x 262144 10 50 8 vread cycles 16` : Vector reading in a 256KB array with a stride of 8 elements and vl=16, 10 times.

An script is also provided, which can be called as `./run.sh [vec/seq] outputfile`, where:
- **[vec/seq]**: Use `vec` to run both sequential and vector modes (write, read, chase, vread) or `seq` to avoid vread.
- **outputfile**: Name of the output csv

Examples:
- `./run.sh vec sdv_mem.csv`: Generates the file sdv\_mem.csv containing the system's latency for the 4 modes and different buffer sizes.
- `./run.sh seq arriesgado_mem.csv`: Generates the file arriesgado\_mem.csv containing the system's latency for the 3 sequential modes and different buffer sizes.
