#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


#ifdef HUGE

#include <stddef.h>
#include <sys/mman.h>
#define HUGE_PAGE_SIZE (1<<21)
#define alignment 128
#define huge_alloc(size) ({\
	void * pointer = NULL;\
	if(size != 0){ \
		size_t num_pages = (((size-1)/HUGE_PAGE_SIZE)+1);\
		pointer = mmap(NULL, num_pages * HUGE_PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, -1, 0);\
	}\
	pointer;\
})
#define huge_free(X,size) ({\
	size_t num_pages = (((size-1)/HUGE_PAGE_SIZE)+1);\
        munmap(X, num_pages * HUGE_PAGE_SIZE);\
})
#else

#define huge_alloc(size) aligned_alloc(128,size)
#define huge_free(X,size) free(X)

#endif




#define GET_TIME() getusec_();
static uint64_t getusec_(void) {
	struct timeval time;
	gettimeofday(&time, 0);
	return ((uint64_t)time.tv_sec * (uint64_t)1e6 + (uint64_t)time.tv_usec);
}

#define GET_CYCLES() \
({ \
        uint64_t counter; \
        asm volatile("csrr %0, cycle" : "=r"(counter)); \
        counter; \
})


#define WRITE_test()\
	for(int i=0; i<batches; ++i){			\
		for (int j=0; j<N*stride; j+=stride){	\
			list[j] = 0xCAFE; 		\
		}					\
	}	
					
#define READ_test()\
	for(int i=0; i<batches; ++i){			\
		for (int j=0; j<N*stride; j+=stride){	\
			r = list[j];			\
		}					\
	}

#define CHASE_test()\
	for(int i=0; i<batches; ++i){			\
		ptr = &list[0];				\
		while(ptr!=0){				\
			ptr = (uint64_t *)*(ptr);	\
		}					\
	}

#define VREAD_UNIT_test()\
	for(int i=0; i<batches; ++i){					\
		for (int j=0; j<N*gVL; j+=gVL){				\
			asm volatile("vle.v v1, (%0)"::"r"(&list[j])); 	\
		}							\
	}

#define VREAD_STRIDE_test()\
	for(int i=0; i<batches; ++i){										\
		for (int j=0; j<N*stride*gVL; j+=gVL*stride){							\
			asm volatile("vlse.v  v1, (%0), %1"::"r"(&list[j]),"r"(stride*sizeof(uint64_t)));	\
		}												\
	}\


#define Measure_lat(mod,mes){\
	uint64_t t1 = GET_##mes();\
	mod##_test()	\
 	asm volatile("fence \n");\
	uint64_t t2 = GET_##mes();\
	delta = t2-t1;\
	lat = (((double)delta)/((double)N));\
	lat = lat / (double)batches;}


char * names[4] = {"write","read","chase","vread"};
#define WRITE 0
#define READ 1
#define CHASE 2
#define VREAD 3
#define TIME 0
#define CYCLES 1
int main(int argc, char * argv[]){

	/***********  INITIALIZE **************/
	if (argc != 7 && argc != 8){
		printf("usage: ./latency.x BUFFER_SIZE(B) BATCHES FREQ(MHz) PADDING(elems) MODE MEASURE {gVL}\n");
		printf("MODE: write, read, chase, vread\n");
		printf("MEASUE: time, cycles\n");
		printf("{gVL}: mandatory only if MODE==vread, specify gVL (must be smaller than N)\n");
		printf("\nExample: ./latency.x 4096 1 50 2 chase cycles\n");
		printf("This will create a 4096Byte array and access elements with a stride of 2 (2048 accesses bytes, 256 64bit-elements)\n");
		exit(-1);
	}

	int mode;
	if (strcmp(argv[5],"write") == 0) mode=WRITE;
	else if  (strcmp(argv[5],"read") == 0) mode=READ;
	else if  (strcmp(argv[5],"chase") == 0) mode=CHASE;
	else if  (strcmp(argv[5],"vread") == 0) mode=VREAD;
	else {
		printf("incorrect MODE. Your input is %s,  valid inputs are write,read,chase\n",argv[5]);
		exit(-1);
	}
	int measure;
	if (strcmp(argv[6],"time") == 0) measure=TIME;
	else if  (strcmp(argv[6],"cycles") == 0) measure=CYCLES;
	else {
		printf("incorrect MEASURE. Your input is %s,  valid inputs are time,cycles\n",argv[5]);
		exit(-1);
	}
	

	int size = atoi(argv[1]); //Elements accessed in a batch
	int batches = atoi(argv[2]); //batches :)
	double freq = atof(argv[3]); //Core Freq in MHz
	int stride = atoi(argv[4]); // Distance in elements between accesses

	int N = size / (stride*sizeof(uint64_t));
	if (N==0){
		printf("size (%d) too small for stride (%d).\n\tMinimum size for this stride=%lu\n\tMaximum stride for this size=%lu\n",size,stride,stride*sizeof(uint64_t),size/sizeof(uint64_t));
		exit(-1);
	}

	long gVL=0;
	if (mode==VREAD){
		if (argc != 8) printf("Please specify a vector length as the last parameter\n");
		long desired = atol(argv[7]);
        	gVL = __builtin_epi_vsetvl(desired, __epi_e64, __epi_m1);
		if (N/gVL == 0){
			printf("gVL (%ld) too large for this size (%d). Max gVL = %d for this size.\n",gVL,size,N);
			exit(-1);
		}
		N /= gVL;
	}
	printf("N=%d\n",N);

	
	/***********  START TEST  **************/
	uint64_t * volatile list = (uint64_t*)huge_alloc(size);

	//Try to put the vector in cache, so if it fits in cache the time measure won't include accessing to memory
	for (int j=0; j<N-1; ++j){
		list[j*stride] = (uint64_t)&list[(j+1)*stride];
	}
	list[(N-1)*stride] = 0;//(uint64_t)&list[0];
	
	volatile uint64_t r;
	uint64_t * ptr;
	uint64_t delta;
	double lat=0;

	if (measure == TIME){
		if (mode == READ){ Measure_lat(READ,TIME);
		}else if (mode == WRITE){ Measure_lat(WRITE,TIME);
		}else if (mode == CHASE){ Measure_lat(CHASE,TIME);
		}else if (mode == VREAD && stride==1){ Measure_lat(VREAD_UNIT,TIME);
		}else if (mode == VREAD && stride>1){ Measure_lat(VREAD_STRIDE,TIME);
		}
		printf("mode\tmem_acc\tstride\tbatches\tsize(KB)\tlatency(us)\tlatency(cyc)\tgVL\n");
		printf("%s\t%d\t%d\t%d\t%.4f\t%.4f\t%.4f\t%ld\n",names[mode],N,stride,batches,((double)(size)/1024.0),lat,lat*freq,gVL);
	}else if (measure == CYCLES){
		if (mode == READ){ Measure_lat(READ,CYCLES);
		}else if (mode == WRITE){ Measure_lat(WRITE,CYCLES);
		}else if (mode == CHASE){ Measure_lat(CHASE,CYCLES);
		}else if (mode == VREAD && stride==1){ Measure_lat(VREAD_UNIT,CYCLES);
		}else if (mode == VREAD && stride>1){ Measure_lat(VREAD_STRIDE,CYCLES);
		}
		printf("mode\tmem_acc\tstride\tbatches\tsize(KB)\tlatency(us)\tlatency(cyc)\tgVL\n");
		printf("%s\t%d\t%d\t%d\t%.4f\t%.4f\t%.4f\t%ld\n",names[mode],N,stride,batches,((double)(size)/1024.0),lat/freq,lat,gVL);

	}
	huge_free(list,size);
	return 0;
}
