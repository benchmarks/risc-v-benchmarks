#!/bin/bash

RUN_DIR=`pwd`/../../common/run
source ${RUN_DIR}/run_main.sh native
export PARSING_FILE="`pwd`/parse.sh"

if test $# -ne 2; then
	echo "usage: ./run.sh [vec / seq] output_file"
	exit
fi

mode=$1
output=$2

if [[ "$mode" != "vec" ]] && [[ "$mode" != "seq" ]]; then
	echo "usage: ./run.sh binary [vec / seq] output_file"
	exit
fi

freq=50
reps=50000
measure=cycles

#Get header
#$binary 16 1 50 1 read cycles | grep mode > $output
echo "mode mem_acc stride batches size(KB) latency(us) latency(cyc) gVL" > $output

#From 1KB to 16MB
size=1024 # 1024B
sizemax=$((1<<24)) # 16 MB
while test $size -le $sizemax; 
do
	echo "Testing size $size / $sizemax"
	IFS=;
	for padding in 8 #1 2 4 8 16
	do
		### scalar
		for mod in chase read write
		do 
			BINARY=`pwd`/bin/latency.x
  			ARGS="$size $reps $freq $padding $mod cycles"
			export PREFIX_VEHAVE_TRACE_FILE=traces/memlat-${size}-$padding-$mod
			echo "Executing ${BINARY} ${ARGS}"
			${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "$output"
			#var=`$binary $size $reps $freq $padding $mod cycles`
			#echo $var | tail -n 1 >> $output
		done

		### vectorial
		if [[ "$mode" == "vec" ]]; then
		for VL in 1 2 4 8 16 32 64 128 256
		do
			if test $VL -le $(( $size / ($padding * 8) )); then 
				BINARY=`pwd`/bin/latency.x
	  			ARGS="$size $reps $freq $padding $mod cycles $VL"
				export PREFIX_VEHAVE_TRACE_FILE=traces/memlat-${size}-$padding-$mod-$VL
				echo "Executing ${BINARY} ${ARGS}"
				${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "$output"
				#var=`$binary $size $reps $freq $padding vread cycles $VL`
				#echo $var | tail -n 1 >> $output
			fi
		done
		fi
	done
	size=$(( $size * 2 ))
	reps=$(( $reps / 2 ))
	if test $reps -lt 10; then
		reps=10
	fi
done
