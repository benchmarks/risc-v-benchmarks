#!/bin/bash

RUN_DIR=`pwd`/../../common/run
mode=$1;
source ${RUN_DIR}/run_main.sh $*

export PARSING_FILE="`pwd`/parse.sh"

intrinsics_versions="onlyload onlyload_unroll4 onlyload_unroll4_tail onlystore onlystore_unroll4 onlystore_unroll4_tail"

versions=

for i in ${intrinsics_versions}; do
  versions="${versions} intrinsic_${i}"
  versions="${versions} intrinsic_${i}_HP"
done

for size in `seq 256 256 262144`; do
  for vl in 8 16 32 64 128 256; do
  ntimes=32
  ARGS="${size} ${vl} ${ntimes}"
    for version in ${versions}; do
      BINARY=`pwd`/bin/OnlyUnit_${version}
      export PREFIX_VEHAVE_TRACE_FILE=traces/OnlyUnit_${version}-${size}_elements-${vl}_rvl-${ntimes}_loops
      echo "Executing ${BINARY} ${ARGS}"
      ${RUN_DIR}/run_loop.sh "${BINARY}" "${ARGS}" "${PARSING_FILE}" "data_${mode}.csv"
    done
  done
done
