#include "intrinsic_wrapper.h"

#define onlyload_unroll4_tail(in, size, gvl) \
    do{\
      asm("vsetvli zero, %0, e64,m1"::"r"(gvl));\
      int num_loops = size/(gvl*4);\
      for(uint64_t i = 0; i < num_loops*4; i+=4){\
       asm("vle.v v1 , (%0)"::"r"(&in[(i+0)*gvl]));\
       asm("vle.v v2 , (%0)"::"r"(&in[(i+1)*gvl]));\
       asm("vle.v v3 , (%0)"::"r"(&in[(i+2)*gvl]));\
       asm("vle.v v4 , (%0)"::"r"(&in[(i+3)*gvl]));\
      }\
      for(uint64_t index = (num_loops*4); index < size/gvl; index++){\
       asm("vle.v v1 , (%0)"::"r"(&in[index*gvl]));\
      }\
    }while(0)
