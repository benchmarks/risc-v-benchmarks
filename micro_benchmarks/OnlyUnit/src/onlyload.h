#include "intrinsic_wrapper.h"

#define onlyload(in, size, gvl) \
    do{\
      uint64_t num_loops = size/gvl;\
      asm("vsetvli zero, %0, e64, m1"::"r"(gvl));\
      for(uint64_t index = 0; index < num_loops; index++){\
       asm("vle.v v1 , (%0)"::"r"(&in[index*gvl]));\
      }\
    }while(0)
