#include "intrinsic_wrapper.h"

#define onlystore(in, size, gvl) \
    do{\
      const uint64_t num_vloads = size/gvl;\
      asm("vsetvli zero, %0, e64,m1"::"r"(gvl));\
      for(uint64_t index = 0; index < num_vloads; index++){\
       asm("vse.v v1 , (%0)"::"r"(&in[index*gvl]));\
      }\
    }while(0)
