#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <limits.h>
#include <float.h>

#include "elapsed_time.h"
#include "counters.h"

//#define read_counter(reg) ({ register unsigned long counter;  \
                             asm volatile ( "csrr %0, " #reg : "=r"(counter) ); \
                             counter; })

#define LOOP_COUNT_ALL(N, PTR_CYCLES, PTR_TIME, F) \
  do{\
  setup_counter(0x323, 0x20);\
  unsigned long start_retries = read_counter(hpmcounter3);\
  uint64_t start_time = get_time();\
  unsigned long start_cycle = read_counter(cycle);\
  start_cycle = read_counter(cycle);\
  for(int i = 0; i < N; i++){\
    F;\
  }\
  asm("fence"); \
  unsigned long end_cycle = read_counter(cycle);\
  uint64_t end_time = get_time();\
  unsigned long end_retries = read_counter(hpmcounter3);\
  *PTR_TIME = elapsed_time(start_time, end_time);\
  *PTR_CYCLES = end_cycle - start_cycle; \
  printf("retries: %lu\n", end_retries-start_retries);\
  }while(0)

#define LOOP_COUNT_MIN(N, PTR_CYCLES, PTR_TIME, F) \
  do{\
  double __time_min = 1E+37;\
  uint64_t cycle_min = UINT_MAX;\
  for(int i = 0; i < N; i++){\
    uint64_t start_time = get_time();\
    unsigned long start_cycle = read_counter(cycle);\
    start_cycle = read_counter(cycle);\
    F;\
    asm("fence"); \
    unsigned long end_cycle = read_counter(cycle);\
    uint64_t end_time = get_time();\
    double elapsed = elapsed_time(start_time, end_time);\
    unsigned long elapsed_cycle = end_cycle - start_cycle; \
    if(elapsed < __time_min) __time_min = elapsed; \
    if(elapsed_cycle < cycle_min)cycle_min = elapsed_cycle; \
  }\
  *(PTR_CYCLES) = cycle_min;\
  *(PTR_TIME) = __time_min;\
  }while(0)
