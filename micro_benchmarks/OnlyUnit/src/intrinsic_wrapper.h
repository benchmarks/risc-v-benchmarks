#if !VLOAD_NON_TEMPORAL
#define vload_1xf64 __builtin_epi_vload_1xf64
#else
#define vload_1xf64 __builtin_epi_vload_nt_1xf64
#endif

#if !VSTORE_NON_TEMPORAL
#define vstore_1xf64 __builtin_epi_vstore_1xf64
#else
#define vstore_1xf64 __builtin_epi_vstore_nt_1xf64
#endif
