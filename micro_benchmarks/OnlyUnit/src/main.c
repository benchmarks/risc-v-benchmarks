#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/mman.h>

#include "loops_utils.h"
#include "compare_results.h"
#include "alloc_hp.h"

#ifndef TEST
#error You must define TEST
#endif

#define stringify(x) #x
#define GEN_INC_PATH(a) stringify(a.h)


#include GEN_INC_PATH(TEST)

int main(int argc, char **argv){

  if(argc != 4){
    printf("USAGE: ./OnlyLoadsUnit-{TEST} elements vl(Unused for scalar) ntimes\n"); 
    exit(1);
  }

  const size_t elements = atoi(argv[1]);
  const size_t RVL = atoi(argv[2]);
  const size_t ntimes = atoi(argv[3]);

  double buffer_size_mib = (double)(elements*sizeof(double))/(1024.0*1024.0);
  //printf("Buffer_in = %f MiB\nBuffer_in = %f MiB\nTotal allocated = %f MiB\n\n", buffer_size_mib, buffer_size_mib, 2*buffer_size_mib);

  //Checks
  #if USE_VECTOR_RISCV
  const size_t VL = __builtin_epi_vsetvl(RVL, __epi_e64, __epi_m1);
  printf("RVL=%lu GVL=%lu\n", RVL, VL);
  #else
  const size_t VL = 1;
  #endif

  if(VL <= 0 || VL > 256){
     printf("VL must be greater than 0 and less than 256\n");
     exit(1);
  }

  if(elements < VL){
     printf("Please put elements >= VL\n");
     exit(1);
  }

  if(elements%VL != 0){
     printf("elements must be multiple of vl\n");
     exit(1);
  }

  if(elements < 256){
     printf("elements must be bigger or equal to 256\n");
     exit(1);
  }

  if(ntimes < 1){
     printf("ntimes must be bigger or equal to 1\n");
     exit(1);
  }

  #if HP
  double* buffer = (double*)malloc_hp(elements*sizeof(double));
  #else
  double* buffer = (double*)aligned_alloc(64, elements*sizeof(double));
  #endif



  for(uint64_t i = 0; i < elements; i++){
    buffer[i] = i%1023;
  }

  double    time_all = 0.f;
  uint64_t cycles_all = 0;
  double    time_min = 1.f;
  uint64_t cycles_min = 0;

  //Heating
  LOOP_COUNT_MIN(16, &cycles_min, &time_min, TEST(buffer, elements, VL));
  LOOP_COUNT_MIN(ntimes, &cycles_min, &time_min, TEST(buffer, elements, VL));
  //Heating
  LOOP_COUNT_ALL(16, &cycles_all, &time_all, TEST(buffer, elements, VL));
  LOOP_COUNT_ALL(ntimes, &cycles_all, &time_all, TEST(buffer, elements, VL));

  uint64_t bytes = elements*8;
  double bytes_1loop = bytes;
  double bytes_all_loop = (ntimes*bytes);

  double time_all_bw = (bytes_all_loop/time_all)/1000000;
  double cycle_all_bw = bytes_all_loop/(cycles_all/50.0);
  double bytesc_all = bytes_all_loop/cycles_all;

  double time_min_bw = (bytes_1loop/time_min)/1000000;
  double cycle_min_bw = bytes_1loop/(cycles_min/50.0);
  double bytesc_min = bytes_1loop/cycles_min;

  printf("header:\t Time(s) \tcycles\tTime_min(s)\tcycles_min\tBW(MB/s)\tBW(MB/s) (measure in cycles @50MHz)\tBW_min(MB/s)\tBW_min(MB/s) (measure in cycles @50MHz)\t Bytes/c min\t Bytes/c avg\n");
  printf("results:\t %.8f\t%lu\t%.8f\t%lu\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n",time_all, cycles_all, time_min, cycles_min, time_all_bw, cycle_all_bw, time_min_bw, cycle_min_bw, bytesc_min, bytesc_all);
  return 0;
}
