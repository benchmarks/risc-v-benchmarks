#pragma once
#include <stdint.h>
//VPU counters
#define VPU_LOAD_RETRIES    ((uint64_t)0x020)
#define VPU_NUM_VECTOR_INST ((uint64_t)0b10<<5)
#define VPU_STALL_RENAMING  ((uint64_t)0b11<<5)
#define VPU_STALL_ARITH     ((uint64_t)0b100<<5)
#define VPU_STALL_MEM       ((uint64_t)0b101<<5)

//VPU counters events?
//#define CSR_MHPMEVENT3 ((uint16_t)0x323)
//#define CSR_MHPMEVENT4 ((uint16_t)0x324)

#define CSR_HPMCOUNTER3 0xc03
#define CSR_HPMCOUNTER4 0xc04

//Avispado counters

//Avispado counters events?
#define CSR_MHPMEVENT5 ((uint16_t)0x325)
#define CSR_MHPMEVENT6 ((uint16_t)0x326)
#define CSR_MHPMEVENT7 ((uint16_t)0x327)
#define CSR_MHPMEVENT8 ((uint16_t)0x328)

enum CSR_MHPMEVENTS {
   CSR_MHPMEVENT3=((uint16_t)0x323),
   CSR_MHPMEVENT4=((uint16_t)0x324),
};
//enum CSR_MHPCOUNTERS {
//   CSR_MHPCOUNTER3=((uint16_t)0xc03),
//   CSR_MHPCOUNTER4=((uint16_t)0xc04),
//};

#define read_counter(reg)                                                      \
  ({                                                                           \
    uint64_t counter;                                                          \
    asm volatile("csrr %0, " #reg : "=r"(counter));                            \
    counter;                                                                   \
  })

void setup_counter(uint16_t csr, uint64_t event);
//void setup_vpu_counter(uint16_t csr, uint64_t event);
//void setup_avispado_counter(uint16_t csr, uint64_t event);
