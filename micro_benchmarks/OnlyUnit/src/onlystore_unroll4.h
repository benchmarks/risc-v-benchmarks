#include "intrinsic_wrapper.h"

#define onlystore_unroll4(in, size, gvl) \
    do{\
      int gvl0=0, gvl1=0, gvl2=0, gvl3=0;\
      for(uint64_t index0 = 0; index0 < size; ){\
       asm("vsetvli %1, %0, e64,m1":"=r"(gvl0):"r"(size-index0));\
       asm("vse.v v1 , (%0)"::"r"(&in[index0]));\
       int index1 = index0+gvl0;\
       asm("vsetvli %1, %0, e64,m1":"=r"(gvl1):"r"(size-index1));\
       asm("vse.v v1 , (%0)"::"r"(&in[index1]));\
       int index2 = index1+gvl1;\
       asm("vsetvli %1, %0, e64,m1":"=r"(gvl2):"r"(size-index2));\
       asm("vse.v v1 , (%0)"::"r"(&in[index2]));\
       int index3 = index2+gvl2;\
       asm("vsetvli %1, %0, e64,m1":"=r"(gvl3):"r"(size-index3));\
       asm("vse.v v1 , (%0)"::"r"(&in[index3]));\
       index0 = index3+gvl3;\
      }\
    }while(0)
